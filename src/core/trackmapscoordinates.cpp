/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "trackmapscoordinates.h"

#include "eventdata.h"

TrackMapsCoordinates::TrackMapsCoordinates()
{
}

bool TrackMapsCoordinates::loadTrackCoordsFile()
{
    QString fName = F1LTCore::trackCoordsFile();
    if (!fName.isNull())
    {
        QFile f(fName);
        if (f.open(QIODevice::ReadOnly))
        {
            QDataStream stream(&f);

            char *cbuf;

            stream >> cbuf;
            QString sbuf(cbuf);
            delete [] cbuf;

            if (sbuf != "F1LT_TC")
                return false;

            int size;
            stream >> size;

            for (int i = 0; i < size; ++i)
            {
                TrackCoordinates trackCoordinates;

                int coordSize;
                stream >> cbuf;
                trackCoordinates.name = QString(cbuf);
                delete [] cbuf;

                stream >> trackCoordinates.year;

                int r,g,b;
                stream >> r;
                stream >> g;
                stream >> b;

                trackCoordinates.bgColor = QColor(r, g, b);

                stream >> trackCoordinates.initialMapAngle;
                stream >> trackCoordinates.indexes[TrackCoordinates::S1];
                stream >> trackCoordinates.indexes[TrackCoordinates::S2];
                stream >> trackCoordinates.indexes[TrackCoordinates::START];
                stream >> trackCoordinates.indexes[TrackCoordinates::PITOUT];
                stream >> trackCoordinates.horizontalRatio;
                stream >> trackCoordinates.verticalRatio;
                stream >> coordSize;

                for (int j = 0; j < coordSize; ++j)
                {
                    QPoint p;
                    stream >> p;
                    trackCoordinates.coordinates.append(p);
                }

                int cornerSize;
                stream >> cornerSize;
                for (int j = 0; j < cornerSize; ++j)
                {
                    TrackCoordinates::Corner corner;
                    stream >> cbuf;
                    corner.name = QString(cbuf);
                    delete [] cbuf;

                    stream >> corner.coordinates;
                    trackCoordinates.corners.append(corner);
                }

                int labelSize;
                stream >> labelSize;
                for (int j = 0; j < labelSize; ++j)
                {
                    TrackCoordinates::Label label;
                    int type;
                    stream >> type;
                    label.type = (TrackCoordinates::Label::LabelType)type;
                    stream >> label.coordinates;
                    trackCoordinates.labels.append(label);
                }

                int drsSize;
                stream >> drsSize;
                for (int j = 0; j < drsSize; ++j)
                {
                    TrackCoordinates::DRSZone drs;
                    stream >> drs.startIdx;
                    stream >> drs.endIdx;
                    trackCoordinates.drsZones.append(drs);
                }

                ltTrackCoordinates.append(trackCoordinates);

//                for (int j = 0; j < ltEvents.size(); ++j)
//                {
//                    if (ltEvents[j].eventShortName.toLower() == trackCoordinates.name.toLower())
//                    {
//                        ltEvents[j].trackCoordinates = trackCoordinates;
//                        break;
//                    }
//                }
            }
            return true;
        }
    }
    return false;
}

const TrackCoordinates *TrackMapsCoordinates::getCurrentTrackCoordinates() const
{
    EventData &ed = EventData::getInstance();
    const TrackCoordinates *currentTrackCoordinates = NULL;
    for (int i = 0; i < ltTrackCoordinates.size(); ++i)
    {
        if (ed.getEventInfo().eventPlace.contains(ltTrackCoordinates[i].name))
        {
            int year = ed.getEventInfo().fpDate.year();

            if (year == ltTrackCoordinates[i].year)
            {
                return &ltTrackCoordinates[i];
            }

            //if we don't have this years track coordinates, let's search for the newest ones
            if ((year > ltTrackCoordinates[i].year) &&
                ((currentTrackCoordinates == NULL) || (currentTrackCoordinates->year < ltTrackCoordinates[i].year)))
            {
                currentTrackCoordinates = &ltTrackCoordinates[i];
            }
        }
    }
    return currentTrackCoordinates;
}
