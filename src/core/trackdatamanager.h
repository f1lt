/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef TRACKDATAMANAGER_H
#define TRACKDATAMANAGER_H

#include <QFile>
#include <QPixmap>
#include <QString>

struct TrackData
{
    QString name;
    int year;
    double length;
    QString location;
    qint64 mapFilePos;

    QPixmap map;

    bool operator<(const TrackData &td) const
    {
        if (name == td.name)
            return year < td.year;

        return name < td.name;
    }

    bool operator==(const TrackData &td) const
    {
        return ((name == td.name) && (year == td.year) && (location == td.location));
    }

    TrackData()
    {
        year = 2010;
        length = 0.0;
        mapFilePos = 0;
    }
};

class TrackDataManager
{
public:
    TrackDataManager();
    bool loadTracksData();
    void loadTrackMap(TrackData &td);
    QPixmap getTrackMap(QString trackName, int year);
    QPixmap getCurrentTrackMap();

private:
    qint64 trackMapsStartingFilePos;
    QList<TrackData> tracks;
};

#endif // TRACKDATAMANAGER_H
