/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef SEASONXMLFILEMANAGER_H
#define SEASONXMLFILEMANAGER_H

#include <QObject>

#include <QDomDocument>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

struct LTTeam;
struct LTEvent;

class SeasonXmlFileManager : public QObject
{
    Q_OBJECT
public:
    explicit SeasonXmlFileManager(QObject *parent = 0);       

    void obtainSeasonXmlFile();
    void parseFile(const QByteArray &buf);
    void parseXML();
    LTEvent parseEvent(const QDomElement &element);
    LTTeam parseTeam(const QDomElement &element);

signals:
    void seasonXmlFileParsed(int year, const QVector<LTEvent> &, const QVector<LTTeam> &);

public slots:
    void finished();


private:
    QDomDocument seasonXmlFile;

    QNetworkAccessManager manager;
    QNetworkRequest req;
    QNetworkReply *reply;

};

#endif // SEASONXMLFILEMANAGER_H
