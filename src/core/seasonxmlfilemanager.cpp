/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "seasonxmlfilemanager.h"

#include "../net/networksettings.h"
#include "seasondata.h"

SeasonXmlFileManager::SeasonXmlFileManager(QObject *parent) :
    QObject(parent)
{
}

void SeasonXmlFileManager::obtainSeasonXmlFile()
{
    if (seasonXmlFile.isNull())
    {
        req = QNetworkRequest(NetworkSettings::getInstance().getSeasonXmlUrl());
        req.setRawHeader("User-Agent","f1lt");

        if (NetworkSettings::getInstance().usingProxy())
            manager.setProxy(NetworkSettings::getInstance().getProxy());
        else
            manager.setProxy(QNetworkProxy::NoProxy);

        reply = manager.get(req);

        connect(reply, SIGNAL(finished()), this, SLOT(finished()), Qt::UniqueConnection);
    }
    else
    {
        parseXML();
    }
}

void SeasonXmlFileManager::parseFile(const QByteArray &buf)
{
    seasonXmlFile.clear();
    seasonXmlFile.setContent(buf);

    parseXML();
}

void SeasonXmlFileManager::parseXML()
{
    QVector<LTTeam> teams;
    QVector<LTEvent> events;
    QDomElement docElem = seasonXmlFile.documentElement();

    QDomNode n = docElem.firstChild();
    while(!n.isNull())
    {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull())
        {
            if ((e.tagName() == "Year") &&
                (e.text().toInt() != QDate::currentDate().year()))
                return;

            if (e.tagName() == "Races")
            {
                QDomNode racesNode = e.firstChild();

                while (!racesNode.isNull())
                {
                    LTEvent event = parseEvent(racesNode.toElement());
                    events.append(event);

                    racesNode = racesNode.nextSibling();
                }
            }

            if (e.tagName() == "Teams")
            {
                QDomNode teamNode = e.firstChild();

                while (!teamNode.isNull())
                {
                    LTTeam team = parseTeam(teamNode.toElement());
                    teams.append(team);

                    teamNode = teamNode.nextSibling();
                }
            }
        }
        n = n.nextSibling();
    }

    if (!teams.isEmpty() && !events.isEmpty())
        emit seasonXmlFileParsed(QDate::currentDate().year(), events, teams);
}

LTEvent SeasonXmlFileManager::parseEvent(const QDomElement &element)
{
    LTEvent event;

    if (!element.isNull())
    {
        event.eventNo = element.attribute("no").toInt();

        QDomNode child = element.firstChild();

        while (!child.isNull())
        {
            if (child.toElement().tagName() == "Event")
                event.eventName = child.toElement().text();

            if (child.toElement().tagName() == "ShortName")
                event.eventShortName = child.toElement().text();

            if (child.toElement().tagName() == "Place")
                event.eventPlace = child.toElement().text();

            if (child.toElement().tagName() == "Laps")
                event.laps = child.toElement().text().toInt();

            if (child.toElement().tagName() == "PracticeDate")
                event.fpDate = QDate::fromString(child.toElement().text(), "dd-MM-yyyy");

            if (child.toElement().tagName() == "RaceDate")
                event.raceDate = QDate::fromString(child.toElement().text(), "dd-MM-yyyy");

            child = child.nextSibling();
        }
    }
    return event;
}

LTTeam SeasonXmlFileManager::parseTeam(const QDomElement &element)
{
    LTTeam team;

    if (!element.isNull())
    {
        QDomNode teamNode = element.firstChild();

        while (!teamNode.isNull())
        {
            if (teamNode.toElement().tagName() == "Name")
                team.teamName = teamNode.toElement().text();

            if (teamNode.toElement().tagName() == "Drivers")
            {
                QDomNode driverNode = teamNode.firstChild();

                while (!driverNode.isNull())
                {
                    LTDriver driver;
                    driver.no = driverNode.toElement().attribute("no").toInt();

                    QDomNode driverDataNode = driverNode.firstChild();

                    while (!driverDataNode.isNull())
                    {
                        if (driverDataNode.toElement().tagName() == "Name")
                            driver.name = driverDataNode.toElement().text();

                        if (driverDataNode.toElement().tagName() == "ShortName")
                            driver.shortName = driverDataNode.toElement().text();

                        driverDataNode = driverDataNode.nextSibling();
                    }
                    team.drivers.append(driver);

                    driverNode = driverNode.nextSibling();
                }
            }

            teamNode = teamNode.nextSibling();
        }
    }
    return team;
}

void SeasonXmlFileManager::finished()
{
    QByteArray buf = reply->readAll();

    parseFile(buf);
}
