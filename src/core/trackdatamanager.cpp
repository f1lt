/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "trackdatamanager.h"
#include "eventdata.h"

TrackDataManager::TrackDataManager()
{
    trackMapsStartingFilePos = 0;
}

bool TrackDataManager::loadTracksData()
{
    if (tracks.isEmpty())
    {
        QString fName = F1LTCore::trackDataFile();
        QFile file(fName);

        if (file.open(QIODevice::ReadOnly))
        {
            QDataStream stream(&file);

            char *tab;
            stream >> tab;

            QString sbuf(tab);

            delete [] tab;
            if (sbuf == "F1LT_TD")
            {
                tracks.clear();

                int size = 0;
                stream >> size;

                for (int i = 0; i < size; ++i)
                {
                    TrackData td;
                    stream >> td.name;
                    stream >> td.year;
                    stream >> td.location;
                    stream >> td.length;
                    stream >> td.mapFilePos;

                    tracks.append(td);
                }
                trackMapsStartingFilePos = file.pos();

                return true;
            }
        }
    }
    else
        return true;

    return false;
}

void TrackDataManager::loadTrackMap(TrackData &td)
{
    QString fName = F1LTCore::trackDataFile();
    QFile file(fName);

    if (file.open(QIODevice::ReadOnly))
    {
        QDataStream stream(&file);

        file.seek(trackMapsStartingFilePos + td.mapFilePos);

        stream >> td.map;
    }
}

QPixmap TrackDataManager::getTrackMap(QString trackName, int year)
{
    TrackData *td = 0;
    for (int i = 0; i < tracks.size(); ++i)
    {
        if (trackName.contains(tracks[i].name))
        {
            if (year == tracks[i].year)
            {
                td = &tracks[i];
                break;
            }

            //if we don't have this years track coordinates, let's search for the newest ones
            if ((year > tracks[i].year) &&
                ((td == NULL) || (td->year < tracks[i].year)))
            {
                td = &tracks[i];
            }
        }
    }
    if (td != 0)
    {
        if (td->map.isNull())
            loadTrackMap(*td);

        return td->map;
    }
    return QPixmap();
}

QPixmap TrackDataManager::getCurrentTrackMap()
{
    EventData &ed = EventData::getInstance();
    return getTrackMap(ed.getEventInfo().eventPlace, ed.getEventInfo().fpDate.year());
}
