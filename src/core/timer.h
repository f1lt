/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include <QThread>

class TimerThread : public QThread
{
Q_OBJECT

public:

    TimerThread(QObject *parent = 0) : QThread(parent)
    {
        interval = 0;
        prevTick = 0;
        running = false;
    }

    int getInterval() const { return interval; }

    void start(int msecs)
    {
        interval = msecs;
        prevTick = 0;
        originalInterval = msecs;
        if (!isRunning())
        {
            running = true;
            QThread::start();
        }
    }

    void stop()
    {
        running = false;
    }

    void run();

signals:
    void timeout();

public slots:
    void setInterval(int msecs)
    {
        prevTick = 0;
        interval = msecs;
        originalInterval = msecs;
        offset = 0;
        prevInterval = interval;
    }
    void synchronize();

private:
    bool running;
    int interval;
    int prevInterval;
    int offset;
    int originalInterval;
    qint64 prevTick;
};

class Timer : public QObject
{
    Q_OBJECT
public:
    explicit Timer(QObject *parent = 0);
    ~Timer();

    int getInterval() const
    {
        return timerInterval;
    }
    bool isActive() const
    {
        return (timerThread != 0);
    }

signals:
    void timeout();
    void setTimerInterval(int msecs);

public slots:
    void start(int msecs);
    void stop();
    void setInterval(int msecs);

private:
    TimerThread *timerThread;
    int timerInterval;
};

#endif // TIMER_H
