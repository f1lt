/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "timer.h"
#include <QDateTime>
#include <QDebug>

void TimerThread::run()
{
    prevInterval = originalInterval;
    offset = 0;

    while (running)
    {
        emit timeout();
        msleep(interval);

        synchronize();
    }
}

void TimerThread::synchronize()
{    
    qint64 currentTick = QDateTime::currentMSecsSinceEpoch();

    if (prevTick != 0)
    {
        offset = (currentTick - prevTick) - prevInterval;
        prevInterval = originalInterval - offset;
        interval = originalInterval - offset;

        if (interval < 1)
            interval = 1;
    }
    prevTick = currentTick;
}


Timer::Timer(QObject *parent) :
    QObject(parent), timerThread(0)
{
}

Timer::~Timer()
{
    if (timerThread)
    {
        timerThread->stop();
        timerThread->wait();
    }
}

void Timer::start(int msecs)
{
    if (!timerThread)
    {
        timerInterval = msecs;
        timerThread = new TimerThread(this);
        connect(timerThread, SIGNAL(timeout()), this, SIGNAL(timeout()));
    }

    timerThread->start(msecs);
}

void Timer::stop()
{
    if (timerThread)
    {
        timerThread->stop();
        disconnect(timerThread, SIGNAL(timeout()), this, SIGNAL(timeout()));
    }

    timerThread = 0;
}

void Timer::setInterval(int msecs)
{
    if (timerThread)
    {
        timerInterval = msecs;
        timerThread->setInterval(msecs);
    }
}
