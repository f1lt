/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef TRACKMAPSCOORDINATES_H
#define TRACKMAPSCOORDINATES_H

#include <QColor>
#include <QPoint>
#include <QString>
#include <QVector>

/*!
 * \brief The TrackCoordinates struct stores coordinates of a one track map
 */
struct TrackCoordinates
{
    int indexes[4];     //index of S1, S2 and pit out
    QColor bgColor;
    int initialMapAngle;
    int horizontalRatio;
    int verticalRatio;
    QVector<QPoint> coordinates;
    QString name;       //track name
    int year;           //track version

    enum MainPoints
    {
        S1 = 0, S2, PITOUT, START
    };

    struct Corner
    {
        QString name;
        QPoint coordinates;
    };

    struct Label
    {
        enum LabelType
        {
            S1 = 0, S2, SF, DRS
        };
        LabelType type;
        QPoint coordinates;

    };

    struct DRSZone
    {
        int startIdx;
        int endIdx;

        DRSZone() : startIdx(0), endIdx(0) { }
        DRSZone(int start, int end) : startIdx(start), endIdx(end) { }
    };

    TrackCoordinates()
    {
        indexes[0] = 0;
        indexes[1] = 0;
        indexes[2] = 0;
        initialMapAngle = 0;
        horizontalRatio = 0;
        verticalRatio = 0;
        year = 0;
    }

    QVector<Corner> corners;
    QVector<Label> labels;
    QVector<DRSZone> drsZones;
};


/*!
 * \brief The TrackMapsCoordinates class stores track map coordinates loaded from the .dat file. It used in driver tracker.
 */
class TrackMapsCoordinates
{
public:
    TrackMapsCoordinates();
    const TrackCoordinates *getCurrentTrackCoordinates() const;

    bool loadTrackCoordsFile();

private:
    QVector<TrackCoordinates> ltTrackCoordinates;
};

#endif // TRACKMAPSCOORDINATES_H
