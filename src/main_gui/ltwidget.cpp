/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "ltwidget.h"
#include "ui_ltwidget.h"

#include <QDebug>
#include <QPainter>

#include "models/practicemodel.h"
#include "models/qualimodel.h"
#include "models/racemodel.h"

LTWidget::LTWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LTWidget),
    eventData(EventData::getInstance()),
    eventType((LTPackets::EventType)0),
    currDriverId(0),    
    drawCarThumbnails(true),
    drawHelmets(true),
    ltModel(0),
    thumbnailsSize(75)
{
    ui->setupUi(this);
    loadCarImages();
    itemDelegate = new LTMainItemDelegate(ui->tableView, drawCarThumbnails, thumbnailsSize);
    ui->tableView->setItemDelegate(itemDelegate);

    ui->tableView->setIconSize(QSize(12, 12));
    //connect(ui->tableView, SIGNAL(headerClicked(int)), this, SLOT(on_tableView_headerClicked(int)));
}

LTWidget::~LTWidget()
{
    if (ltModel)
        delete ltModel;
    delete ui;
}

void LTWidget::clearData()
{
    if (ltModel)
    {
        delete ltModel;
        ltModel = 0;
    }
}

void LTWidget::loadCarImages()
{
    showDiff = 0;
    currDriverId = 0;

    ImagesFactory::getInstance().getCarThumbnailsFactory().loadCarThumbnails(thumbnailsSize);
}

void LTWidget::setFont(const QFont &font)
{
    ui->tableView->setFont(font);    
    QWidget::setFont(font);

    QFontMetrics f(font);

    if (ltModel)
    {
        for (int i = 0; i < ltModel->rowCount(); ++i)
            ui->tableView->setRowHeight(i, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
    }
}

void LTWidget::updateLT(const DataUpdates &dataUpdates)
{
    if (eventType != eventData.getEventType() || !ltModel)
    {
        clearData();

        if (eventData.getEventType() == LTPackets::PRACTICE_EVENT)
        {
            ltModel = new PracticeModel(this);
            ui->tableView->setModel(ltModel);
        }
        else if (eventData.getEventType() == LTPackets::QUALI_EVENT)
        {
            ltModel = new QualiModel(this);
            ui->tableView->setModel(ltModel);
        }
        else if (eventData.getEventType() == LTPackets::RACE_EVENT)
        {
            ltModel = new RaceModel(this);
            ui->tableView->setModel(ltModel);            
        }
        resizeEvent(0);
        eventType = eventData.getEventType();
    }
//    else
//        ui->tableView->reset();

    if (ltModel)
    {
        ltModel->updateLT(dataUpdates);
        if (currDriverId > 0)
            ui->tableView->setCurrentIndex(ltModel->indexOf(currDriverId));

    }
}

void LTWidget::resizeEvent(QResizeEvent *e)
{
    int w = ui->tableView->viewport()->width();/*-viewport()->width()*0.0115*/;//event->size().width()-event->size().width()*0.0115;
    int carsWidth = drawCarThumbnails ? w * 0.12 : 0;
    int helmetsWidth = drawHelmets ? w * 0.05 : 0;

    int totalW = w - carsWidth - helmetsWidth;


    switch(eventData.getEventType())
    {
        case LTPackets::RACE_EVENT:

            ui->tableView->setColumnWidth(RaceModel::POSITION, 0.05 * totalW);
            ui->tableView->setColumnWidth(RaceModel::NUMBER, 0.09 * totalW);
            ui->tableView->setColumnWidth(RaceModel::CAR, carsWidth);
            ui->tableView->setColumnWidth(RaceModel::HELMET, helmetsWidth);
            ui->tableView->setColumnWidth(RaceModel::NAME, 0.22 * totalW);
            ui->tableView->setColumnWidth(RaceModel::GAP, 0.1 * totalW);
            ui->tableView->setColumnWidth(RaceModel::INTERVAL, 0.1 * totalW);
            ui->tableView->setColumnWidth(RaceModel::TIME, 0.12 * totalW);
            ui->tableView->setColumnWidth(RaceModel::S1, 0.09 * totalW);
            ui->tableView->setColumnWidth(RaceModel::S2, 0.09 * totalW);
            ui->tableView->setColumnWidth(RaceModel::S3, 0.09 * totalW);
            ui->tableView->setColumnWidth(RaceModel::PIT, 0.05 * totalW);

            break;

        case LTPackets::PRACTICE_EVENT:

            ui->tableView->setColumnWidth(PracticeModel::POSITION, 0.05 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::NUMBER, 0.1 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::CAR, carsWidth);
            ui->tableView->setColumnWidth(PracticeModel::HELMET, helmetsWidth);
            ui->tableView->setColumnWidth(PracticeModel::NAME, 0.2 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::LAST, 0.12 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::BEST, 0.12 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::GAP, 0.1 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::S1, 0.09 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::S2, 0.09 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::S3, 0.09 * totalW);
            ui->tableView->setColumnWidth(PracticeModel::LAP, 0.05 * totalW);

            break;

        case LTPackets::QUALI_EVENT:

            ui->tableView->setColumnWidth(QualiModel::POSITION, 0.05 * totalW);
            ui->tableView->setColumnWidth(QualiModel::NUMBER, 0.09 * totalW);
            ui->tableView->setColumnWidth(QualiModel::CAR, carsWidth);
            ui->tableView->setColumnWidth(QualiModel::HELMET, helmetsWidth);
            ui->tableView->setColumnWidth(QualiModel::NAME, 0.19 * totalW);
            ui->tableView->setColumnWidth(QualiModel::LAST, 0.1 * totalW);
            ui->tableView->setColumnWidth(QualiModel::Q1, 0.1 * totalW);
            ui->tableView->setColumnWidth(QualiModel::Q2, 0.1 * totalW);
            ui->tableView->setColumnWidth(QualiModel::Q3, 0.1 * totalW);
            ui->tableView->setColumnWidth(QualiModel::S1, 0.07 * totalW);
            ui->tableView->setColumnWidth(QualiModel::S2, 0.07 * totalW);
            ui->tableView->setColumnWidth(QualiModel::S3, 0.07 * totalW);
            ui->tableView->setColumnWidth(QualiModel::LAP, 0.06 * totalW);

            break;
    }
    if (ltModel)
    {
        QFontMetrics f(ui->tableView->font());

        for (int i = 0; i < ltModel->rowCount(); ++i)
            ui->tableView->setRowHeight(i, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
    }
    QWidget::resizeEvent(e);
}

void LTWidget::on_tableView_clicked(const QModelIndex &index)
{    
    if (ltModel)
    {
        if (ltModel->indexInDriverRowsData(index))
        {
            const DriverData *dd = ltModel->getDriverData(index);

            if (dd)
            {
                currDriverId = dd->getCarID();

                ltModel->selectDriver(currDriverId, index.column());
                ltModel->updateLT();

                emit driverSelected(currDriverId);
            }
        }
//        ui->tableView->setCurrentIndex(index);
    }
}

void LTWidget::on_tableView_doubleClicked(const QModelIndex &index)
{
    if (ltModel && ltModel->indexInDriverRowsData(index))
    {
        const DriverData *dd = ltModel->getDriverData(index);
        currDriverId = dd->getCarID();
        emit driverDoubleClicked(currDriverId);
    }
}

void LTWidget::on_tableView_headerClicked(int column)
{
    if (ltModel)
    {
        ltModel->headerClicked(column);

        if (currDriverId > 0)
        {
            QModelIndex index = ltModel->indexOf(currDriverId);
            ui->tableView->setCurrentIndex(index);
        }
    }
}
