/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/



#ifndef PITSTOPSMODEL_H
#define PITSTOPSMODEL_H

#include <QAbstractTableModel>

#include "../../core/eventdata.h"

struct PitStopAtom
{
    double time;
    int lap;
    QString driver;
    int pos;

    bool operator <(const PitStopAtom &psa) const
    {
        return time < psa.time;
    }
};



class PitStopsModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    enum Column
    {
        NO = 0,
        LAP,
        NUMBER,
        NAME,
        TIME,
        POS
    };

    explicit PitStopsModel(QObject *parent = 0);

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count);
        endInsertRows();
        return true;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginRemoveRows(parent, row, row + count - 1);
        endRemoveRows();
        return true;
    }

    QVariant headerData(const QModelIndex & index, int role = Qt::DisplayRole) const;

    void update();

    void headerClicked(int col);


signals:
    
public slots:

private:
    int rows;    

    void getPitstops(const QVector<DriverData> &driversData);
    QList< PitStopAtom > pitStops;

    Column currentColumn;
    
};

class PitStopsLessThan
{
public:
    PitStopsLessThan(int col) : column(col) { }

    bool operator() (const PitStopAtom &ps1, const PitStopAtom &ps2)
    {
        if (column == PitStopsModel::TIME)    //sort by time
        {
            if (ps1.time == ps2.time)
                return ps1.lap < ps2.lap;

            return ps1.time < ps2.time;
        }

        if (column == PitStopsModel::LAP)    //sort by lap
        {
            if (ps1.lap == ps2.lap)
                return ps1.time < ps2.time;

            return ps1.lap < ps2.lap;
        }

        if (column == PitStopsModel::NAME)    //sort by driver
        {
            if (ps1.driver == ps2.driver)
                return ps1.lap < ps2.lap;

            return ps1.driver < ps2.driver;
        }

        return ps1.lap < ps2.lap;
    }

private:
    int column;
};

#endif // PITSTOPSMODEL_H
