/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "qualimodel.h"

#include "../../core/colorsmanager.h"
#include <QDebug>

QualiModel::QualiModel(QObject *parent) : LTModel(parent), eventData(EventData::getInstance()), qualiPeriodSelected(0)
{
    pitStatusIcons[0] = QIcon(QPixmap(":/track/status-pit.png").scaledToHeight(20, Qt::SmoothTransformation));
    pitStatusIcons[1] = QIcon(QPixmap(":/track/status-finish.png").scaledToHeight(20, Qt::SmoothTransformation));
}

int QualiModel::rowCount(const QModelIndex &) const
{
    return eventData.getDriversData().size() + 3;
}

int QualiModel::columnCount(const QModelIndex &) const
{
    return LAP+1;
}

void QualiModel::headerClicked(int column)
{
    int col = column - Q1 + 1;

    if (col == eventData.getQualiPeriod() || col == 3)//col >= 1 && col <= 2)
    {
        if (qualiPeriodSelected == 0)
            qualiPeriodSelected = col * 10;

        else
            qualiPeriodSelected = 0;

        updateLT();
    }
    else if (col == 1 || col == 2)
    {
        if (qualiPeriodSelected == col)
            qualiPeriodSelected = col * 10;

        else if (qualiPeriodSelected == col * 10)
            qualiPeriodSelected = 0;

        else
            qualiPeriodSelected = col;

        updateLT();
    }
}

void QualiModel::updateLT()
{
    //driversData = QList<DriverData>(EventData::getInstance().getDriversData());
    gatherDriversData();
    int qPeriod = qualiPeriodSelected >= 10 ? qualiPeriodSelected / 10 : qualiPeriodSelected;

    qSort(driversData.begin(), driversData.end(),
          QualiLessThan(qPeriod, EventData::getInstance().getSessionRecords().getQualiBestTime(1).calc107p()));

    QModelIndex topLeft = QAbstractTableModel::index(firstRow(), 0);
    QModelIndex bottomRight = QAbstractTableModel::index(rowCount()-1, columnCount()-1);

    emit dataChanged(topLeft, bottomRight);
}

void QualiModel::updateLT(const DataUpdates &dataUpdates)
{
    //driversData = QList<DriverData>(EventData::getInstance().getDriversData());

    if (dataUpdates.postionChanged || (selectedDriver.first > 0 && dataUpdates.driverIds.contains(selectedDriver.first)))
    {
        updateLT();
    }
    else
    {
        QModelIndex topLeft, bottomRight;
        QSetIterator<int> iter(dataUpdates.driverIds);

        if (iter.hasNext())
        {
            QModelIndex index = indexOf(iter.next());
            topLeft = index.sibling(index.row(), 0);

            bottomRight = index.sibling(index.row(), columnCount()-1);

            while (iter.hasNext())
            {
                index = indexOf(iter.next());

                if (index.row() <= topLeft.row())
                    topLeft = index;

                if (index.row() >= bottomRight.row())
                {
                    bottomRight = index;
                    bottomRight = bottomRight.sibling(bottomRight.row(), columnCount()-1);
                }

            }
            if (topLeft.row() > -1 && bottomRight.row() > -1)
            {
                emit dataChanged(topLeft, bottomRight);
            }
        }
    }
}

QVariant QualiModel::data(const QModelIndex &index, int role) const
{
    int row = index.row()-1;

    if (row >= 0 && row < driversData.size())
    {
        DriverData &dd = *driversData[row];//eventData.getDriverDataByPos(row);
        return driverRowData(dd, index, role);

    }
    else if (index.row() == 0)
        return headerRowData(index, role);

    else
        return extraRowData(index, role);
}

QVariant QualiModel::driverRowData(const DriverData &dd, const QModelIndex &index, int role) const
{
    switch (index.column())
    {
        case POSITION:
            if (role == Qt::DisplayRole)                            
                return QString("%1.").arg(index.row());//dd.getPosition());


            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().positionColor());


            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            break;

        case NUMBER:
            if (role == Qt::DisplayRole)
                return dd.getNumber();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numberColor());

            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            if (role == Qt::DecorationRole)
                return getIcon(dd);

            break;

        case CAR:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case HELMET:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case NAME:
            if (role == Qt::DisplayRole)
                return dd.getDriverName();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().driverColor());

            break;

        case LAST:
            if ((role == Qt::DisplayRole || role == Qt::ForegroundRole) && !dd.getLapData().isEmpty())
            {
                LapTime lt = dd.getLapData().last().getTime();

                int i = dd.getLapData().size() - 2;
                while (lt.toString().contains("LAP") && i >= -1)
                {
                    if (i == -1)
                        lt = LapTime();
                    else
                        lt = dd.getLapData()[i].getTime();

                    --i;
                }

                if (role == Qt::DisplayRole)
                {
                    if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                        return gapToSelected(dd, LAST);

                    return lt.toString();
                }

                if (role == Qt::ForegroundRole)
                {
                    if (lt == dd.getQualiTime(eventData.getQualiPeriod()))
                            return ColorsManager::getInstance().getColor(LTPackets::GREEN);

                    return ColorsManager::getInstance().getColor(LTPackets::WHITE);
                }
            }
            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
        break;

        case Q1:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                    return gapToSelected(dd, Q1);

                if (qualiPeriodSelected == 10 && &dd != driversData.first())
                    return DriverData::calculateGap(dd.getQualiTime(1), driversData.first()->getQualiTime(1));

                return dd.getQualiTime(1).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().qualiTimeColor(1));
            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            break;

        case Q2:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                    return gapToSelected(dd, Q2);

                if (qualiPeriodSelected == 20 && &dd != driversData.first())
                    return DriverData::calculateGap(dd.getQualiTime(2), driversData.first()->getQualiTime(2));

                return dd.getQualiTime(2).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().qualiTimeColor(2));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case Q3:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                    return gapToSelected(dd, Q3);

                if (qualiPeriodSelected == 30 && &dd != driversData.first())
                    return DriverData::calculateGap(dd.getQualiTime(3), driversData.first()->getQualiTime(3));

                return dd.getQualiTime(3).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().qualiTimeColor(3));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            break;

            break;

        case S1:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(1).isValid())
                    return gapToSelected(dd, S1);

                return dd.getLastLap().getSectorTime(1).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(1));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S2:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(2).isValid())
                    return gapToSelected(dd, S2);

                return dd.getLastLap().getSectorTime(2).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(2));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S3:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(3).isValid())
                    return gapToSelected(dd, S3);

                return dd.getLastLap().getSectorTime(3).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(3));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case LAP:
            if (role == Qt::DisplayRole && dd.getLastLap().getLapNumber() > 0)
                return dd.getLastLap().getLapNumber();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numLapsColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;
    }
    return QVariant();
}

QVariant QualiModel::headerRowData(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case POSITION: return "P";
            case NAME: return "Name";
            case LAST: return "Last";
            case Q1: return "Q1";
            case Q2: return "Q2";
            case Q3: return "Q3";
            case S1: return "S1";
            case S2: return "S2";
            case S3: return "S3";
            case LAP: return "Lap";
            default: return "";
        }
    }
    if (role == Qt::ForegroundRole)
    {
        if (index.column() >= Q1 && index.column() <= Q3)
        {
            int col = index.column() - Q1 + 1;
            if (col == qualiPeriodSelected)
                return ColorsManager::getInstance().getColor(LTPackets::WHITE);

            if (col * 10 == qualiPeriodSelected)
                return ColorsManager::getInstance().getColor(LTPackets::YELLOW);
        }
        return ColorsManager::getInstance().getColor(LTPackets::DEFAULT);
    }

    if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= POSITION && index.column() <= NUMBER)
            return (int)(Qt::AlignVCenter | Qt::AlignRight);

        if (index.column() >= LAST)
            return Qt::AlignCenter;

    }

    return QVariant();
}

QVariant QualiModel::extraRowData(const QModelIndex &index, int role) const
{
    if (index.row() == rowCount()-2)
        return QVariant();

    if (role == Qt::DisplayRole && !driversData.isEmpty() && driversData.first()->getQualiTime(1).isValid())
    {
        switch (index.column())
        {
            case NAME: return "Q1 107% time";
            case Q1:
            {
                LapTime q1Best = EventData::getInstance().getSessionRecords().getQualiBestTime(1);

                if (q1Best.isValid())
                    return q1Best.calc107p().toString();
            }

            case Q2:
            {
                LapTime q1Best = EventData::getInstance().getSessionRecords().getQualiBestTime(1);

                if (q1Best.isValid())
                    return QString("+%1").arg(DriverData::calculateGap(q1Best.calc107p(), q1Best));
            }
        }
    }
    else if (role == Qt::ForegroundRole)
        return ColorsManager::getInstance().getColor(LTPackets::RED);

    if (role == Qt::TextAlignmentRole && index.column() >= LAST)
        return Qt::AlignCenter;

    return QVariant();
}

QVariant QualiModel::gapToSelected(const DriverData &dd, int column) const
{
    int qPeriod = column - Q1 + 1;

    DriverData currD = eventData.getDriverDataById(selectedDriver.first);

    if (column == LAST)
    {
        if (!dd.getLapData().isEmpty() && !currD.getLapData().isEmpty())
        {
            LapTime lt = dd.getLapData().last().getTime();

            int i = dd.getLapData().size() - 2;
            while (lt.toString().contains("LAP") && i >= -1)
            {
                if (i == -1)
                    lt = LapTime();
                else
                    lt = dd.getLapData()[i].getTime();

                --i;
            }

            LapTime currLT = currD.getLapData().last().getTime();

            i = currD.getLapData().size() - 2;
            while (currLT.toString().contains("LAP") && i >= -1)
            {
                if (i == -1)
                    currLT = LapTime();
                else
                    currLT = currD.getLapData()[i].getTime();

                --i;
            }


            if ((selectedDriver.first == dd.getCarID()) || !currLT.isValid())
                return lt.toString();

            QString gap = DriverData::calculateGap(lt, currLT);

            if (gap.size() > 0 && gap[0] != '-')
                gap = "+" + gap;

            return gap;
        }
    }

    if (qPeriod > 0 && qPeriod < 4)
    {
        if ((selectedDriver.first == dd.getCarID()) || !currD.getQualiTime(qPeriod).isValid())
            return dd.getQualiTime(qPeriod).toString();

        QString gap = DriverData::calculateGap(dd.getQualiTime(qPeriod), currD.getQualiTime(qPeriod));

        if (gap.size() > 0 && gap[0] != '-')
            gap = "+" + gap;

        return gap;
    }

    if (column >= S1)
    {
        int sector = column - S1 + 1;

        if ((selectedDriver.first == dd.getCarID()) || !currD.getLastLap().getSectorTime(sector).isValid())
            return dd.getLastLap().getSectorTime(sector).toString();

        else
        {
            QString gap = DriverData::calculateGap(dd.getLastLap().getSectorTime(sector), currD.getLastLap().getSectorTime(sector));

            if ((gap.size() > 0) && (gap[0] != '-') && (gap != "0.000"))
                gap = "+" + gap;

            return gap.left(gap.size()-2);
        }
    }

    return "";
}

QVariant QualiModel::getIcon(const DriverData &dd) const
{
    if (dd.getNumber() > 0)
    {
        if (dd.isInPits())
            return pitStatusIcons[0];

        if ((eventData.isSessionFinished() || eventData.isQualiBreak()) && !dd.getLapData().isEmpty() &&
            dd.getLapData().last().getPracticeLapExtraData().getSessionTime().toString("h:mm:ss") == "0:00:00")
            return pitStatusIcons[1];
    }

    return QVariant();
}
