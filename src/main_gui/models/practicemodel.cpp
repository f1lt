/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "practicemodel.h"

#include "../../core/colorsmanager.h"

PracticeModel::PracticeModel(QObject *parent) : LTModel(parent), eventData(EventData::getInstance())
{
    pitStatusIcons[0] = QIcon(QPixmap(":/track/status-pit.png").scaledToHeight(20, Qt::SmoothTransformation));
    pitStatusIcons[1] = QIcon(QPixmap(":/track/status-finish.png").scaledToHeight(20, Qt::SmoothTransformation));
}


int PracticeModel::rowCount(const QModelIndex &) const
{
    return eventData.getDriversData().size() + 3;
}

int PracticeModel::columnCount(const QModelIndex &) const
{
    return LAP + 1;
}

QVariant PracticeModel::data(const QModelIndex &index, int role) const
{
    int row = index.row()-1;

    if (row >= 0 && row < driversData.size())
    {
        DriverData &dd = *driversData[row];//eventData.getDriverDataByPos(row);
        return driverRowData(dd, index, role);

    }
    else if (index.row() == 0)
        return headerRowData(index, role);

    else
        return extraRowData(index, role);
}

QVariant PracticeModel::driverRowData(const DriverData &dd, const QModelIndex &index, int role) const
{
    switch (index.column())
    {
        case POSITION:
            if (role == Qt::DisplayRole)
                return QString("%1.").arg(dd.getPosition());

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().positionColor());

            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            break;

        case NUMBER:
            if (role == Qt::DisplayRole)
                return dd.getNumber();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numberColor());

            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            if (role == Qt::DecorationRole)
                return getIcon(dd);

            break;

        case CAR:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case HELMET:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case NAME:
            if (role == Qt::DisplayRole)
                return dd.getDriverName();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().driverColor());

            break;

        case LAST:
            if ((role == Qt::DisplayRole || role == Qt::ForegroundRole) && !dd.getLapData().isEmpty())
            {
                LapTime lt = dd.getLapData().last().getTime();

                int i = dd.getLapData().size() - 2;
                while (lt.toString().contains("LAP") && i >= -1)
                {
                    if (i == -1)
                        lt = LapTime();
                    else
                        lt = dd.getLapData()[i].getTime();

                    --i;
                }

                if (role == Qt::DisplayRole)
                {
                    if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                        return gapToSelected(dd, LAST);

                    return lt.toString();
                }

                if (role == Qt::ForegroundRole)
                {
                    if (lt == dd.getSessionRecords().getBestLap().getTime())
                            return ColorsManager::getInstance().getColor(LTPackets::GREEN);

                    return ColorsManager::getInstance().getColor(LTPackets::WHITE);
                }
            }
            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
        break;

        case BEST:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST)
                    return gapToSelected(dd, BEST);

                return dd.getLastLap().getTime().toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().lapTimeColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            break;

        case GAP:
            if (role == Qt::DisplayRole)
            {                
                if (dd.getPosition() == 1)
                    return "";

                return dd.getLastLap().getGap();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().gapColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S1:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(1).isValid())
                    return gapToSelected(dd, S1);

                return dd.getLastLap().getSectorTime(1).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(1));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S2:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(2).isValid())
                    return gapToSelected(dd, S2);

                return dd.getLastLap().getSectorTime(2).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(2));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S3:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= LAST && dd.getLastLap().getSectorTime(3).isValid())
                    return gapToSelected(dd, S3);

                return dd.getLastLap().getSectorTime(3).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(3));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case LAP:
            if (role == Qt::DisplayRole && dd.getLastLap().getLapNumber() > 0)
                return dd.getLastLap().getLapNumber();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numLapsColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;
    }
    return QVariant();
}

QVariant PracticeModel::headerRowData(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case POSITION: return "P";
            case NAME: return "Name";
            case LAST: return "Last";
            case BEST: return "Best";
            case GAP: return "Gap";
            case S1: return "S1";
            case S2: return "S2";
            case S3: return "S3";
            case LAP: return "Lap";
            default: return "";
        }
    }
    if (role == Qt::ForegroundRole)
        return ColorsManager::getInstance().getColor(LTPackets::DEFAULT);

    if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= POSITION && index.column() <= NUMBER)
            return (int)(Qt::AlignVCenter | Qt::AlignRight);

        if (index.column() >= LAST)
            return Qt::AlignCenter;

    }

    return QVariant();
}

QVariant PracticeModel::extraRowData(const QModelIndex &index, int role) const
{
    if (index.row() == rowCount()-2)
        return QVariant();

    if (!driversData.isEmpty() && role == Qt::DisplayRole && driversData.first()->getSessionRecords().getBestLap().getTime().isValid() && !driversData.isEmpty())
    {
        switch (index.column())
        {
            case NAME: return "107% time";
            case BEST:
            {
                LapTime lt = driversData.first()->getSessionRecords().getBestLap().getTime();

                if (lt.isValid())
                    return lt.calc107p().toString();
            }
            case GAP:
            {
                LapTime lt = driversData.first()->getSessionRecords().getBestLap().getTime();
                if (lt.isValid())
                    return QString("+%1").arg(DriverData::calculateGap(lt.calc107p(), lt));
            }
        }
    }
    else if (role == Qt::ForegroundRole)
        return ColorsManager::getInstance().getColor(LTPackets::RED);

    if (role == Qt::TextAlignmentRole && ((index.column() == BEST) || (index.column() == GAP)))
        return Qt::AlignCenter;

    return QVariant();
}

QVariant PracticeModel::gapToSelected(const DriverData &dd, int column) const
{        
    DriverData currD = eventData.getDriverDataById(selectedDriver.first);

    if (column == LAST)
    {
        if (!dd.getLapData().isEmpty() && !currD.getLapData().isEmpty())
        {
            LapTime lt = dd.getLapData().last().getTime();

            int i = dd.getLapData().size() - 2;
            while (lt.toString().contains("LAP") && i >= -1)
            {
                if (i == -1)
                    lt = LapTime();
                else
                    lt = dd.getLapData()[i].getTime();

                --i;
            }

            LapTime currLT = currD.getLapData().last().getTime();

            i = currD.getLapData().size() - 2;
            while (currLT.toString().contains("LAP") && i >= -1)
            {
                if (i == -1)
                    currLT = LapTime();
                else
                    currLT = currD.getLapData()[i].getTime();

                --i;
            }


            if ((selectedDriver.first == dd.getCarID()) || !currLT.isValid())
                return lt.toString();

            QString gap = DriverData::calculateGap(lt, currLT);

            if (gap.size() > 0 && gap[0] != '-')
                gap = "+" + gap;

            return gap;
        }
    }

    if (column == BEST)
    {
        if ((selectedDriver.first == dd.getCarID()) || !currD.getLastLap().getTime().isValid())
            return dd.getLastLap().getTime().toString();

        QString gap = DriverData::calculateGap(dd.getLastLap().getTime(), currD.getLastLap().getTime());

        if (gap.size() > 0 && gap[0] != '-')
            gap = "+" + gap;

        return gap;
    }

    if (column >= S1)
    {
        int sector = column - S1 + 1;

        if ((selectedDriver.first == dd.getCarID()) || !currD.getLastLap().getSectorTime(sector).isValid())
            return dd.getLastLap().getSectorTime(sector).toString();

        else
        {
            QString gap = DriverData::calculateGap(dd.getLastLap().getSectorTime(sector), currD.getLastLap().getSectorTime(sector));

            if ((gap.size() > 0) && (gap[0] != '-') && (gap != "0.000"))
                gap = "+" + gap;

            return gap.left(gap.size()-2);
        }
    }

    return "";
}

QVariant PracticeModel::getIcon(const DriverData &dd) const
{
    if (dd.getNumber() > 0)
    {
        if (dd.isInPits())
            return pitStatusIcons[0];

        if (eventData.isSessionFinished() && !dd.getLapData().isEmpty() &&
            dd.getLapData().last().getPracticeLapExtraData().getSessionTime().toString("h:mm:ss") == "0:00:00")
            return pitStatusIcons[1];
    }

    return QVariant();
}
