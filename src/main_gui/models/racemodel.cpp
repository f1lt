/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "racemodel.h"

#include "../../core/colorsmanager.h"
#include <QPixmap>

RaceModel::RaceModel(QObject *parent) : LTModel(parent), eventData(EventData::getInstance())
{
    statusIcons[0] = QIcon(QPixmap(":/track/up-arrow.png").scaledToHeight(10, Qt::SmoothTransformation));
    statusIcons[1] = QIcon(QPixmap(":/track/down-arrow.png").scaledToHeight(10, Qt::SmoothTransformation));
    statusIcons[2] = QIcon(QPixmap(":/track/status-pit.png").scaledToHeight(20, Qt::SmoothTransformation));
    statusIcons[3] = QIcon(QPixmap(":/track/status-finish.png").scaledToHeight(20, Qt::SmoothTransformation));
}

int RaceModel::rowCount(const QModelIndex &) const
{    
    return eventData.getDriversData().size() + 3;
}

int RaceModel::columnCount(const QModelIndex &) const
{
    return PIT + 1;
}

QVariant RaceModel::data(const QModelIndex &index, int role) const
{
    int row = index.row()-1;

    if (row >= 0 && row < driversData.size() && driversData[row] != 0)
    {
        DriverData &dd = *driversData[row];//eventData.getDriverDataByPos(row);
        return driverRowData(dd, index, role);

    }
    else if (index.row() == 0)
        return headerRowData(index, role);

    else
        return extraRowData(index, role);
}

QVariant RaceModel::driverRowData(const DriverData &dd, const QModelIndex &index, int role) const
{
    switch (index.column())
    {
        case POSITION:
            if (role == Qt::DisplayRole && dd.getPosition() > 0 && !dd.isRetired())
            {
                return QString("%1.").arg(dd.getPosition());
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().positionColor());

            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            break;

        case NUMBER:
            if (role == Qt::DisplayRole && dd.getPosition() > 0)
                return dd.getNumber();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numberColor());

            if (role == Qt::TextAlignmentRole)
                return (int)(Qt::AlignVCenter | Qt::AlignRight);

            if (role == Qt::DecorationRole)
                return getIcon(dd);

            break;

        case CAR:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case HELMET:

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case NAME:
            if (role == Qt::DisplayRole)
                return dd.getDriverName();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().driverColor());

            break;

        case GAP:
            if (role == Qt::DisplayRole)
                return dd.getLastLap().getGap();


            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().gapColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;
            break;

        case INTERVAL:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= INTERVAL)
                    return gapToSelected(dd, INTERVAL);

                return dd.getLastLap().getInterval();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().intervalColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case TIME:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= INTERVAL && dd.getLastLap().getTime().isValid())
                    return gapToSelected(dd, TIME);

                return dd.getLastLap().getTime().toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().lapTimeColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            if (role == Qt::ToolTipRole)
                return "Best lap: " + dd.getSessionRecords().getBestLap().getTime().toString() + QString(" (L%1)").arg(dd.getSessionRecords().getBestLap().getLapNumber());

            break;

            break;

        case S1:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= INTERVAL && dd.getLastLap().getSectorTime(1).isValid())
                    return gapToSelected(dd, S1);

                return dd.getLastLap().getSectorTime(1).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(1));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S2:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= INTERVAL && dd.getLastLap().getSectorTime(2).isValid())
                    return gapToSelected(dd, S2);

                return dd.getLastLap().getSectorTime(2).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(2));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case S3:
            if (role == Qt::DisplayRole)
            {
                if (selectedDriver.first != 0 && selectedDriver.second >= INTERVAL && dd.getLastLap().getSectorTime(3).isValid())
                    return gapToSelected(dd, S3);

                return dd.getLastLap().getSectorTime(3).toString();
            }

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().sectorColor(3));

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;

        case PIT:
            if (role == Qt::DisplayRole && dd.getNumPits() > 0)
                return dd.getNumPits();

            if (role == Qt::ForegroundRole)
                return ColorsManager::getInstance().getColor(dd.getColorData().numLapsColor());

            if (role == Qt::TextAlignmentRole)
                return Qt::AlignCenter;

            break;
    }
    return QVariant();
}

QVariant RaceModel::headerRowData(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case POSITION: return "P";
            case NAME: return "Name";
            case GAP: return "Gap";
            case INTERVAL: return "Interval";
            case TIME: return "Time";
            case S1: return "S1";
            case S2: return "S2";
            case S3: return "S3";
            case PIT: return "Pit";
            default: return "";
        }
    }
    if (role == Qt::ForegroundRole)
        return ColorsManager::getInstance().getColor(LTPackets::DEFAULT);

    if (role == Qt::TextAlignmentRole)
    {
        if (index.column() >= POSITION && index.column() <= NUMBER)
            return (int)(Qt::AlignVCenter | Qt::AlignRight);

        if (index.column() > NAME)
            return Qt::AlignCenter;

    }

    return QVariant();
}

QVariant RaceModel::extraRowData(const QModelIndex &index, int role) const
{
    if (index.row() == rowCount()-2)
        return QVariant();

    if (role == Qt::DisplayRole && eventData.getSessionRecords().getFastestLap().getDriverName() != "")
    {
        switch (index.column())
        {
            case NUMBER: return "FL:";
            case NAME: return eventData.getSessionRecords().getFastestLap().getDriverName();
            case GAP: return "lap";
            case INTERVAL: return eventData.getSessionRecords().getFastestLap().getLapNumber();
            case TIME: return eventData.getSessionRecords().getFastestLap().getTime().toString();
        }
    }
    else if (role == Qt::ForegroundRole && (index.column() == NAME || index.column() == INTERVAL || index.column() == TIME))
        return ColorsManager::getInstance().getColor(LTPackets::VIOLET);

    else if (role == Qt::ForegroundRole && (index.column() == NUMBER || index.column() == GAP))
        return ColorsManager::getInstance().getColor(LTPackets::DEFAULT);

    if (role == Qt::TextAlignmentRole && index.column() == NUMBER)
        return (int)(Qt::AlignVCenter | Qt::AlignRight);

    if (role == Qt::TextAlignmentRole && (index.column() >= GAP && index.column() <= TIME))
        return Qt::AlignCenter;

    return QVariant();
}

QVariant RaceModel::gapToSelected(const DriverData &dd, int column) const
{    
    DriverData currD = eventData.getDriverDataById(selectedDriver.first);

    if (column == TIME)
    {        
        if (selectedDriver.first == dd.getCarID() || !currD.getLastLap().getTime().isValid() || !dd.getLastLap().getTime().isValid())
            return dd.getLastLap().getTime().toString();

        QString gap = DriverData::calculateGap(dd.getLastLap().getTime(), currD.getLastLap().getTime());

        if (gap.size() > 0 && gap[0] != '-')
            gap = "+" + gap;

        return gap;
    }

    if (column == INTERVAL)
    {        
        if (selectedDriver.first == dd.getCarID())
            return "";

        return eventData.calculateInterval(dd, currD, -1);//EventData.getInstance().lapsCompleted);//.driversData.get(currentCar-1).lastLap.numLap);
    }

    if (column >= S1)
    {
        int sector = column - S1 + 1;

        if ((selectedDriver.first == dd.getCarID()) || !currD.getLastLap().getSectorTime(sector).isValid())
            return dd.getLastLap().getSectorTime(sector).toString();

        else
        {
            QString gap = DriverData::calculateGap(dd.getLastLap().getSectorTime(sector), currD.getLastLap().getSectorTime(sector));

            if ((gap.size() > 0) && (gap[0] != '-') && (gap != "0.000"))
                gap = "+" + gap;

            return gap.left(gap.size()-2);
        }
    }

    return "";
}

QVariant RaceModel::getIcon(const DriverData &dd) const
{
    if (!dd.isRetired() && dd.getNumber() > 0)
    {
        if (dd.isInPits())
            return statusIcons[2];

        if (eventData.getCompletedLaps() == eventData.getEventInfo().laps)
        {
            QString gap = dd.getLastLap().getGap();
            int lapsBehindLeader = 0;
            if (gap.contains("L"))
            {
                lapsBehindLeader = gap.left(gap.size()-1).toInt();
            }
            if ((dd.getLastLap().getLapNumber() + lapsBehindLeader) >= eventData.getCompletedLaps())
                return statusIcons[3];
        }

        int lastPos = 0, prevPos = 0;
        if (dd.getLapData().size() > 1)
        {
            lastPos = dd.getLastLap().getPosition();
            prevPos = dd.getLapData()[dd.getLapData().size()-2].getPosition();
        }
        else if (!dd.getPositionHistory().isEmpty())
        {
            lastPos = dd.getLastLap().getPosition();
            prevPos = dd.getPositionHistory()[dd.getLastLap().getLapNumber() == 1 ? 0 : dd.getPositionHistory().size()-1];
        }
        if (lastPos != prevPos)
            return (lastPos < prevPos ? statusIcons[0] : statusIcons[1]);
    }
    return QVariant();
}
