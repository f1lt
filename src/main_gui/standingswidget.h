/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef STANDINGSWIDGET_H
#define STANDINGSWIDGET_H

#include <QWidget>
#include <QTableWidgetItem>

#include "../core/colorsmanager.h"
#include "../core/ltpackets.h"
#include "../net/packetparser.h"
#include "../net/standingsmanager.h"

namespace Ui {
class StandingsWidget;
}

class StandingsWidget : public QWidget
{
    Q_OBJECT

public:

    enum CurrentTab
    {
        DRIVERS_TABLE = 0,
        TEAMS_TABLE
    };

    explicit StandingsWidget(QWidget *parent = 0);
    ~StandingsWidget();

    void gatherPoints();

    QTableWidgetItem* setItem(QTableWidget *table, int row, int col, QString text = "", Qt::ItemFlags flags = Qt::NoItemFlags, int align = Qt::AlignCenter,
                     QColor textColor = ColorsManager::getInstance().getColor(LTPackets::DEFAULT), QBrush background = QBrush());

    void setFont(const QFont &font);
    void setConnected(bool c)
    {
        connected = c;
    }

protected:
    void resizeEvent(QResizeEvent *);

public slots:
    void update(const DataUpdates &dataUpdates);
    void updateDriversTable();
    void updateTeamsTable();

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_driversTableWidget_clicked(const QModelIndex &index);

    void on_teamsTableWidget_clicked(const QModelIndex &index);

private:
    Ui::StandingsWidget *ui;
    QPixmap upArrowPixmap;
    QPixmap downArrowPixmap;

    Driver *selectedDriver;
    Team *selectedTeam;
    bool connected;
};

#endif // STANDINGSWIDGET_H
