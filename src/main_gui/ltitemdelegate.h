/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/



#ifndef LTITEMDELEGATE_H
#define LTITEMDELEGATE_H

#include <QItemDelegate>

#include "../core/eventdata.h"

class LTItemDelegate: public QItemDelegate
{
public:
    LTItemDelegate(QObject* parent = 0) : QItemDelegate(parent)
    {
    }

    void paint(QPainter* painter, const QStyleOptionViewItem& rOption, const QModelIndex& rIndex) const;
};

class LTMainItemDelegate : public LTItemDelegate
{
public:
    LTMainItemDelegate(QObject* parent = 0, bool drawC = true, bool drawH = true, int tsize = 75, int hsize = 18) :
        LTItemDelegate(parent), drawCars(drawC), drawHelmets(drawH), thumbnailsSize(tsize), helmetSize(hsize)
    {
    }

    void setDrawCarThumbnails(bool val)
    {
        drawCars = val;
    }

    void setDrawHelmets(bool val)
    {
        drawHelmets = val;
    }

    void paint(QPainter* painter, const QStyleOptionViewItem& rOption, const QModelIndex& rIndex) const;


private:
    bool drawCars;
    bool drawHelmets;
    int thumbnailsSize;
    int helmetSize;
};

#endif // LTITEMDELEGATE_H
