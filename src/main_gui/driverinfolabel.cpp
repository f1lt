/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "driverinfolabel.h"
#include <QPainter>

#include <QDebug>

#include "../core/colorsmanager.h"

DriverInfoLabel::DriverInfoLabel(QWidget *parent) :
    QWidget(parent), driverData(0)
{
    backgroundPixmap = QPixmap(":/ui_icons/label-small.png");
}

QSize DriverInfoLabel::sizeHint()
{
    return backgroundPixmap.size();
}

QSize DriverInfoLabel::minimumSize()
{
    return backgroundPixmap.size();
}

void DriverInfoLabel::update()
{
    if (height() < sizeHint().height())
    {
        QSize size = backgroundPixmap.size();
        size.setHeight(size.height() + 10);
        setMinimumSize(size);
        updateGeometry();
    }
    repaint();
}

void DriverInfoLabel::paintEvent(QPaintEvent *)
{
    if (driverData == 0)
        return;

    QPainter p;
    p.begin(this);
    int x = (width() - backgroundPixmap.width())/2;

    QColor color = /*QColor(170, 170, 170);*/ColorsManager::getInstance().getCarColor(driverData->getNumber());

    int numX = x + 105;
    int numY = 1;

    numX = x+10;
    numY = 32;

    //p.drawPixmap(x, 0, backgroundPixmap);

    QString txt = QString::number(driverData->getPosition());

    //if (driverData->getPosition() < 10)
    txt = QString("%1").arg(txt, 2, ' ');

    p.setFont(QFont("Arial", 30, 100));
    p.setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW)));

    if (driverData->isInPits())
        p.setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::PIT)));

    p.drawText(numX, numY, txt);

    txt = driverData->getDriverName();
    p.setPen(QColor(255, 255, 255));
    p.setFont(QFont("Arial", 16, 100));
    numX = x + 70;
    numY = 15;
    p.drawText(numX, numY, txt);

    p.setFont(QFont("Arial", 10, 100));

    numX = x + 90;
    numY = 35;

    p.setPen(QColor(170, 170, 170));//ColorsManager::getInstance().getDefaultColor(LTPackets::WHITE)));
    txt = SeasonData::getInstance().getTeamName(driverData->getDriverName());
    p.drawText(numX, numY, txt);


    txt = QString::number(driverData->getNumber());
    if (driverData->getNumber() < 10)
        txt = " " + txt;

    numX = x + 70;
    numY = 35;

    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen(QColor());
    p.setBrush(color);
    p.drawEllipse(QPoint(numX+7, numY-5), 9, 9);

    if (qGray(color.rgb()) < 70.0f)
        p.setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::WHITE)));

    p.drawText(numX, numY, txt);


    numX = x + 250;
    numY = 2;
    p.drawPixmap(numX, numY, ImagesFactory::getInstance().getHelmetsFactory().getHelmet(driverData->getNumber(), 32));
}
