/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivercolorsdialog.h"
#include "ui_drivercolorsdialog.h"

#include <QColorDialog>
#include <QDebug>
#include <QLabel>
#include <QToolButton>

#include "../core/colorsmanager.h"
#include "../core/eventdata.h"

DriverColorsDialog::DriverColorsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DriverColorsDialog)
{
    ui->setupUi(this);
}

DriverColorsDialog::~DriverColorsDialog()
{
    delete ui;
}

int DriverColorsDialog::exec()
{
    SeasonData &sd = SeasonData::getInstance();
    QHash<int, QColor> colors = ColorsManager::getInstance().getDriverColors();

    clear();
    int i = 0;
    for (; i < sd.getTeams().size(); ++i)
    {
        QList<LTDriver> drivers = sd.getMainDrivers(sd.getTeams()[i]);
        if (drivers.size() != 2)
            continue;

        if ((ui->verticalLayout->count()-2) <= i)
        {
            TeamButtons tButtons;
            QHBoxLayout *layout = new QHBoxLayout();
            QLabel *label = new QLabel(this);                        

            label->setText(QString("%1 %2").arg(drivers[0].no).arg(drivers[0].name));
            label->updateGeometry();
            QToolButton *button = new QToolButton(this);            
            button->setMaximumHeight(16);
            button->setMaximumWidth(16);
            setButtonColor(button, /*sd.getCarColor(sd.getTeams()[i].driver1No)*/colors[drivers[0].no]);
            layout->addWidget(button);
            connect(button, SIGNAL(clicked()), this, SLOT(onColorButtonClicked()));

            DriverButton drvButton;
            drvButton.button = button;
            drvButton.color = colors[drivers[0].no];
            drvButton.label = label;
            drvButton.no = drivers[0].no;

            button = new QToolButton(this);
            button->setText("Reset");
            button->setMaximumHeight(20);
            layout->addWidget(button);
            connect(button, SIGNAL(clicked()), this, SLOT(onResetButtonClicked()));

            drvButton.resetButton = button;
            tButtons.driverButtons.append(drvButton);

            layout->addWidget(label);

//            layout = new QHBoxLayout(this);
            label = new QLabel(this);
            label->setText(QString("%1 %2").arg(drivers[1].no).arg(drivers[1].name));
            label->updateGeometry();
            button = new QToolButton(this);            
            button->setMaximumHeight(16);
            button->setMaximumWidth(16);
            setButtonColor(button, /*sd.getCarColor(sd.getTeams()[i].driver2No)*/colors[drivers[1].no]);
            layout->addWidget(button);
            connect(button, SIGNAL(clicked()), this, SLOT(onColorButtonClicked()));

            drvButton.button = button;
            drvButton.color = colors[drivers[1].no];
            drvButton.label = label;
            drvButton.no = drivers[1].no;

            button = new QToolButton(this);
            button->setText("Reset");
            button->setMaximumHeight(20);
            layout->addWidget(button);
            connect(button, SIGNAL(clicked()), this, SLOT(onResetButtonClicked()));

            drvButton.resetButton = button;
            tButtons.driverButtons.append(drvButton);

            layout->addWidget(label);

            teamButtons.append(tButtons);

            ui->verticalLayout->insertLayout(ui->verticalLayout->count() - 2, layout);
        }
//        else
//        {
//            QHBoxLayout *layout = static_cast<QHBoxLayout*>(ui->verticalLayout->itemAt(i)->layout());
//            QLabel *label = static_cast<QLabel*>(layout->itemAt(2)->widget());
//            QToolButton *button = static_cast<QToolButton*>(layout->itemAt(0)->widget());

//            label->setText(QString("%1 %2").arg(drivers[0].no).arg(drivers[0].name));
//            setButtonColor(button, ColorsManager::getInstance().getCarColor(drivers[0].no));

//            label->setVisible(true);
//            button->setVisible(true);

//            button = static_cast<QToolButton*>(layout->itemAt(1)->widget());
//            button->setVisible(true);

//            label = static_cast<QLabel*>(layout->itemAt(5)->widget());
//            button = static_cast<QToolButton*>(layout->itemAt(3)->widget());

//            label->setText(QString("%1 %2").arg(drivers[1].no).arg(drivers[1].name));
//            setButtonColor(button, ColorsManager::getInstance().getCarColor(drivers[1].no));

//            label->setVisible(true);
//            button->setVisible(true);

//            button = static_cast<QToolButton*>(layout->itemAt(4)->widget());
//            button->setVisible(true);
//        }
    }
//    for (; i < ui->verticalLayout->count()-2; ++i)
//    {
//        QHBoxLayout *layout = static_cast<QHBoxLayout*>(ui->verticalLayout->itemAt(i)->layout());
//        QLabel *label = static_cast<QLabel*>(layout->itemAt(2)->widget());
//        QToolButton *button = static_cast<QToolButton*>(layout->itemAt(0)->widget());

//        label->setVisible(false);
//        button->setVisible(false);

//        button = static_cast<QToolButton*>(layout->itemAt(1)->widget());
//        button->setVisible(false);

//        label = static_cast<QLabel*>(layout->itemAt(5)->widget());
//        button = static_cast<QToolButton*>(layout->itemAt(3)->widget());

//        label->setVisible(false);
//        button->setVisible(false);

//        button = static_cast<QToolButton*>(layout->itemAt(4)->widget());
//        button->setVisible(false);
//    }

    return QDialog::exec();
}

void DriverColorsDialog::onColorButtonClicked()
{
    QToolButton *button = static_cast<QToolButton*>(QObject::sender());

    for (int i = 0; i < teamButtons.size(); ++i)
    {
        for (int j = 0; j < teamButtons[i].driverButtons.size(); ++j)
        {
            QToolButton *colorButton = teamButtons[i].driverButtons[j].button;

            if (button == colorButton)
            {
                QColor color = QColorDialog::getColor(teamButtons[i].driverButtons[j].color, this);

                if (color.isValid())
                {
                    setButtonColor(colorButton, color);
                    teamButtons[i].driverButtons[j].color = color;
                }
                return;
            }
        }
    }
}

void DriverColorsDialog::onResetButtonClicked()
{
    QToolButton *button = static_cast<QToolButton*>(QObject::sender());

    for (int i = 0; i < teamButtons.size(); ++i)
    {
        for (int j = 0; j < teamButtons[i].driverButtons.size(); ++j)
        {
            QToolButton *colorButton = teamButtons[i].driverButtons[j].button;
            QToolButton *resetButton = teamButtons[i].driverButtons[j].resetButton;

            if (button == resetButton)
            {
                setButtonColor(colorButton, ColorsManager::getInstance().getDefaultDriverColors()[teamButtons[i].driverButtons[j].no]);
                teamButtons[i].driverButtons[j].color = ColorsManager::getInstance().getDefaultDriverColors()[teamButtons[i].driverButtons[j].no];

                return;
            }
        }
    }
}

void DriverColorsDialog::setButtonColor(QToolButton *button, QColor color)
{
    QString styleSheet = "background-color: rgb(" + QString("%1, %2, %3").arg(color.red()).arg(color.green()).arg(color.blue()) + ");\n     "\
            "border-style: solid;\n     "\
            "border-width: 1px;\n     "\
            "border-radius: 3px;\n     "\
            "border-color: rgb(153, 153, 153);\n     "\
            "padding: 3px;\n";

    button->setStyleSheet(styleSheet);
}

void DriverColorsDialog::clear()
{
    for (int i = 0; i < teamButtons.size(); ++i)
    {
        for (int j = 0; j < teamButtons[i].driverButtons.size(); ++j)
        {
            delete teamButtons[i].driverButtons[j].button;
            delete teamButtons[i].driverButtons[j].resetButton;
            delete teamButtons[i].driverButtons[j].label;
        }
        teamButtons[i].driverButtons.clear();
    }
    teamButtons.clear();

    for (int i = ui->verticalLayout->count()-3; i >= 0; --i)
        ui->verticalLayout->removeItem(ui->verticalLayout->itemAt(i));
}

void DriverColorsDialog::on_buttonBox_accepted()
{
    QHash<int, QColor> colors;

    for (int i = 0; i < teamButtons.size(); ++i)
    {
        for (int j = 0; j < teamButtons[i].driverButtons.size(); ++j)
        {
            colors[teamButtons[i].driverButtons[j].no] = teamButtons[i].driverButtons[j].color;
        }
    }
    ColorsManager::getInstance().setDriverColors(colors);
    ImagesFactory::getInstance().getHelmetsFactory().reloadHelmets();
}

void DriverColorsDialog::on_pushButton_clicked()
{
    for (int i = 0; i < teamButtons.size(); ++i)
    {
        for (int j = 0; j < teamButtons[i].driverButtons.size(); ++j)
        {
            QToolButton *colorButton = teamButtons[i].driverButtons[j].button;

            setButtonColor(colorButton, ColorsManager::getInstance().getDefaultDriverColors()[teamButtons[i].driverButtons[j].no]);
            teamButtons[i].driverButtons[j].color = ColorsManager::getInstance().getDefaultDriverColors()[teamButtons[i].driverButtons[j].no];
        }
    }
}
