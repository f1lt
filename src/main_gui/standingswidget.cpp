/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "standingswidget.h"
#include "ui_standingswidget.h"
#include "../core/colorsmanager.h"


#include "ltitemdelegate.h"

#include <QDebug>
#include <QLabel>

StandingsWidget::StandingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StandingsWidget)
{
    ui->setupUi(this);

    ui->driversTableWidget->setItemDelegate(new LTItemDelegate(this));
    ui->teamsTableWidget->setItemDelegate(new LTItemDelegate(this));

    upArrowPixmap = QPixmap(":/track/up-arrow.png").scaledToHeight(10, Qt::SmoothTransformation);
    downArrowPixmap = QPixmap(":/track/down-arrow.png").scaledToHeight(10, Qt::SmoothTransformation);

    selectedDriver = 0;
    selectedTeam = 0;
    connected = false;
}

StandingsWidget::~StandingsWidget()
{
    delete ui;
}

void StandingsWidget::gatherPoints()
{
    EventData &ed = EventData::getInstance();
    StandingsManager &sm = StandingsManager::getInstance();
    sm.resetTeamRacePoints();

    for (int i = 0; i < ed.getDriversData().size(); ++i)
    {
        QString name = ed.getDriversData()[i].getDriverName();
        int racePoints = 0;

        if (((ed.getEventType() == LTPackets::RACE_EVENT) &&
            !ed.isFridayBeforeFP1() &&
            connected &&
            sm.oldStandings()) &&
            (ed.getDriversData()[i].getPosition() <= SeasonData::getInstance().getSessionDefaults().getPointPositionsNumber()) &&
            (ed.getDriversData()[i].getPosition() >= 1))
        {
            racePoints = SeasonData::getInstance().getSessionDefaults().getChampionshipPoints(ed.getDriversData()[i].getPosition());
        }
        sm.setDriverRacePoints(name, racePoints);

        int teamIdx = SeasonData::getInstance().getTeamIdx(ed.getDriversData()[i].getNumber());

        if (teamIdx > -1)
        {
            LTTeam team = SeasonData::getInstance().getTeams()[teamIdx];
            sm.addTeamRacePoints(team.teamName, racePoints);
        }
    }

    //calculateLeftPoints();
    sm.sortStandings();
}

void StandingsWidget::update(const DataUpdates &dataUpdates)
{
    if (dataUpdates.postionChanged)
    {
        gatherPoints();

        updateDriversTable();
        updateTeamsTable();
    }
}

void StandingsWidget::updateDriversTable()
{
    StandingsManager &sm = StandingsManager::getInstance();
    QFontMetrics f(ui->driversTableWidget->font());

    if (ui->driversTableWidget->rowCount() == 0)
        ui->driversTableWidget->insertRow(0);

    setItem(ui->driversTableWidget, 0, 0, "P", Qt::NoItemFlags, Qt::AlignRight | Qt::AlignVCenter);
    setItem(ui->driversTableWidget, 0, 1, "");
    setItem(ui->driversTableWidget, 0, 2, "");
    setItem(ui->driversTableWidget, 0, 3, "Name", Qt::NoItemFlags, Qt::AlignLeft | Qt::AlignVCenter);
    setItem(ui->driversTableWidget, 0, 4, "Points", Qt::NoItemFlags, Qt::AlignRight | Qt::AlignVCenter);        

    for (int i = 0; i < sm.getDriverStandings().size(); ++i)
    {
        Driver driver = sm.getDriverStandings()[i];

        int no = SeasonData::getInstance().getDriverNo(driver.name);

        if (ui->driversTableWidget->rowCount() <= (i + 1))
        {
            ui->driversTableWidget->insertRow(i + 1);
            ui->driversTableWidget->setRowHeight(i + 1, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
        }
        if (ui->driversTableWidget->columnSpan(i, 0) > 1)
            ui->driversTableWidget->setSpan(i, 0, 1, 1);

        QString points = QString::number(driver.points + driver.racePoints);
        QColor ptsColor = ColorsManager::getInstance().getColor(LTPackets::WHITE);

        if (selectedDriver && (selectedDriver->championshipPos != driver.championshipPos))
        {
            int pointsDiff = (driver.points + driver.racePoints) - (selectedDriver->points + selectedDriver->racePoints);
            points = QString::number(pointsDiff);

            if (pointsDiff > 0)
                points = "+" + points;

            if (pointsDiff != 0)
                ptsColor = pointsDiff < 0 ? ColorsManager::getInstance().getColor(LTPackets::GREEN) : ColorsManager::getInstance().getColor(LTPackets::RED);
        }
        else
        {
            if (driver.racePoints > 0)
                points += QString(" (+%1)").arg(driver.racePoints);
        }

        setItem(ui->driversTableWidget, i + 1, 0, QString("%1.").arg(i+1), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignRight | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::CYAN));
        setItem(ui->driversTableWidget, i + 1, 3, driver.name, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignLeft | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));
        setItem(ui->driversTableWidget, i + 1, 4, points, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignRight | Qt::AlignVCenter, ptsColor);

        //up/down pixmap
        QLabel *label = qobject_cast<QLabel*>(ui->driversTableWidget->cellWidget(i + 1, 1));
        if (!label)
        {
            label = new QLabel(this);
            ui->driversTableWidget->setCellWidget(i + 1, 1, label);
        }

        if (driver.championshipPos < (i + 1))
            label->setPixmap(downArrowPixmap);

        else if (driver.championshipPos > (i + 1))
            label->setPixmap(upArrowPixmap);

        else
            label->clear();

        label->setAlignment(Qt::AlignCenter);

        //helmet
        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(no, 18);
        label = qobject_cast<QLabel*>(ui->driversTableWidget->cellWidget(i + 1, 2));
        if (!label)
        {
            label = new QLabel(this);
            ui->driversTableWidget->setCellWidget(i + 1, 2, label);
        }

        label->setPixmap(helmet);
        label->setAlignment(Qt::AlignCenter);
    }

    if (ui->driversTableWidget->rowCount() == (sm.getDriverStandings().size() + 1))
    {
        ui->driversTableWidget->insertRow(ui->driversTableWidget->rowCount());
        ui->driversTableWidget->insertRow(ui->driversTableWidget->rowCount());
    }

    setItem(ui->driversTableWidget, ui->driversTableWidget->rowCount()-1, 0, QString("Max points left to get: %1").arg(sm.getDriverPointsLeft()), Qt::NoItemFlags, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));
    ui->driversTableWidget->setSpan(ui->driversTableWidget->rowCount()-1, 0, 1, ui->driversTableWidget->columnCount());
}

void StandingsWidget::updateTeamsTable()
{
    StandingsManager &sm = StandingsManager::getInstance();
    QFontMetrics f(ui->teamsTableWidget->font());

    if (ui->teamsTableWidget->rowCount() == 0)
        ui->teamsTableWidget->insertRow(0);

    setItem(ui->teamsTableWidget, 0, 0, "P", Qt::NoItemFlags, Qt::AlignRight | Qt::AlignVCenter);
    setItem(ui->teamsTableWidget, 0, 1);
    setItem(ui->teamsTableWidget, 0, 2, "Name", Qt::NoItemFlags, Qt::AlignLeft | Qt::AlignVCenter);
    setItem(ui->teamsTableWidget, 0, 3, "Points", Qt::NoItemFlags, Qt::AlignRight | Qt::AlignVCenter);

    for (int i = 0; i < sm.getTeamStandings().size(); ++i)
    {
        Team team = sm.getTeamStandings()[i];

        if (ui->teamsTableWidget->rowCount() <= (i + 1))
        {
            ui->teamsTableWidget->insertRow(i + 1);
            ui->teamsTableWidget->setRowHeight(i + 1, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
        }
        if (ui->teamsTableWidget->columnSpan(i, 0) > 1)
            ui->teamsTableWidget->setSpan(i, 0, 1, 1);

        QString points = QString::number(team.points + team.racePoints);

        QColor ptsColor = ColorsManager::getInstance().getColor(LTPackets::WHITE);

        if (selectedTeam && (selectedTeam->championshipPos != team.championshipPos))
        {
            int pointsDiff = (team.points + team.racePoints) - (selectedTeam->points + selectedTeam->racePoints);
            points = QString::number(pointsDiff);

            if (pointsDiff > 0)
                points = "+" + points;

            if (pointsDiff != 0)
                ptsColor = pointsDiff < 0 ? ColorsManager::getInstance().getColor(LTPackets::GREEN) : ColorsManager::getInstance().getColor(LTPackets::RED);
        }
        else
        {
            if (team.racePoints > 0)
                points += QString(" (+%1)").arg(team.racePoints);
        }

        setItem(ui->teamsTableWidget, i + 1, 0, QString("%1.").arg(i+1), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignRight | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::CYAN));
        setItem(ui->teamsTableWidget, i + 1, 2, team.name, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignLeft | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));
        setItem(ui->teamsTableWidget, i + 1, 3, points, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignRight | Qt::AlignVCenter, ptsColor);

        //up/down pixmap
        QLabel *label = qobject_cast<QLabel*>(ui->teamsTableWidget->cellWidget(i + 1, 1));
        if (!label)
        {
            label = new QLabel(this);
            ui->teamsTableWidget->setCellWidget(i + 1, 1, label);
        }

        if (team.championshipPos < (i + 1))
            label->setPixmap(downArrowPixmap);

        else if (team.championshipPos > (i + 1))
            label->setPixmap(upArrowPixmap);

        else
            label->clear();

        label->setAlignment(Qt::AlignCenter);
    }

    if (ui->teamsTableWidget->rowCount() == (sm.getTeamStandings().size() + 1))
    {
        ui->teamsTableWidget->insertRow(ui->teamsTableWidget->rowCount());
        ui->teamsTableWidget->insertRow(ui->teamsTableWidget->rowCount());
    }

    setItem(ui->teamsTableWidget, ui->teamsTableWidget->rowCount()-1, 0, QString("Max points left to get: %1").arg(sm.getTeamPointsLeft()), Qt::NoItemFlags, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));
    ui->teamsTableWidget->setSpan(ui->teamsTableWidget->rowCount()-1, 0, 1, ui->teamsTableWidget->columnCount());
}

QTableWidgetItem* StandingsWidget::setItem(QTableWidget *table, int row, int col, QString text, Qt::ItemFlags flags, int align,
             QColor textColor, QBrush background)
{
    QTableWidgetItem *item = table->item(row, col);
    if (!item)
    {        
        item = new QTableWidgetItem(text);
        item->setFlags(flags);
        table->setItem(row, col, item);

        QFontMetrics f(table->font());
        table->setRowHeight(row, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
    }
    item->setTextAlignment(align);
    item->setBackground(background);
    item->setText(text);
    item->setTextColor(textColor);

    return item;
}

void StandingsWidget::setFont(const QFont &font)
{
    ui->driversTableWidget->setFont(font);
    ui->teamsTableWidget->setFont(font);

    QFontMetrics f(font);

    for (int i = 0; i < ui->driversTableWidget->rowCount(); ++i)
        ui->driversTableWidget->setRowHeight(i, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));

    for (int i = 0; i < ui->teamsTableWidget->rowCount(); ++i)
        ui->teamsTableWidget->setRowHeight(i, (int)((double)(f.height()) * F1LTCore::fontScaleFactor()));
}

void StandingsWidget::resizeEvent(QResizeEvent *)
{
    on_tabWidget_currentChanged(ui->tabWidget->currentIndex());
}

void StandingsWidget::on_tabWidget_currentChanged(int index)
{
    int w;

    switch (index)
    {
        case DRIVERS_TABLE:
            w = ui->driversTableWidget->viewport()->width();
            ui->driversTableWidget->setColumnWidth(0, 0.1 * (double)w);
            ui->driversTableWidget->setColumnWidth(1, 0.1 * (double)w);
            ui->driversTableWidget->setColumnWidth(2, 0.1 * (double)w);
            ui->driversTableWidget->setColumnWidth(3, 0.5 * (double)w);
            ui->driversTableWidget->setColumnWidth(4, 0.2 * (double)w);
            break;

        case TEAMS_TABLE:
            w = ui->teamsTableWidget->viewport()->width();
            ui->teamsTableWidget->setColumnWidth(0, 0.1 * (double)w);
            ui->teamsTableWidget->setColumnWidth(1, 0.1 * (double)w);
            ui->teamsTableWidget->setColumnWidth(2, 0.5 * (double)w);
            ui->teamsTableWidget->setColumnWidth(3, 0.3 * (double)w);
    }
}

void StandingsWidget::on_driversTableWidget_clicked(const QModelIndex &index)
{
    int row = index.row();

    if (row > 0)
    {
        Driver *newSelectedDriver = &StandingsManager::getInstance().getDriverStandings()[row-1];

        if (selectedDriver == newSelectedDriver)
            selectedDriver = 0;
        else
            selectedDriver = newSelectedDriver;

        update(DataUpdates(true));
    }
}

void StandingsWidget::on_teamsTableWidget_clicked(const QModelIndex &index)
{
    int row = index.row();

    if (row > 0)
    {
        Team *newSelectedTeam = &StandingsManager::getInstance().getTeamStandings()[row-1];

        if (selectedTeam == newSelectedTeam)
            selectedTeam = 0;
        else
            selectedTeam = newSelectedTeam;

        update(DataUpdates(true));
    }
}
