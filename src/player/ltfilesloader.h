/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef LTFILESLOADER_H
#define LTFILESLOADER_H


#include <QList>
#include <QPair>
#include <string>
#include "../net/datastreamreader.h"


/*!
 * \brief The LTFilesLoader class is used to manage (load and save) .lt files. It can load both V1 and V2 .lt files version, but saves only in new format (V2) as the old one is no longer supported.
 */
class LTFilesLoader
{
public:
    LTFilesLoader();

    bool loadFile(QString fName, QVector< QPair<int, Packet> > &packets);
    bool loadV1File(QDataStream &stream, QVector< QPair<int, Packet> > &packets);
    bool loadV2File(QDataStream &stream, QVector< QPair<int, Packet> > &packets);

    void saveFile(QString fName, const QVector< QPair<int, Packet> > &packets);
    void saveMainData(QDataStream &stream);
    void savePackets(QDataStream &stream, const QVector< QPair<int, Packet> > &packets);

private:
    QString encrypt(QString text)
    {
        return text;
//        int sz = text.size();
//        QString ret;
//        for (int i = 0; i < sz; ++i)
//        {
//            char c = text[i].toAscii();
//            c ^= (1 << (i%7));
//            ret += c;
//        }
//        return ret;
    }
};

#endif // LTFILESLOADER_H
