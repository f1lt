/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "ltfilesloader.h"
#include "../core/eventdata.h"

#include <QDebug>

LTFilesLoader::LTFilesLoader()
{
}

bool LTFilesLoader::loadFile(QString fName, QVector<QPair<int, Packet> > &packets)
{
    QFile file(fName);

    if (file.open(QIODevice::ReadOnly))
    {
        QDataStream stream(&file);

        char *tab;
        stream >> tab;

        QString sbuf(tab);

        delete [] tab;
        if (sbuf == "F1LT")
        {                        
            bool ret = loadV1File(stream, packets);

            //old files didn't contain any info about FP number, try to guess it from the file name
            QFileInfo fInfo(fName);
            QRegExp reg("fp(\\d)");

            if (reg.indexIn(fInfo.fileName()) != -1)
                EventData::getInstance().setFPNumber(reg.cap(1).toInt());

            return ret;
        }

        if (sbuf == "F1LT2_LT")
        {            
            return loadV2File(stream, packets);                
        }
    }
    return false;
}

bool LTFilesLoader::loadV1File(QDataStream &stream, QVector<QPair<int, Packet> > &packets)
{
    LTEvent ltEvent;
    QVector<LTTeam> ltTeamList;

    int ibuf;
    int size;
    QPixmap pixBuf;
    QString sbuf;

    //load event data
    stream >> ibuf;
    ltEvent.eventNo = ibuf;

    stream >> sbuf;
    ltEvent.eventName = sbuf;    

    stream >> sbuf;
    ltEvent.eventShortName = sbuf;

    stream >> sbuf;
    ltEvent.eventPlace = sbuf;

    stream >> sbuf;
    ltEvent.fpDate = QDate::fromString(sbuf, "dd-MM-yyyy");

    stream >> sbuf;
    ltEvent.raceDate = QDate::fromString(sbuf, "dd-MM-yyyy");

    stream >> ibuf;
    ltEvent.laps = ibuf;    

    stream >> pixBuf;

    SeasonData::getInstance().loadSeasonData(ltEvent.fpDate.year());
    EventData::getInstance().clear();

    ltEvent = SeasonData::getInstance().getEvent(ltEvent.fpDate);

    //load drivers data
    stream >> size;
    ltTeamList.resize(size);
    for (int i = 0; i < size; ++i)
    {
        ltTeamList[i].drivers.resize(2);

        stream >> sbuf; ltTeamList[i].teamName = sbuf;

        stream >> sbuf; ltTeamList[i].drivers[0].name = sbuf;
        stream >> sbuf; ltTeamList[i].drivers[0].shortName = sbuf;
        stream >> ibuf; ltTeamList[i].drivers[0].no = ibuf;

        stream >> sbuf; ltTeamList[i].drivers[1].name = sbuf;
        stream >> sbuf; ltTeamList[i].drivers[1].shortName = sbuf;
        stream >> ibuf; ltTeamList[i].drivers[1].no = ibuf;

        stream >> ltTeamList[i].carImg;
    }
    SeasonData::getInstance().loadSeasonData(ltEvent.fpDate.year());
    SeasonData::getInstance().updateTeamList(ltTeamList);

    EventData::getInstance().clear();
    EventData::getInstance().setEventInfo(ltEvent);

    stream >> size;
    packets.resize(size);
    for (int i = 0; i < size; ++i)
    {
        stream >> packets[i].first;
        stream >> packets[i].second.type;
        stream >> packets[i].second.carID;
        stream >> packets[i].second.data;
        stream >> packets[i].second.length;
        stream >> packets[i].second.longData;
    }
    return true;
}


bool LTFilesLoader::loadV2File(QDataStream &stream, QVector<QPair<int, Packet> > &packets)
{
    LTEvent ltEvent;
    QVector<LTTeam> ltTeamList;

    int ibuf;
    int size;
    char *cbuf;

    //load event data
    stream >> ibuf;
    ltEvent.eventNo = ibuf;

    stream >> cbuf;
    ltEvent.eventName = encrypt(QString(cbuf));
    delete [] cbuf;

    stream >> cbuf;
    ltEvent.eventShortName = encrypt(QString(cbuf));
    delete [] cbuf;

    stream >> cbuf;
    ltEvent.eventPlace = encrypt(QString(cbuf));
    delete [] cbuf;


    stream >> cbuf;
    ltEvent.fpDate = QDate::fromString(encrypt(QString(cbuf)), "dd-MM-yyyy");    
    delete [] cbuf;

    stream >> cbuf;
    ltEvent.raceDate = QDate::fromString(encrypt(QString(cbuf)), "dd-MM-yyyy");
    delete [] cbuf;

    stream >> ibuf;
    ltEvent.laps = ibuf;

    SeasonData::getInstance().loadSeasonData(ltEvent.fpDate.year());
    EventData::getInstance().clear();

    stream >> ibuf;
    EventData::getInstance().setEventType((LTPackets::EventType)ibuf);

    stream >> ibuf;
    if (EventData::getInstance().getEventType() == LTPackets::PRACTICE_EVENT)
        EventData::getInstance().setFPNumber(ibuf);    

    ltEvent = SeasonData::getInstance().getEvent(ltEvent.fpDate);
    EventData::getInstance().setEventInfo(ltEvent);

    //load drivers data
    stream >> size;
    ltTeamList.resize(size);
    for (int i = 0; i < size; ++i)
    {
        stream >> cbuf; ltTeamList[i].teamName = encrypt(QString(cbuf));
        delete [] cbuf;

        int dsize;
        stream >> dsize;

        ltTeamList[i].drivers.resize(dsize);
        for (int j = 0; j < dsize; ++j)
        {
            stream >> cbuf; ltTeamList[i].drivers[j].name = encrypt(QString(cbuf));
            delete [] cbuf;

            stream >> cbuf; ltTeamList[i].drivers[j].shortName = encrypt(QString(cbuf));
            delete [] cbuf;

            stream >> ibuf; ltTeamList[i].drivers[j].no = ibuf;
        }
    }
    SeasonData::getInstance().updateTeamList(ltTeamList);

    stream >> size;
    packets.resize(size);
    for (int i = 0; i < size; ++i)
    {
        stream >> packets[i].first;
        stream >> packets[i].second.type;
        stream >> packets[i].second.carID;
        stream >> packets[i].second.data;
        stream >> packets[i].second.length;
        stream >> cbuf;
        packets[i].second.longData.clear();
        packets[i].second.longData.append(encrypt(QString(cbuf)));        

        delete [] cbuf;
    }
    return true;
}

void LTFilesLoader::saveFile(QString fName, const QVector<QPair<int, Packet> > &packets)
{
    QFile file(fName);
    if (file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);

        const char tab[9] = "F1LT2_LT";

        stream << tab;

        //save the teams and events info
        saveMainData(stream);
        savePackets(stream, packets);
    }
}

void LTFilesLoader::saveMainData(QDataStream &stream)
{
    LTEvent event = EventData::getInstance().getEventInfo();

    stream << event.eventNo;
    stream << encrypt(event.eventName).toStdString().c_str();
    stream << encrypt(event.eventShortName).toStdString().c_str();
    stream << encrypt(event.eventPlace).toStdString().c_str();
    stream << encrypt(event.fpDate.toString("dd-MM-yyyy")).toStdString().c_str();
    stream << encrypt(event.raceDate.toString("dd-MM-yyyy")).toStdString().c_str();
    stream << event.laps;
    stream << EventData::getInstance().getEventType();
    stream << EventData::getInstance().getFPNumber();

    QVector<LTTeam> teams = SeasonData::getInstance().getTeamsFromCurrentSession();
    stream << teams.size();
    for (int i = 0; i < teams.size(); ++i)
    {
        stream << encrypt(teams[i].teamName).toStdString().c_str();
        stream << teams[i].drivers.size();

        for (int j = 0; j < teams[i].drivers.size(); ++j)
        {
            stream << encrypt(teams[i].drivers[j].name).toStdString().c_str();
            stream << encrypt(teams[i].drivers[j].shortName).toStdString().c_str();
            stream << teams[i].drivers[j].no;
        }
    }
}

void LTFilesLoader::savePackets(QDataStream &stream, const QVector<QPair<int, Packet> > &packets)
{
    stream << packets.size();

    for (int i = 0; i < packets.size(); ++i)
    {
        stream << packets[i].first;
        stream << packets[i].second.type;
        stream << packets[i].second.carID;
        stream << packets[i].second.data;
        stream << packets[i].second.length;

//        stream << encrypt(QString(packets[i].second.longData).toStdString()).c_str();
        stream << encrypt(QString(packets[i].second.longData)).toStdString().c_str();
//        stream << QString(packets[i].second.longData).toStdString().c_str();
    }
}
