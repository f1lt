/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef STANDINGSMANAGER_H
#define STANDINGSMANAGER_H

#include <QDate>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QObject>

#include "../core/eventdata.h"

struct Driver
{
    int points;
    int racePoints;
    int championshipPos;
    QString name;

    bool operator<(const Driver &d) const
    {
        if ((points + racePoints) == (d.points + d.racePoints))
            return championshipPos < d.championshipPos;

        return (points + racePoints) > (d.points + d.racePoints);
    }

    Driver()
    {
        points = 0;
        racePoints = 0;
        championshipPos = 0;
    }
};

struct Team
{
    int points;
    int racePoints;
    int championshipPos;
    QString name;

    bool operator<(const Team &t) const
    {
        if ((points + racePoints) == (t.points + t.racePoints))
            return championshipPos < t.championshipPos;

        return (points + racePoints) > (t.points + t.racePoints);
    }

    Team()
    {
        points = 0;
        racePoints = 0;
        championshipPos = 0;
    }
};

class StandingsManager : public QObject
{
    Q_OBJECT
public:

    static StandingsManager &getInstance()
    {
        static StandingsManager instance;
        return instance;
    }

    enum ParsingStatus
    {
        NO_PARSING,
        PARSING_DRIVERS,
        PARSING_TEAMS
    };

    void obtainStandings();
    void parseStandingsLine(QString line, int row);

    void setDriverRacePoints(QString name, int racePoints);
    void addTeamRacePoints(QString name, int racePoints);
    void sortStandings()
    {
        qSort(driverStandings);
        qSort(teamStandings);
    }

    void resetTeamRacePoints()
    {
        for (int i = 0; i < teamStandings.size(); ++i)
        {
            teamStandings[i].racePoints = 0;
        }
    }

    QDate getStandingsDate() const
    {
        return standingsDate;
    }

    int getDriversPointsCnt() const
    {
        return driversPointsCnt;
    }

    bool oldStandings() const
    {
        if (EventData::getInstance().getEventInfo().raceDate >= getStandingsDate())
            return true;

        return false;
    }

    int getDriverPointsLeft() const;
    int getTeamPointsLeft() const;
    int getEventsLeft() const;

    QList<Driver> &getDriverStandings()
    {
        return driverStandings;
    }

    QList<Team> &getTeamStandings()
    {
        return teamStandings;
    }

    bool parsedStandings()
    {
        return parsed;
    }

signals:
    void standingsParsed();
    void error (QNetworkReply::NetworkError code);

public slots:
    void finished();

private:
    explicit StandingsManager(QObject *parent = 0);

    QNetworkAccessManager manager;
    QNetworkRequest req;
    QNetworkReply *reply;

    ParsingStatus parsingStatus;
    QDate standingsDate;
    int driversPointsCnt;

    QList<Driver> driverStandings;
    QList<Team> teamStandings;

    bool parsed;
};

#endif // STANDINGSMANAGER_H
