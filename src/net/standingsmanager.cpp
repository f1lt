/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "standingsmanager.h"
#include "networksettings.h"

#include <QDebug>

StandingsManager::StandingsManager(QObject *parent) :
    QObject(parent)
{
    parsingStatus = NO_PARSING;
    driversPointsCnt = 0;
    parsed = false;
}

void StandingsManager::obtainStandings()
{
    req = QNetworkRequest(NetworkSettings::getInstance().getStandingsUrl());
    req.setRawHeader("User-Agent","f1lt");

    if (NetworkSettings::getInstance().usingProxy())
        manager.setProxy(NetworkSettings::getInstance().getProxy());
    else
        manager.setProxy(QNetworkProxy::NoProxy);

    reply = manager.get(req);

    connect(reply, SIGNAL(finished()), this, SLOT(finished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(error(QNetworkReply::NetworkError)));
}

void StandingsManager::finished()
{
    QString buf = reply->readAll().constData();

    if (!buf.isEmpty())
    {
        QStringList list = buf.split("\n");

        disconnect(reply, SIGNAL(finished()), this, SLOT(finished()));
        disconnect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(error(QNetworkReply::NetworkError)));

        for (int i = 0; i < list.size(); ++i)
        {
            parseStandingsLine(list[i], i);
        }

        parsed = true;
        emit standingsParsed();
    }
}

void StandingsManager::parseStandingsLine(QString line, int row)
{
    static int currentIdx = 0;
    if (row == 0)
    {
        standingsDate = QDate::fromString(line.replace("\r", ""), "d-M-yyyy");
    }

    else
    {
        if (line.contains("drivers"))
        {
            parsingStatus = PARSING_DRIVERS;
            currentIdx = 0;
            driversPointsCnt = 0;
            return;
        }

        if (line.contains("teams"))
        {
            parsingStatus = PARSING_TEAMS;
            currentIdx = 0;
            return;
        }

        QStringList arr = line.split("/");

        if (arr.size() == 2)
        {
            QString name = arr[0];
            QString points = arr[1];

            if (parsingStatus == PARSING_DRIVERS)
            {
                Driver driver;
                driver.points = points.toInt();
                driver.name = name;
                driver.championshipPos = ++currentIdx;

                driverStandings.append(driver);
            }
            else if (parsingStatus == PARSING_TEAMS)
            {
                Team team;
                team.points = points.toInt();
                team.name = name;
                team.championshipPos = ++currentIdx;

                teamStandings.append(team);
            }
        }
    }
}

int StandingsManager::getDriverPointsLeft() const
{
    return SeasonData::getInstance().getSessionDefaults().getChampionshipPoints(1) * getEventsLeft();
}

int StandingsManager::getTeamPointsLeft() const
{
    return (SeasonData::getInstance().getSessionDefaults().getChampionshipPoints(1) + SeasonData::getInstance().getSessionDefaults().getChampionshipPoints(2)) * getEventsLeft();
}


int StandingsManager::getEventsLeft() const
{
    QDate currDate = QDateTime::currentDateTimeUtc().date();
    QDate raceDate = EventData::getInstance().getEventInfo().raceDate;

    if ((currDate < raceDate) ||
        ((currDate == raceDate) && !EventData::getInstance().isSessionFinished()))
        return SeasonData::getInstance().getEvents().size() - SeasonData::getInstance().getCurrentEvent().eventNo + 1;

    return SeasonData::getInstance().getEvents().size() - SeasonData::getInstance().getCurrentEvent().eventNo;

}

void StandingsManager::setDriverRacePoints(QString name, int racePoints)
{
    bool found = false;
    for (int i = 0; i < driverStandings.size(); ++i)
    {
        if (name == driverStandings[i].name)
        {
            driverStandings[i].racePoints = racePoints;
            found = true;
            break;
        }
    }
    if (!found && (racePoints != 0))
    {
        Driver driver;
        driver.name = name;
        driver.racePoints = racePoints;
        driver.championshipPos = driverStandings.size() + 1;

        driverStandings.append(driver);
    }
}

void StandingsManager::addTeamRacePoints(QString name, int racePoints)
{
    for (int i = 0; i < teamStandings.size(); ++i)
    {
        if (name == teamStandings[i].name)
        {
            teamStandings[i].racePoints += racePoints;
            break;
        }
    }
}
