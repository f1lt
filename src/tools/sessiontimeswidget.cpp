/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "sessiontimeswidget.h"
#include "ui_sessiontimeswidget.h"

#include <QClipboard>
#include <QDebug>
#include <QStringList>

#include "../core/colorsmanager.h"
#include "../core/eventdata.h"
#include "../core/seasondata.h"
#include "../main_gui/ltitemdelegate.h"

SessionTimesWidget::SessionTimesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SessionTimesWidget), selectedDriver(0), relativeTimes(false), tableViewState(SHOW_LAP_TIME)
{
    ui->setupUi(this);
    loadDriversList();

    perfCompWidget = new PerformanceCompareWidget();

    ui->timesTableWidget->setItemDelegate(new LTItemDelegate(this));
    ui->driversListWidget->setItemDelegate(new LTItemDelegate(this));

    connect(ui->timesTableWidget->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(onHeaderClicked(int)));
    connect(ui->timesTableWidget->horizontalHeader(), SIGNAL(sectionPressed(int)), this, SLOT(onHeaderClicked(int)));
    connect(ui->timesTableWidget->horizontalHeader(), SIGNAL(sectionDoubleClicked(int)), this, SLOT(onHeaderDoubleClicked(int)));
}

SessionTimesWidget::~SessionTimesWidget()
{
    delete perfCompWidget;
    delete ui;
}

QTableWidgetItem* SessionTimesWidget::setItem(int row, int col, QString text, Qt::ItemFlags flags, int align,
             QColor textColor, QBrush background)
{
    if (ui->timesTableWidget->rowCount() <= row)
    {
        QFontMetrics f(ui->timesTableWidget->font());
        ui->timesTableWidget->insertRow(row);
        ui->timesTableWidget->setRowHeight(ui->timesTableWidget->rowCount()-1, (int)((double)(f.height()) * F1LTCore::fontScaleFactor() * 1.3));


//        for (int i = 0; i < ui->timesTableWidget->columnCount();  ++i)
//        {
//            if (i != col)
//                ui->timesTableWidget->setItem(row, i, new QTableWidgetItem());
//        }
    }

    QTableWidgetItem *item = ui->timesTableWidget->item(row, col);
    if (!item)
    {
        item = new QTableWidgetItem(text);
        item->setFlags(flags);
        ui->timesTableWidget->setItem(row, col, item);
    }
    item->setTextAlignment(align);
    item->setBackground(background);
    item->setText(text);
    item->setTextColor(textColor);

    return item;
}

void SessionTimesWidget::loadDriversList()
{
    QStringList list = SeasonData::getInstance().getDriversList();
    list.removeFirst();

    ui->driversListWidget->clear();
    ui->driversListWidget->addItems(list);   

    for (int i = ui->timesTableWidget->columnCount(); i < list.size(); ++i)
    {
        ui->timesTableWidget->insertColumn(i);
    }

    if (list.size() < ui->timesTableWidget->columnCount())
    {
        for (int i = ui->timesTableWidget->columnCount(); i >= list.size(); --i)
            ui->timesTableWidget->removeColumn(i);
    }
    ui->timesTableWidget->clear();

    for (int i = 0; i < ui->driversListWidget->count(); ++i)
    {
        ui->driversListWidget->item(i)->setFlags(ui->driversListWidget->item(i)->flags() | Qt::ItemIsUserCheckable);

        QTableWidgetItem *item = ui->timesTableWidget->horizontalHeaderItem(i);
        if (item == 0)
        {
            item = new QTableWidgetItem();
            ui->timesTableWidget->setHorizontalHeaderItem(i, item);
        }
        item->setText(getName(i));
    }
    restoreCheckedArray();
}

void SessionTimesWidget::exec()
{
    setWindowTitle("Session times: " + EventData::getInstance().getEventInfo().eventName);
    loadDriversList();
    update();
    show();
}

void SessionTimesWidget::update()
{
    switch(EventData::getInstance().getEventType())
    {
        case LTPackets::RACE_EVENT:     handleRaceEvent(); break;
        case LTPackets::QUALI_EVENT:    handleQualiEvent(); break;
        case LTPackets::PRACTICE_EVENT: handlePracticeEvent(); break;
    }

    if (ui->top10Button->isChecked())
        on_top10Button_toggled(true);
}

void SessionTimesWidget::handleRaceEvent()
{
    for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
    {
        int driverNo = getNumber(i);

        DriverData *dd = EventData::getInstance().getDriverDataPtr(driverNo);

        if (dd != 0)
        {
            LapTime bestTime = dd->getSessionRecords().getBestLap().getTime();
            int bestLapNo = dd->getSessionRecords().getBestLap().getLapNumber();

            if (tableViewState != SHOW_LAP_TIME)
            {
                bestTime = dd->getSessionRecords().getBestSectorTime((int)tableViewState);
                bestLapNo = dd->getSessionRecords().getBestSectorLapNumber((int)tableViewState);
            }

            for (int j = EventData::getInstance().getCompletedLaps(); j >= 1; --j)
            {
                LapData ld = dd->getLapData(j);
                LapTime lt = ld.getTime();
                if (tableViewState != SHOW_LAP_TIME)
                    lt = ld.getSectorTime((int)tableViewState);

                QColor color = ((!relativeTimes || bestLapNo == j) && lt.isValid()) ? ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::YELLOW);

                if (ld.getCarID() == dd->getCarID())
                {
                    QString time = lt.toString();

                    if (time == "IN PIT")
                        time = QString("IN PIT (%1)").arg(dd->getPitTime(j));

                    if (selectedDriver != 0 && ui->relativeButton->isChecked())
                    {
                        color = (lt.isValid()) ? ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::RED);
                        if (selectedDriver != dd)
                        {
                            LapData sld = selectedDriver->getLapData(j);
                            LapTime slt = sld.getTime();

                            if (tableViewState != SHOW_LAP_TIME)
                                slt = sld.getSectorTime((int)tableViewState);

                            if (sld.getCarID() == selectedDriver->getCarID() && slt.isValid() && lt.isValid())
                            {
                                color = (lt < slt) ? ColorsManager::getInstance().getColor(LTPackets::GREEN) : ColorsManager::getInstance().getColor(LTPackets::RED);

                                if (relativeTimes)
                                {
                                    time = DriverData::calculateGap(lt, slt);

                                    //remove trailing zeroes
                                    if (tableViewState != SHOW_LAP_TIME)
                                        time = time.left(time.size()-2);
                                }

                            }
                        }

                    }
                    else
                    {
                        if (j == bestLapNo)
                            color = ColorsManager::getInstance().getColor(LTPackets::GREEN);

                        if (!ld.getTime().isValid())
                            color = ColorsManager::getInstance().getColor(LTPackets::RED);

                        if (relativeTimes && bestLapNo != j && lt.isValid())
                        {
                            time = DriverData::calculateGap(lt, bestTime);

                            if (tableViewState != SHOW_LAP_TIME)
                                time = time.left(time.size()-2);
                        }

                    }
                    setItem(EventData::getInstance().getCompletedLaps()-j, i, time, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, color);


                    QTableWidgetItem *item = ui->timesTableWidget->verticalHeaderItem(j-1);

                    if (item == 0)
                    {
                        item = new QTableWidgetItem();
                        item->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
                        ui->timesTableWidget->setVerticalHeaderItem(j-1, item);
                    }
                    item->setText(QString::number(EventData::getInstance().getCompletedLaps()-j+1));
                }
                else
                    setItem(EventData::getInstance().getCompletedLaps()-j, i, "", Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, color);

            }
        }
    }
    removeRows(EventData::getInstance().getCompletedLaps());
}

void SessionTimesWidget::handleQualiEvent()
{
    int row = 0;
    for (int q = 3; q >= 1; --q)
    {
        for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
            setItem(row, i, QString("Q%1").arg(q), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter,
                    ColorsManager::getInstance().getColor(LTPackets::BACKGROUND), /*SeasonData::getInstance().getColor(LTPackets::YELLOW)*/QBrush(QColor(123, 123, 123)));

        QTableWidgetItem *item = ui->timesTableWidget->verticalHeaderItem(row);

        if (item == 0)
        {
            item = new QTableWidgetItem();
            ui->timesTableWidget->setVerticalHeaderItem(row, item);
        }
        item->setText(QString("Q%1").arg(q));

        ++row;

//        for (int j = 1; j <= SeasonData::getInstance().getSessionDefaults().getQualiLength(q); ++j)
        for (int j = SeasonData::getInstance().getSessionDefaults().getQualiLength(q); j >= 1; --j)
        {
            bool rowInserted = false;

            LapData sld;
            if (selectedDriver != 0 && ui->relativeButton->isChecked())
                sld = selectedDriver->getQLapData(j, q);

            for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
            {
                int driverNo = getNumber(i);

                DriverData *dd = EventData::getInstance().getDriverDataPtr(driverNo);

                if (dd != 0)
                {
                    LapTime bestTime = dd->getSessionRecords().getBestQualiLap(3).getTime();
                    int bestQ = 3;

                    while (!bestTime.isValid() && bestQ > 1)
                    {
                        --bestQ;
                         bestTime = dd->getSessionRecords().getBestQualiLap(bestQ).getTime();
                    }

                    if (tableViewState != SHOW_LAP_TIME)
                        bestTime = dd->getSessionRecords().getBestSectorTime((int)tableViewState);

                    int bestLapNo = dd->getSessionRecords().getBestQualiLap(bestQ).getLapNumber();

                    if (tableViewState != SHOW_LAP_TIME)
                        bestLapNo = dd->getSessionRecords().getBestSectorLapNumber((int)tableViewState);


                    LapData ld = dd->getQLapData(j, q);

                    LapTime lt = ld.getTime();

                    if (tableViewState != SHOW_LAP_TIME)
                        lt = ld.getSectorTime((int)tableViewState);

                    QColor color = ((!relativeTimes || bestLapNo == ld.getLapNumber()) && lt.isValid()) ?
                                ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::YELLOW);

                    if (lt.toString().contains("LAP"))
                        color = ColorsManager::getInstance().getColor(LTPackets::RED);

                    if (ld.getCarID() == dd->getCarID())
                    {
                        QString time = lt.toString();

                        if (selectedDriver != 0 && ui->relativeButton->isChecked())
                        {
                            LapTime slt = sld.getTime();

                            if (tableViewState != SHOW_LAP_TIME)
                                slt = sld.getSectorTime((int)tableViewState);

                            color = lt.isValid() ? ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::RED);
                            if (selectedDriver != dd && sld.getCarID() == selectedDriver->getCarID() && slt.isValid() && lt.isValid())
                            {
                                color = (lt < slt) ? ColorsManager::getInstance().getColor(LTPackets::GREEN) : ColorsManager::getInstance().getColor(LTPackets::RED);

                                if (relativeTimes)
                                {
                                    time = DriverData::calculateGap(lt, slt);

                                    if (tableViewState != SHOW_LAP_TIME)
                                        time = time.left(time.size()-2);
                                }
                            }

                        }
                        else
                        {
                            if (ld.getLapNumber() == bestLapNo)
                                color = ColorsManager::getInstance().getColor(LTPackets::GREEN);

                            if (relativeTimes && bestLapNo != ld.getLapNumber() && lt.isValid())
                            {
                                time = DriverData::calculateGap(lt, bestTime);

                                if (tableViewState != SHOW_LAP_TIME)
                                    time = time.left(time.size()-2);
                            }
                        }

                        setItem(row, i, time, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, color);
                        rowInserted = true;
                    }
                    else
                        setItem(row, i, "", Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter);
                }
            }
            if (rowInserted)
            {
                QTableWidgetItem *item = ui->timesTableWidget->verticalHeaderItem(row);

                if (item == 0)
                {
                    item = new QTableWidgetItem();                   
                    ui->timesTableWidget->setVerticalHeaderItem(row, item);
                }
                item->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
                item->setText(QString::number(j));

                ++row;
            }
        }
    }
    removeRows(row);
}

void SessionTimesWidget::handlePracticeEvent()
{
    int row = 0;
//    for (int j = 1; j <= SeasonData::getInstance().getSessionDefaults().getFPLength(); ++j)
    for (int j = SeasonData::getInstance().getSessionDefaults().getFPLength(); j >= 1; --j)
    {
        bool rowInserted = false;

        LapData sld;
        if (selectedDriver != 0 && ui->relativeButton->isChecked())
            sld = selectedDriver->getFPLapData(j);

        for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
        {
            int driverNo = getNumber(i);

            DriverData *dd = EventData::getInstance().getDriverDataPtr(driverNo);

            if (dd != 0)
            {
                LapTime bestTime = dd->getSessionRecords().getBestLap().getTime();
                int bestLapNo = dd->getSessionRecords().getBestLap().getLapNumber();

                if (tableViewState != SHOW_LAP_TIME)
                {
                    bestTime = dd->getSessionRecords().getBestSectorTime((int)tableViewState);
                    bestLapNo = dd->getSessionRecords().getBestSectorLapNumber((int)tableViewState);
                }


                LapData ld = dd->getFPLapData(j);
                LapTime lt = ld.getTime();

                if (tableViewState != SHOW_LAP_TIME)
                    lt = ld.getSectorTime((int)tableViewState);


                QColor color = ((!relativeTimes || bestLapNo == ld.getLapNumber()) && lt.isValid()) ?
                            ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::YELLOW);

                if (lt.toString().contains("LAP"))
                    color = ColorsManager::getInstance().getColor(LTPackets::RED);

                if (ld.getCarID() == dd->getCarID())
                {
                    QString time = lt.toString();

                    if (selectedDriver != 0 && ui->relativeButton->isChecked())
                    {
                        LapTime slt = sld.getTime();

                        if (tableViewState != SHOW_LAP_TIME)
                            slt = sld.getSectorTime((int)tableViewState);

                        color = lt.isValid() ? ColorsManager::getInstance().getColor(LTPackets::WHITE) : ColorsManager::getInstance().getColor(LTPackets::RED);
                        if (selectedDriver != dd && sld.getCarID() == selectedDriver->getCarID() && slt.isValid() && lt.isValid())
                        {
                            color = (lt < slt) ? ColorsManager::getInstance().getColor(LTPackets::GREEN) : ColorsManager::getInstance().getColor(LTPackets::RED);

                            if (relativeTimes)
                            {
                                time = DriverData::calculateGap(lt, slt);

                                if (tableViewState != SHOW_LAP_TIME)
                                    time = time.left(time.size()-2);
                            }
                        }

                    }
                    else
                    {
                        if (ld.getLapNumber() == bestLapNo)
                            color = ColorsManager::getInstance().getColor(LTPackets::GREEN);

                        if (relativeTimes && bestLapNo != ld.getLapNumber() && lt.isValid())
                        {
                            time = DriverData::calculateGap(lt, bestTime);

                            if (tableViewState != SHOW_LAP_TIME)
                                time = time.left(time.size()-2);
                        }
                    }

                    setItem(row, i, time, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, color);
                    rowInserted = true;
                }
                else
                    setItem(row, i, "", Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter);
            }            
        }
        if (rowInserted)
        {
            QTableWidgetItem *item = ui->timesTableWidget->verticalHeaderItem(row);

            if (item == 0)
            {
                item = new QTableWidgetItem();
                item->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
                ui->timesTableWidget->setVerticalHeaderItem(row, item);
            }
            item->setText(QString::number(j));

            ++row;
        }
    }
    removeRows(row);
}

int SessionTimesWidget::getNumber(int row)
{
    QListWidgetItem *item = ui->driversListWidget->item(row);
    if (item)
    {
        QString text = item->text();

        int no = -1;
        int idx = text.indexOf(" ");
        if (idx != -1)
        {
            bool ok;
            no = text.left(idx).toInt(&ok);

            if (!ok)
                no = -1;
        }
        return no;
    }
    return -1;
}

QString SessionTimesWidget::getName(int row)
{
    QListWidgetItem *item = ui->driversListWidget->item(row);
    if (item)
    {
        QString text = item->text();
        QRegExp reg("(\\d+)\\s(\\D+)");

        if (reg.indexIn(text) != -1)
        {
            return SeasonData::getInstance().getDriverShortName(reg.cap(2));
        }
//        int idx = text.indexOf(" ");
//        if (idx != -1)
//        {
//            return text.right(text.size()-idx-1);
//        }
    }
    return QString();
}

void SessionTimesWidget::removeRows(int row)
{
    for (int i = ui->timesTableWidget->rowCount()-1; i >= row; --i)
        ui->timesTableWidget->removeRow(i);
}

void SessionTimesWidget::setFont(const QFont &font)
{
    ui->timesTableWidget->setFont(font);

    QFontMetrics f(font);
    int height = (int)((double)(f.height()) * F1LTCore::fontScaleFactor() * 1.3);

    for (int i = 0; i < ui->timesTableWidget->rowCount(); ++i)
        ui->timesTableWidget->setRowHeight(i, height);
}

void SessionTimesWidget::saveSettings(QSettings &settings)
{
    settings.setValue("ui/session_times_geometry", saveGeometry());
    settings.setValue("ui/session_times_splitter", ui->splitter->saveState());
    settings.setValue("ui/session_times_table", ui->timesTableWidget->saveGeometry());

    settings.setValue("ui/session_times_columns", checkedArray);

    QList<QVariant> columnSizes;
    for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
        columnSizes << ui->timesTableWidget->columnWidth(i);

    settings.setValue("ui/session_times_column_sizes", columnSizes);

    perfCompWidget->saveSettings(settings);
}

void SessionTimesWidget::loadSettings(QSettings &settings)
{
    restoreGeometry(settings.value("ui/session_times_geometry").toByteArray());
    ui->splitter->restoreState(settings.value("ui/session_times_splitter").toByteArray());
    ui->timesTableWidget->restoreGeometry(settings.value("ui/session_times_table").toByteArray());

    checkedArray = settings.value("ui/session_times_columns", QByteArray(ui->driversListWidget->count(), 2)).toByteArray();

    QList<QVariant> columnSizes = settings.value("ui/session_times_column_sizes").toList();
    for (int i = 0; i < columnSizes.size(); ++i)
    {
        if (columnSizes[i].toInt() > 0)
            ui->timesTableWidget->setColumnWidth(i, columnSizes[i].toInt());
    }

    perfCompWidget->loadSettings(settings);
}

void SessionTimesWidget::saveCheckedArray()
{
    checkedArray.resize(ui->driversListWidget->count());
    for (int i = 0; i < ui->driversListWidget->count(); ++i)
    {
        QListWidgetItem *item = ui->driversListWidget->item(i);
        checkedArray[i] = (int)item->checkState();
    }
}

void SessionTimesWidget::restoreCheckedArray()
{
    for (int i = 0; i < ui->driversListWidget->count(); ++i)
    {
        QListWidgetItem *item = ui->driversListWidget->item(i);

        if (i < checkedArray.size())
        {
            int t = checkedArray[i];
            item->setCheckState((Qt::CheckState)t);

            if ((Qt::CheckState)t == Qt::Unchecked)
                ui->timesTableWidget->setColumnHidden(i, true);
        }
        else
            item->setCheckState(Qt::Checked);
    }
}

int SessionTimesWidget::getQualiPeriod(int row)
{
    for (int i = row-1; i >= 0; --i)
    {
        QTableWidgetItem *item = ui->timesTableWidget->item(i, 0);

        if (item && item->text().contains("Q"))
        {
            bool ok = false;
            int qPeriod = item->text().right(1).toInt(&ok);

            if (ok)
                return qPeriod;
        }
    }
    return 0;
}

void SessionTimesWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        on_closeButton_clicked();

    if (event->key() == Qt::Key_C && event->modifiers() == Qt::ControlModifier)
    {
        QTableWidget *table = ui->timesTableWidget;

        QItemSelectionModel * selection = table->selectionModel();
        QModelIndexList indexes = selection->selectedIndexes();

        if(indexes.size() < 1)
            return;

        qSort(indexes.begin(), indexes.end());

        QString selected_text;
        QString driverNames;


        QModelIndex current;
        int firstCol = 100, lastCol = 0;

        Q_FOREACH(current, indexes)
        {
            if (current.column() < firstCol)
                firstCol = current.column();

            if (current.column() > lastCol)
                lastCol = current.column();
        }

        QVector<bool> emptyCol(lastCol - firstCol + 1, true);

        bool isQuali = (EventData::getInstance().getEventType() == LTPackets::QUALI_EVENT);

        //search for empty columns (they will be excluded)
        for (int i = indexes.first().row(); i <= indexes.last().row(); ++i)
        {
            for (int j = firstCol; j <= lastCol; ++j)
            {
                QTableWidgetItem *item = table->item(i, j);
                if (item && item->isSelected() && !table->isColumnHidden(j) && !item->text().contains("Q"))
                {
                    emptyCol[j - firstCol] = false;
                }
            }
        }

        for (int i = indexes.first().row(); i <= indexes.last().row(); ++i)
        {
            QString row;
            QTableWidgetItem *vitem = table->verticalHeaderItem(i);
            if (vitem)
            {
                if (!isQuali || !vitem->text().contains("Q"))
                {
                    if (isQuali)
                    {
                        int qPeriod = getQualiPeriod(i);

                        if (qPeriod != 0)
                            row.append(QString("Q%1 ").arg(qPeriod));
                    }
                    row.append(vitem->text()).append('\t');
                }
            }

            bool emptyRow = true;
            for (int j = firstCol; j <= lastCol; ++j)
            {
                if (!emptyCol[j - firstCol])
                {
                    QTableWidgetItem *item = table->item(i, j);

                    if (item)
                    {
                        if (item->isSelected() && (!isQuali || !item->text().contains("Q")))
                        {
                            emptyRow = false;
                            row.append(item->text());
                        }

                        row.append('\t');
                    }

                    if (i == indexes.first().row())
                    {
                        if (j == firstCol)
                            driverNames.append('\t');

                        QTableWidgetItem *hitem = table->horizontalHeaderItem(j);
                        if (hitem)
                            driverNames.append(hitem->text());

                        driverNames.append('\t');
                    }
                }
            }
            if (!emptyRow)
                selected_text.append(row).append('\n');
        }


        driverNames.append('\n');
        qApp->clipboard()->setText(driverNames.append(selected_text));
    }

    QWidget::keyPressEvent(event);
}

void SessionTimesWidget::on_closeButton_clicked()
{
    saveCheckedArray();
    close();
}

void SessionTimesWidget::on_flagButton_clicked()
{
    relativeTimes = !relativeTimes;

    if (relativeTimes)
        ui->flagButton->setText("Absolute times");
    else
        ui->flagButton->setText("Relative times");

    update();
}

void SessionTimesWidget::on_driversListWidget_clicked(const QModelIndex &index)
{
    ui->relativeButton->setEnabled(true);
    ui->compareButton->setEnabled(true);
    int row = index.row();

    int no = getNumber(row);

    QListWidgetItem *item = ui->driversListWidget->item(row);
    if (item)
    {
        if (item->checkState() == Qt::Unchecked)
        {
            ui->timesTableWidget->setColumnHidden(row, true);

            if (ui->relativeButton->isChecked())
            {
                ui->relativeButton->setChecked(false);
                update();
            }
        }

        else
        {
            ui->timesTableWidget->selectColumn(row);
            ui->timesTableWidget->setColumnHidden(row, false);

            selectedDriver = EventData::getInstance().getDriverDataPtr(no);

            if (ui->relativeButton->isChecked())
            {
                update();
            }
        }
    }
}

void SessionTimesWidget::on_driversListWidget_doubleClicked(const QModelIndex &index)
{
    ui->relativeButton->setChecked(true);
    on_driversListWidget_clicked(index);
}


void SessionTimesWidget::on_relativeButton_toggled(bool)
{
    update();
}

void SessionTimesWidget::onHeaderClicked(int col)
{
    ui->relativeButton->setEnabled(true);
    ui->compareButton->setEnabled(true);
    ui->driversListWidget->clearSelection();
    QListWidgetItem *item = ui->driversListWidget->item(col);
    if (item)
        ui->driversListWidget->setCurrentItem(item);

    int no = getNumber(col);
    selectedDriver = EventData::getInstance().getDriverDataPtr(no);
    if (ui->relativeButton->isChecked())
        update();

}

void SessionTimesWidget::onHeaderDoubleClicked(int col)
{
    ui->relativeButton->setChecked(true);
    onHeaderClicked(col);
}



void SessionTimesWidget::on_top10Button_toggled(bool toggled)
{
    for (int i = 0; i < ui->driversListWidget->count(); ++i)
    {
        QRegExp reg("(\\d+)\\s\\D+");

        if (reg.indexIn(ui->driversListWidget->item(i)->text()) != -1)
        {
            int no = reg.cap(1).toInt();

            if (toggled)
            {
                if (EventData::getInstance().getDriverDataPtr(no)->getPosition() > 10)
                {
                    ui->driversListWidget->item(i)->setCheckState(Qt::Unchecked);
                    ui->timesTableWidget->setColumnHidden(i, true);
                }
                else
                {
                    ui->driversListWidget->item(i)->setCheckState(Qt::Checked);
                    ui->timesTableWidget->setColumnHidden(i, false);
                }
            }
            else
            {
                ui->driversListWidget->item(i)->setCheckState(Qt::Checked);
                ui->timesTableWidget->setColumnHidden(i, false);
            }
        }
    }
}

void SessionTimesWidget::on_timeBox_currentIndexChanged(int index)
{
    tableViewState = (TableViewState)index;
    update();
}

void SessionTimesWidget::on_compareButton_clicked()
{
    QItemSelectionModel * selection = ui->timesTableWidget->selectionModel();
    QModelIndexList indexes = selection->selectedIndexes();

    QList<LapData> laps;
    for (int i = 0; i < indexes.size(); ++i)
    {
        int driverNo = getNumber(indexes[i].column());

        DriverData *dd = EventData::getInstance().getDriverDataPtr(driverNo);

        if (dd)
        {
            if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT)
            {
                int lapNumber = ui->timesTableWidget->verticalHeaderItem(indexes[i].row())->text().toInt();

                LapData ld = dd->getLapData(lapNumber);

                if (ld.getCarID() == dd->getCarID())
                    laps.append(ld);
            }
            else if (EventData::getInstance().getEventType() == LTPackets::PRACTICE_EVENT)
            {
                int minute = ui->timesTableWidget->verticalHeaderItem(indexes[i].row())->text().toInt();

                LapData ld = dd->getFPLapData(minute);

                if (ld.getCarID() == dd->getCarID())
                    laps.append(ld);
            }
            else
            {
                int minute = ui->timesTableWidget->verticalHeaderItem(indexes[i].row())->text().toInt();
                int qPeriod = getQualiPeriod(indexes[i].row());

                LapData ld = dd->getQLapData(minute, qPeriod);

                if (ld.getCarID() == dd->getCarID())
                    laps.append(ld);
            }
        }
    }
    if (!laps.isEmpty())
        perfCompWidget->exec(laps);
}

void SessionTimesWidget::on_timesTableWidget_pressed(const QModelIndex &index)
{
    ui->compareButton->setEnabled(true);
}
