/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "performancecomparewidget.h"
#include "ui_performancecomparewidget.h"


#include "../core/eventdata.h"
#include "../main_gui/ltitemdelegate.h"

PerformanceCompareWidget::PerformanceCompareWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PerformanceCompareWidget)
{
    ui->setupUi(this);
    ui->timesTableWidget->setItemDelegate(new LTItemDelegate(this));
    timesViewState = SHOW_LAP_TIME;
}

PerformanceCompareWidget::~PerformanceCompareWidget()
{
    delete ui;
}

QTableWidgetItem* PerformanceCompareWidget::setItem(int row, int col, QString text, Qt::ItemFlags flags, int align,
             QColor textColor, QBrush background)
{
    if (ui->timesTableWidget->rowCount() <= row)
    {
        QFontMetrics f(ui->timesTableWidget->font());
        ui->timesTableWidget->insertRow(row);
        ui->timesTableWidget->setRowHeight(ui->timesTableWidget->rowCount()-1, (int)((double)(f.height()) * F1LTCore::fontScaleFactor() * 1.3));
    }

    QTableWidgetItem *item = ui->timesTableWidget->item(row, col);
    if (!item)
    {
        item = new QTableWidgetItem(text);
        item->setFlags(flags);
        ui->timesTableWidget->setItem(row, col, item);
    }
    item->setTextAlignment(align);
    item->setBackground(background);
    item->setText(text);
    item->setTextColor(textColor);

    return item;
}

void PerformanceCompareWidget::exec(QList<LapData> &laps)
{
    if (!laps.isEmpty())
    {
        dpData.clear();

        qSort(laps.begin(), laps.end(), LapDataComparator());

        DriverPerformanceData dp;
        dp.carID = laps.first().getCarID();
        dp.laps.append(laps.first());

        for (int i = 1; i < laps.size(); ++i)
        {
            if (laps[i].getCarID() == dp.carID)
            {
                dp.laps.append(laps[i]);
            }
            else
            {
                dp.calculateBestAndAvgTime(timesViewState);

                dpData.append(dp);

                //another driver
                dp = DriverPerformanceData();
                dp.carID = laps[i].getCarID();
                dp.laps.append(laps[i]);
            }
        }
        dp.calculateBestAndAvgTime(timesViewState);
        dpData.append(dp);

        qSort(dpData);
        updateTable();

        if (!isVisible())
            show();

        else
            raise();
    }
}

void PerformanceCompareWidget::updateTable()
{
    for (int i = 0; i < dpData.size(); ++i)
    {
        DriverData *dd = EventData::getInstance().getDriverDataByIdPtr(dpData[i].carID);

        if (dd)
        {
            setItem(i, 0, dd->getDriverName(), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignLeft | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));
            setItem(i, 1, dpData[i].bestTime.toString(), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::GREEN));
            setItem(i, 2, QString::number(dpData[i].bestTimeLapNumber), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));

            QString avgTime = dpData[i].avgTime.toString();

            //remove '0:'
            if (timesViewState != SHOW_LAP_TIME)
                avgTime = avgTime.right(avgTime.size() - 2);


            setItem(i, 3, avgTime, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::CYAN));

            QString gap = "";

            if (i > 0)
                gap = DriverData::calculateGap(dpData[i].avgTime, dpData.first().avgTime);

            setItem(i, 4, gap, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignRight | Qt::AlignVCenter, ColorsManager::getInstance().getColor(LTPackets::YELLOW));


            setItem(i, 5, dpData[i].totalTime, Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));

            setItem(i, 6, QString::number(dpData[i].laps.size()), Qt::ItemIsSelectable | Qt::ItemIsEnabled, Qt::AlignCenter, ColorsManager::getInstance().getColor(LTPackets::WHITE));

            QTableWidgetItem *item = ui->timesTableWidget->verticalHeaderItem(i);

            if (!item)
            {
                item = new QTableWidgetItem();
                ui->timesTableWidget->setVerticalHeaderItem(i, item);
            }

            item->setText(QString::number(i + 1));
            item->setTextAlignment(Qt::AlignVCenter | Qt::AlignRight);
        }
    }
    for (int j = ui->timesTableWidget->rowCount()-1; j >= dpData.size(); --j)
        ui->timesTableWidget->removeRow(j);
}

void PerformanceCompareWidget::saveSettings(QSettings &settings)
{
    settings.setValue("ui/performance_compare_geometry", saveGeometry());

    QList<QVariant> columnSizes;
    for (int i = 0; i < ui->timesTableWidget->columnCount(); ++i)
        columnSizes << ui->timesTableWidget->columnWidth(i);

    settings.setValue("ui/performance_compare_column_sizes", columnSizes);
}

void PerformanceCompareWidget::loadSettings(QSettings &settings)
{
    restoreGeometry(settings.value("ui/performance_compare_geometry").toByteArray());

    QList<QVariant> columnSizes = settings.value("ui/performance_compare_column_sizes").toList();
    for (int i = 0; i < columnSizes.size(); ++i)
    {
        if (columnSizes[i].toInt() > 0)
            ui->timesTableWidget->setColumnWidth(i, columnSizes[i].toInt());
    }
}

void PerformanceCompareWidget::on_closeButton_clicked()
{
    close();
}

void PerformanceCompareWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        on_closeButton_clicked();
}

void PerformanceCompareWidget::on_sectorBox_currentIndexChanged(int index)
{
    timesViewState = (TimesViewState)index;

    for (int i = 0; i < dpData.size(); ++i)
        dpData[i].calculateBestAndAvgTime(timesViewState);

    qSort(dpData);

    updateTable();
}
