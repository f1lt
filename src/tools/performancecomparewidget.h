/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef PERFORMANCECOMPAREWIDGET_H
#define PERFORMANCECOMPAREWIDGET_H

#include <QKeyEvent>
#include <QSettings>
#include <QTableWidgetItem>
#include <QWidget>
#include "../core/colorsmanager.h"
#include "../core/lapdata.h"

namespace Ui {
class PerformanceCompareWidget;
}

class LapDataComparator
{
public:
    bool operator()(LapData &ld1, LapData &ld2)
    {
        return (ld1.getCarID() < ld2.getCarID());
    }
};

enum TimesViewState
{
    SHOW_LAP_TIME = 0,
    SHOW_S1,
    SHOW_S2,
    SHOW_S3
};

struct DriverPerformanceData
{
    QList<LapData> laps;
    LapTime avgTime;
    LapTime bestTime;
    LapTime totalTime;
    int bestTimeLapNumber;
    int carID;

    bool operator < (const DriverPerformanceData &dp) const
    {
        return avgTime < dp.avgTime;
    }

    void calculateBestAndAvgTime(TimesViewState viewState)
    {
        int sum = 0;
        int n = 0;
        bestTime = LapTime();
        avgTime = LapTime();
        totalTime = LapTime();
        bestTimeLapNumber = 0;

        if (!laps.isEmpty())
        {
            LapTime lt = laps.first().getTime();

            if (viewState != SHOW_LAP_TIME)
                lt = laps.first().getSectorTime((int)viewState);

            if (lt.isValid())
            {
                bestTime = lt;
                bestTimeLapNumber = laps.first().getLapNumber();

                sum = bestTime.toMsecs();
                ++n;
            }

            for (int i = 1; i < laps.size(); ++i)
            {
                lt = laps[i].getTime();

                if (viewState != SHOW_LAP_TIME)
                    lt = laps[i].getSectorTime((int)viewState);

                if (lt < bestTime || !bestTime.isValid())
                {
                    bestTime = lt;
                    bestTimeLapNumber = laps[i].getLapNumber();
                }

                if (lt.isValid())
                {
                    sum += lt.toMsecs();
                    ++n;
                }
            }

            if (n > 0)
            {
                totalTime = LapTime(sum);
                avgTime = LapTime(sum / n);
            }
        }
    }

};

class PerformanceCompareWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PerformanceCompareWidget(QWidget *parent = 0);
    ~PerformanceCompareWidget();

    void exec(QList<LapData> &laps);

    void updateTable();

    void saveSettings(QSettings &settings);
    void loadSettings(QSettings &settings);
    
private slots:
    void on_closeButton_clicked();

    void on_sectorBox_currentIndexChanged(int index);

protected:
    void keyPressEvent(QKeyEvent *);

private:

    QTableWidgetItem* setItem(int row, int col, QString text = "", Qt::ItemFlags flags = Qt::NoItemFlags, int align = Qt::AlignCenter,
                     QColor textColor = ColorsManager::getInstance().getColor(LTPackets::DEFAULT), QBrush background = QBrush());

    Ui::PerformanceCompareWidget *ui;

    QList<DriverPerformanceData> dpData;

    TimesViewState timesViewState;
};

#endif // PERFORMANCECOMPAREWIDGET_H
