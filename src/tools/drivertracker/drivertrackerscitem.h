/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERTRACKERSCITEM_H
#define DRIVERTRACKERSCITEM_H

#include <QGraphicsObject>
#include "drivertrackertimer.h"


namespace DriverTrackerTool
{

class DriverTrackerSCItem : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DriverTrackerSCItem(DriverTrackerTimer *timer, QGraphicsItem *parent = 0);

    QRectF boundingRect () const;
    void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    void getLeaderDriver();

    virtual void setViewScale(double scaleFactor)
    {
        viewScale = scaleFactor;

        resetMatrix();

        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
    }

    virtual void setViewRotation(int rotation)
    {
        viewRotation = rotation;

        resetMatrix();

        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
    }

    void updateScaleAndRotation()
    {
        resetMatrix();
        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
        update();
    }
    
signals:
    
public slots:

    void updatePosition();

private:
    DriverTrackerTimer *dtTimer;
    DriverTrackerPositioner *leaderDriver;
    
    double viewScale;
    int viewRotation;

    QPixmap scHelmet;
};

}

#endif // DRIVERTRACKERSCITEM_H
