/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERRADARAREA_H
#define DRIVERRADARAREA_H

#include <QGraphicsObject>
#include "driverradaritem.h"
#include "drivertrackertimer.h"

namespace DriverTrackerTool
{

class DriverRadarArea : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DriverRadarArea(DriverTrackerTimer *timer, QGraphicsItem *parent = 0);



    virtual QRectF boundingRect () const;
    virtual void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual void setup();

    virtual void updatePosition();

    virtual void checkSetupCorrect();
    virtual void loadDriversList();


    virtual void setViewRotation(int)
    {
    }

    virtual void setViewScale(double)
    {
    }

    void updateSize(int width);
    
signals:
    void driverSelected(int id);
    
public slots:

    void onDRDriverSelected(DriverRadarItem *driver);
    void selectDriver(int id);
    void excludeDriver(int id, bool exclude);

protected:
    QVector<DriverRadarItem*> drp;
    DriverTrackerTimer *dtTimer;

    int selectedDriver;

private:
    int radarX, radarY;
    double radarR;
    double radarPitR;
    double radarLappedR;
    bool scOnTrack;
};

}
#endif // DRIVERRADARAREA_H
