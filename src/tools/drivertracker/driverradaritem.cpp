/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverradaritem.h"

#include "../../core/eventdata.h"
#include "../../core/colorsmanager.h"

#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>


namespace DriverTrackerTool
{



DriverRadarItem::DriverRadarItem(DriverRadarPositioner *positioner, QGraphicsItem *parent) :
    QGraphicsObject(parent), viewScale(1.0), viewRotation(0), selected(false), excluded(false)
{
    label[NORMAL] = QPixmap(":/ui_icons/label-driver.png").scaledToHeight(LABEL_SIZE, Qt::SmoothTransformation);
    label[SELECTED] = QPixmap(":/ui_icons/label-driver-sel.png").scaledToHeight(LABEL_SIZE, Qt::SmoothTransformation);
    label[LEADER] = QPixmap(":/ui_icons/label-driver-leader.png").scaledToHeight(LABEL_SIZE, Qt::SmoothTransformation);
    label[RETIRED] = QPixmap(":/ui_icons/label-driver-retired.png").scaledToHeight(LABEL_SIZE, Qt::SmoothTransformation);
    label[LAPPED] = QPixmap(":/ui_icons/label-driver-lapped.png").scaledToHeight(LABEL_SIZE, Qt::SmoothTransformation);

    setDriverRadarPositioner(positioner);
}

void DriverRadarItem::setFlags()
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}


void DriverRadarItem::setDriverRadarPositioner(DriverRadarPositioner *positioner)
{
    if (positioner)
    {
        drPositioner = positioner;
        connect (drPositioner, SIGNAL(statusChanged()), this, SLOT(update()), Qt::UniqueConnection);
    }
}

void DriverRadarItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        if (!selected)
        {
            selected = true;
            emit driverSelected(this);
            setZValue(2);
        }
        else
        {
            selected = false;
            setZValue(1);
            emit driverSelected(NULL);
        }
        update();
    }
}

QRectF DriverRadarItem::boundingRect() const
{
    if (drPositioner && drPositioner->getDriverData() && drPositioner->getDriverData()->getCarID() > 0)
    {
        int size = HELMET_SIZE;

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);

        int w = helmet.width() > label[0].width() ? helmet.width() : label[0].width();

        return QRectF(-w/2 - 4, -helmet.height()/2, w + 8, helmet.height() + OFFSET * 2 + label[0].height());
    }
    return QRectF();
}

void DriverRadarItem::paint (QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if (!drPositioner || !drPositioner->getDriverData() || (drPositioner->getDriverData()->getCarID() < 1))
        return;

    LTPackets::EventType eType = EventData::getInstance().getEventType();
    if (!excluded && (eType != LTPackets::RACE_EVENT || !drPositioner->getDriverData()->isRetired()))
    {
        p->setClipRect( option->exposedRect );
        drPositioner->setTrackPosition();
        setPos(drPositioner->getPos());

        QPen namePen(QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE)));

        QPixmap drvLabel = label[NORMAL];

        if (selected)
        {
            drvLabel = label[SELECTED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        else if (drPositioner->getDriverData()->getPosition() == 1)
        {
            drvLabel = label[LEADER];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        else if (drPositioner->getDriverData()->isRetired() || drPositioner->isQualiOut())
        {
            drvLabel = label[RETIRED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE));
        }
        else if (drPositioner->isLapped())
        {
            drvLabel = label[LAPPED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), HELMET_SIZE);
        p->drawPixmap(-helmet.width()/2, boundingRect().y(), helmet);

        int labelX = -drvLabel.width() / 2;
        int labelY = boundingRect().y() + helmet.height() + OFFSET;

        int fontSize = (int)((double)LABEL_SIZE / 2);
        p->setFont(QFont("Arial", fontSize, 75));

        QString name = QString("%1 %2").arg(drPositioner->getDriverData()->getPosition()).arg(SeasonData::getInstance().getDriverShortName(drPositioner->getDriverData()->getDriverName()));

        p->setPen(namePen);

        p->drawPixmap(-drvLabel.width()/2, labelY, drvLabel);


        int numX = labelX + (drvLabel.width() - p->fontMetrics().width(name))/2;
        int numY = labelY + (drvLabel.height() + p->fontMetrics().height())/2 - 1;
        p->drawText(numX, numY, name);

    }
}



}
