/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERTRACKERAREA_H
#define DRIVERTRACKERAREA_H

#include <QGraphicsObject>

#include <QGraphicsSceneMouseEvent>
#include "driverradararea.h"
#include "drivertrackeritem.h"
#include "drivertrackerscitem.h"

namespace DriverTrackerTool
{

class DriverTrackerArea : public DriverRadarArea
{
    Q_OBJECT
public:
    explicit DriverTrackerArea(DriverTrackerTimer *timer, QGraphicsItem *parent = 0);

    virtual QRectF boundingRect () const;
    virtual void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual void setup();

    virtual void checkSetupCorrect();
    virtual void loadDriversList();

    virtual void updatePosition();

    virtual void setViewRotation(int rotation)
    {
        for (int i = 0; i < drp.size(); ++i)
            drp[i]->setViewRotation(rotation);

        scItem->setViewRotation(rotation);

        viewRotation = rotation;
    }

    virtual void setViewScale(double scale)
    {
        for (int i = 0; i < drp.size(); ++i)
            drp[i]->setViewScale(scale);

        scItem->setViewScale(scale);
        viewScale = scale;
    }

    void updateAll()
    {
        for (int i = 0; i < drp.size(); ++i)
            drp[i]->update();
    }

    void paintDRSZones(QPainter *painter);
    void paintTrackLabels(QPainter *painter);

    DriverTrackerItem *getSelectedDriver();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

public slots:
    void onDTPDriverSelected(DriverTrackerItem *driver);

private:
    QPixmap trackMap;
    QPixmap trackLabels[3];

    int viewRotation;
    double viewScale;

    enum TrackLabelType
    {
        S1, S2, DRS
    };

    DriverTrackerSCItem *scItem;
};

}
#endif // DRIVERTRACKERAREA_H
