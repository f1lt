/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "driverradarpositioner.h"

#include <QDebug>

#include "../../core/colorsmanager.h"
#include "../../core/eventdata.h"

namespace DriverTrackerTool
{

DriverRadarPositioner::DriverRadarPositioner(QObject *parent, DriverData *dd, int speed, int x, int y, double r, double r1, double rL) :
    QObject(parent),
    driverData(dd), avgTime(100), currentSector(1), currentPositionIndex(0), finishInLapPositionIndex(0), currentLapTime(0), startingNewLap(false),
    inPits(true), lapped(false), prevLapped(false), qualiOut(false), racingState(RACING), wetTrack(false), fps(speed), missedFLCounter(0),
    prevPosition(0), radarX(x), radarY(y), radarR(r), radarPitR(r1), radarLappedR(rL)
{
    avgSectorTimes[0] = 0.0;
    avgSectorTimes[1] = 0.0;
    sectorPositions[0] = 0.0;
    sectorPositions[1] = 0.0;
}

void DriverRadarPositioner::setDriverData(DriverData *dd)
{
    driverData = dd;
}

void DriverRadarPositioner::setStartupPosition()
{
    int laps = EventData::getInstance().getEventInfo().laps;
    lapped = false;
    prevLapped = false;
    qualiOut = false;
    inPits = false;    
    wetTrack = false;

    racingState = RACING;

    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

    if (trackCoordinates)
    {
        //mapping S1 and S2 positions from track map onto radar
        sectorPositions[0] = (double)trackCoordinates->indexes[TrackCoordinates::S1] * 360.0 / (double)trackCoordinates->coordinates.size();
        sectorPositions[1] = (double)trackCoordinates->indexes[TrackCoordinates::S2] * 360.0 / (double)trackCoordinates->coordinates.size();
    }
    else
    {
        sectorPositions[0] = 0.0;
        sectorPositions[1] = 0.0;
    }

    noSmoothSync = true;

    bool sessionFinished = EventData::getInstance().isSessionFinished();

    //a very rough estimate ;)
    avgTime = 200 - 30 * log10(laps*laps);

    if (EventData::getInstance().getSessionRecords().getFastestLap().getTime().isValid())
        avgTime = EventData::getInstance().getSessionRecords().getFastestLap().getTime().toDouble();

    //if it's raining, we add 10 seconds
    if (EventData::getInstance().getWeather().getWetDry().getValue() == 1)
        avgTime += 10;

    //if SC is on track, we add 1 minute
    if (EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED)
        avgTime += 60;

    if (driverData && (driverData->isInPits() || driverData->isRetired() || sessionFinished))
    {
        if (sessionFinished)
            racingState = FINISHED;

        goToPit(true);
        calculatePitPosition();
    }

    else if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT &&
        (!EventData::getInstance().isSessionStarted() || !driverData->getLastLap().getSectorTime(1).isValid()))
    {
        goToPit(false);        
        currentPositionIndex = 360 - driverData->getPosition()*2;
        currentLapTime = -(avgTime-(avgTime * currentPositionIndex) / 360);
    }
    else
    {
        currentLapTime = 0;
        for (int i = 0; i < 3; ++i)
            currentLapTime += driverData->getLastLap().getSectorTime(i+1).toDouble();

        calculatePosition();
    }
}

void DriverRadarPositioner::updatePosition()
{
    if (driverData)
    {
        if (startingNewLap && driverData->getLastLap().getSectorTime(3).isValid())
        {            
            if (noSmoothSync)
            {
                noSmoothSync = false;
            }

            currentSector = 1;
            currentLapTime = 0;
            calculateAvgs();
            startingNewLap = false;

            setSynchronizationPointOnTrackReached();
        }
        if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT && driverData->getLastLap().getSectorTime(3).isValid() &&
                EventData::getInstance().getCompletedLaps() == EventData::getInstance().getEventInfo().laps &&
                ((racingState == FINISHED) || ((fabs(numberOfIndexes() - currentPositionIndex) < 5) && (racingState == FINISH_IN_LAP))))
        {
            currentPositionIndex = 0;
            racingState = FINISHED;
            goToPit(true);
            calculatePitPosition();
        }

        else if (driverData->isInPits() || driverData->isRetired() ||
            (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT && EventData::getInstance().getFlagStatus() == LTPackets::RED_FLAG))
        {
            goToPit(true);
            racingState = RACING;
            calculatePitPosition();
        }
        else if ((EventData::getInstance().getEventType() != LTPackets::RACE_EVENT) &&
                 (missedFLCounter > 2))
        {
            //driver missed finish line 2 times in a row so probably he has retired
            goToPit(true);
            driverData->setRetired(true);
            calculatePitPosition();
        }

        else
        {
            if (inPits)
            {
                //on out lap disable smooth syncing
                noSmoothSync = true;
                calculatePitOutPosition();
            }

            goToPit(false);
            if (racingState != FINISH_IN_LAP)
               racingState = RACING;

           if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT &&
              (EventData::getInstance().getCompletedLaps() != EventData::getInstance().getEventInfo().laps))
                racingState = RACING;

            qualiOut = false;
            calculatePosition();
        }
        if (!driverData->getLastLap().getSectorTime(3).isValid())
        {
            startingNewLap = true;
        }
        setTrackPosition();

        emit positionUpdated();

        if (prevPosition != driverData->getPosition())// &&
           //((prevPosition == 1) || (driverData->getPosition() == 1)))
        {
            emit statusChanged();
        }
        prevPosition = driverData->getPosition();
    }
}

void DriverRadarPositioner::calculatePosition()
{
    EventData &ev = EventData::getInstance();
    if (!ev.isSessionStarted())
        return;

    if (currentPositionIndex > 0)
    {        
        if (driverData->getLastLap().getSectorTime(1).isValid() && ((currentSector == 1) || ((currentSector == 0) && noSmoothSync)) &&
            ((ev.getEventType() == LTPackets::RACE_EVENT && driverData->getColorData().lapTimeColor() == LTPackets::YELLOW) ||
            (!driverData->getLastLap().getSectorTime(2).isValid() || !driverData->getLastLap().getSectorTime(3).isValid())))
        {
            currentSector = 2;
            setSynchronizationPointOnTrackReached();
        }
        else if (driverData->getLastLap().getSectorTime(2).isValid() && ((currentSector == 2) || ((currentSector < 2) && noSmoothSync)) &&
                 !driverData->getLastLap().getSectorTime(3).isValid() &&
                 (driverData->getLastLap().getTime().toString() != "OUT"))
        {
            currentSector = 3;            
            setSynchronizationPointOnTrackReached();
        }
        else
        {

            double step = (360.0 / avgTime) / (double)fps;

            if (syncData.syncCounter > 0)
                step /= 2;

            if (syncData.syncCounter < 0)
                step *= 2;

            currentPositionIndex += step; //(360 * currentLapTime) / avgTime;

            if ((int)(round(currentPositionIndex)) >= numberOfIndexes())
            {
                currentPositionIndex = 0;
                ++missedFLCounter;

                if ((syncData.countingDirection == SynchronizationData::NONE) && !noSmoothSync)
                {
                    syncData.passedSyncPointOnMap = true;
                    syncData.countingDirection = SynchronizationData::UP;
                    syncData.synchronizing = true;
                }
            }
            if (!noSmoothSync)
                detectSynchronizationPointOnMapReached();
        }
        if (racingState == FINISH_IN_LAP)
            finishInLapPositionIndex += (360.0 / avgTime) / fps;
    }
    else
    {
            currentPositionIndex += (360.0 / avgTime) / fps;
    }        
}

void DriverRadarPositioner::calculatePitPosition()
{
    int no = driverData->getNumber() - 1;
    if (no > 12)
        no -= 1;
    currentPositionIndex = no * 7.75;

    EventData &ev = EventData::getInstance();
    if (ev.getEventType() == LTPackets::QUALI_EVENT &&
        ((ev.getQualiPeriod() == 2 && driverData->getPosition() > 16) ||
         (ev.getQualiPeriod() == 3 && driverData->getPosition() > 10)))
    {
        bool prevQualiOut = qualiOut;
        qualiOut = true;

        if (prevQualiOut == false)
            emit statusChanged();

    }
    else
    {
        bool prevQualiOut = qualiOut;
        qualiOut = false;

        if (prevQualiOut == true)
            emit statusChanged();
    }
}

void DriverRadarPositioner::setSynchronizationPointOnTrackReached()
{
    int sectorMaxDiff = (currentSector % 3) + 1;

    double trackPositionDiff;

    if (currentSector == 1)
    {
        if (syncData.passedSyncPointOnMap)
            trackPositionDiff = currentPositionIndex;
        else
            trackPositionDiff = numberOfIndexes() - currentPositionIndex;
    }
    else
    {
        trackPositionDiff = qAbs(sectorIndex(currentSector - 2) - currentPositionIndex);
    }

    if ((((double)(qAbs(syncData.syncCounter)) / (double)fps) > syncData.SYNC_LIMIT) || (syncData.sector == sectorMaxDiff) || noSmoothSync || (trackPositionDiff > 0.3 * (double)numberOfIndexes()))
    {
        if (currentSector > 1)
            currentPositionIndex = sectorIndex(currentSector - 2);
        else
            currentPositionIndex = 0;

        syncData.reset(currentSector);
    }
    else
    {
        syncData.passedSyncPointOnTrack = true;

        if (syncData.passedSyncPointOnMap)
        {
            if (syncData.syncCounter != 0)
            {
                syncData.countingDirection = SynchronizationData::DOWN;
            }
            else
            {
                syncData.reset(currentSector);
            }
        }

        else if ((syncData.countingDirection == SynchronizationData::NONE) &&
                 (((currentSector > 1) && ((currentPositionIndex < sectorIndex(currentSector-2)))) ||
                 ((currentSector == 1) && (currentPositionIndex < numberOfIndexes()) && (currentPositionIndex > sectorIndex(1)))))
        {
            syncData.passedSyncPointOnMap = false;
            syncData.countingDirection = SynchronizationData::DOWN;
            syncData.synchronizing = true;
        }
    }
}

void DriverRadarPositioner::detectSynchronizationPointOnMapReached()
{
    //driver passed synchronization point on the map, but in reality he still hasn't reached it
    if ((racingState != FINISH_IN_LAP) && (syncData.sector == currentSector) && (currentSector < 3) &&
        (currentPositionIndex >= sectorIndex(currentSector-1)) &&
        (syncData.countingDirection == SynchronizationData::NONE))
    {
        syncData.passedSyncPointOnMap = true;
        syncData.countingDirection = SynchronizationData::UP;
        syncData.synchronizing = true;
    }

    //driver passed sync point on the track before reaching it on the map
    else if (((syncData.countingDirection == SynchronizationData::DOWN) && !syncData.passedSyncPointOnMap) && syncData.passedSyncPointOnTrack
             && (
                ((racingState != FINISH_IN_LAP) &&
                ((currentSector > 1) && (currentPositionIndex >= sectorIndex(currentSector-2))))
                ||
                ((currentSector == 1) && (currentPositionIndex >= 0) && (currentPositionIndex < sectorIndex(0)))))
    {
        if (syncData.syncCounter != 0)
        {
            syncData.countingDirection = SynchronizationData::UP;
            syncData.passedSyncPointOnMap = true;
            syncData.sector = currentSector;
        }
        else
        {
            syncData.reset(currentSector);
            if (currentSector == 1)
                missedFLCounter = 0;
        }
    }

    if ((syncData.countingDirection == SynchronizationData::UP))
        syncData.syncCounter++;

    if ((syncData.countingDirection == SynchronizationData::DOWN))
        syncData.syncCounter--;

    if (syncData.syncCounter == 0)
    {
        syncData.reset(currentSector);

        if (currentSector == 1)
            missedFLCounter = 0;
    }
}

void DriverRadarPositioner::calculateAvgs()
{
    if (driverData)
    {
        double sumT = 0, sumS1 = 0, sumS2 = 0;
        int k = 0, ks1=0, ks2=0;

        LapData last = driverData->getLastLap();

        if (!driverData->getLapData().isEmpty())
            last = driverData->getLapData().last();

        int i = driverData->getLapData().size()-1;


        //if the current lap is a second lap out of pits, synchronization has to be done to the best lap, not the last which was slow due to the pit stop
        if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT && i > 1 &&
            (driverData->getLapData()[i-1].getRaceLapExtraData().isPitLap() || driverData->getLapData()[i-1].getRaceLapExtraData().isSCLap()) &&
            !last.getRaceLapExtraData().isPitLap() &&
            !last.getRaceLapExtraData().isSCLap())
        {
            last = driverData->getSessionRecords().getBestLap();

            //increase the time by 5 seconds
            last.setTime(LapTime(last.getTime().toMsecs() + 5000));

            sumT = last.getTime().toDouble();
            k = 1;
        }

        //if the session wasn't dry, durig quali and fp avg time will be calculated using last lap, not the best one
        if ((EventData::getInstance().getEventType() != LTPackets::RACE_EVENT) &&
            !wetTrack && (EventData::getInstance().getWeather().getWetDry().getValue() == 1))
            wetTrack = true;

        if ((EventData::getInstance().getEventType() != LTPackets::RACE_EVENT) && !wetTrack)
        {
            last = driverData->getSessionRecords().getBestLap();
            sumT = last.getTime().toDouble();
            k = 1;
        }

        for (i = driverData->getLapData().size()-1; i >= driverData->getLapData().size()-4 && i >= 0; --i)
        {
            LapData ld = driverData->getLapData()[i];                        

            if (ld.getTime().isValid() &&
               (ld.getRaceLapExtraData().isSCLap() == last.getRaceLapExtraData().isSCLap()) &&
                    (fabs(ld.getTime().toDouble() - last.getTime().toDouble()) < 5))
            {
                sumT += ld.getTime().toDouble();                
                ++k;
            }

            if (ld.getSectorTime(1).isValid() &&
                fabs(ld.getSectorTime(1).toDouble()-last.getSectorTime(1).toDouble()) < 5)
            {
                sumS1 += ld.getSectorTime(1).toDouble();
                ++ks1;
            }

            if (ld.getSectorTime(2).isValid() &&
                fabs(ld.getSectorTime(2).toDouble()-last.getSectorTime(2).toDouble()) < 5)
            {
                sumS2 += ld.getSectorTime(2).toDouble();
                ++ks2;
            }
        }

        if (sumT != 0 && k > 0)
            avgTime = sumT / k;


        if ((sumS1 != 0) && (ks1 > 0) && (sumS2 != 0) && (ks2 > 0) &&
            ((sectorPositions[0] == 0) || (sectorPositions[1] == 0)))
        {

            avgSectorTimes[0] = sumS1 / ks1;
            sectorPositions[0] = (360.0 * avgSectorTimes[0]) / avgTime;

            avgSectorTimes[1] = sumS2 / ks2;
            sectorPositions[1] = (360.0 * (avgSectorTimes[1] + avgSectorTimes[0])) / avgTime;
        }

        if (driverData->getPosition() != 1 && driverData->getLastLap().getGap().contains("L"))
            lapped = true;
        else
            lapped = false;

        if (prevLapped != lapped)
        {
            emit statusChanged();
            prevLapped = lapped;
        }


        if (driverData->getLastLap().getRaceLapExtraData().isPitLap() || driverData->isRetired())
            goToPit(true);
        else        
            goToPit(false);


        if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT && driverData->getLastLap().getSectorTime(3).isValid() &&
            EventData::getInstance().getCompletedLaps() == EventData::getInstance().getEventInfo().laps)
        {
            racingState = FINISH_IN_LAP;
            avgTime += 40;
            finishInLapPositionIndex = 0;
        }

    }
}

void DriverRadarPositioner::setTrackPosition()
{
    double r = radarR;

    if (inPits/* && !qualiOut*/)
        r = radarPitR;

    else if (lapped/* || qualiOut*/)
        r = radarLappedR;    

    x = radarX, y = radarY - r;
    if (currentPositionIndex > 0)
    {        
        double alpha = currentPositionIndex / 180 * M_PI;// - quarter * 90;

        x = radarX + sin(alpha) * r;
        y = radarY - cos(alpha) * r;
    }
}



void DriverRadarPositioner::goToPit(bool pit)
{
    /* this is only to force QGraphicsItem to refresh its cache when driver goes to pits */
    if (pit)
    {
        if (!inPits)
        {
            inPits = true;
            emit statusChanged();
        }
    }
    else
    {
        if (inPits)
        {
            inPits = false;
            noSmoothSync = true;
            missedFLCounter = 0;
            emit statusChanged();
        }
    }
}



}

