/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERRADARPOSITIONER_H
#define DRIVERRADARPOSITIONER_H


#include <QObject>
#include "../../core/driverdata.h"

namespace DriverTrackerTool
{

class DriverRadarPositioner : public QObject
{
    Q_OBJECT
public:
    DriverRadarPositioner(QObject *parent = 0, DriverData *dd = 0, int fps = 40, int x = 0, int y = 0, double r = 0.0, double rP = 0.0, double rL = 0.0);
    virtual ~DriverRadarPositioner() { }

    virtual QPoint getCoordinates()
    {
        setTrackPosition();
        return QPoint(x, y);
    }

    virtual void setTrackPosition();
    virtual void updatePosition();


    void calculateAvgs();

    void setDriverData(DriverData *dd);

    virtual void setStartupPosition();

    void setRadarCoords(int x, int y, double r, double rP, double rL)
    {
        radarX = x;
        radarY = y;
        radarR = r;
        radarPitR = rP;
        radarLappedR = rL;

//        update();
    }

    virtual void calculatePosition();
    virtual void calculatePitPosition();
    virtual void calculatePitOutPosition()
    {
        currentPositionIndex = 0;
        syncData.reset(currentSector);
    }

    virtual void setSynchronizationPointOnTrackReached();
    virtual void detectSynchronizationPointOnMapReached();

    virtual int numberOfIndexes() const
    {
        return 360;
    }

    virtual int sectorIndex(int sector)
    {
        if (sectorPositions[sector] == 0)
        {
            return (360 / 3) * (sector + 1);
        }
        return sectorPositions[sector];
    }

    void setFPS(int s)
    {
        fps = s;
    }

    int getDriverId()
    {
        if (driverData != 0)
            return driverData->getCarID();

        return -1;
    }

    DriverData *getDriverData()
    {
        return driverData;
    }

    QPointF getPos()
    {
        return QPointF(x, y);
    }


    void goToPit(bool pit);
    bool isInPits()
    {
        return inPits;
    }

    bool isQualiOut() const
    {
        return qualiOut;
    }

    bool isFinished() const
    {
        return (racingState == FINISHED);
    }
    bool isLapped() const
    {
        return lapped;
    }

    double getCurrentDeg() const
    {
        return currentPositionIndex;
    }

    int getFPS() const
    {
        return fps;
    }

    double getAvgTime() const
    {
        return avgTime;
    }

    void setNoSmoothSync(bool value)
    {
        noSmoothSync = value;
        syncData.reset(currentSector);
    }


signals:
    void positionUpdated();
    void statusChanged();

protected:


    DriverData *driverData;
    double avgTime;
    double avgSectorTimes[2];
    double sectorPositions[2];
    int currentSector;
    double currentPositionIndex;
    double finishInLapPositionIndex;

    int currentLapTime;
    bool startingNewLap;

    bool inPits;
    bool lapped;
    bool prevLapped;

    bool qualiOut;

    enum RacingState
    {
        RACING = 0, FINISH_IN_LAP, FINISHED
    };
    RacingState racingState;

    bool noSmoothSync;

    bool wetTrack;

    int fps;

    double x, y;

    /* Sometimes driver goes out of the track during FP or quali, but he is not set as "retired" by server,
     * so he goes round and round on the track. This will be used to put the driver into the pits if
     * he misses finish line few times in a row
     */
    int missedFLCounter;

    int prevPosition;

    struct SynchronizationData
    {
        enum sync_direction
        {
            NONE = 0,
            UP,
            DOWN
        };

        bool synchronizing;
        bool passedSyncPointOnMap;
        bool passedSyncPointOnTrack;

        sync_direction countingDirection;
        int syncCounter;
        int sector;

        SynchronizationData() : SYNC_LIMIT(15)
        {
            syncCounter = 0;
            sector = 0;
            countingDirection = NONE;
            passedSyncPointOnMap = false;
            synchronizing = false;
            passedSyncPointOnTrack = false;
        }

        void reset(int sec = 0)
        {
            syncCounter = 0;
            sector = sec;
            countingDirection = NONE;

            passedSyncPointOnMap = false;
            synchronizing = false;
            passedSyncPointOnTrack = false;
        }

        const int SYNC_LIMIT;
    };

    SynchronizationData syncData;

private:
    int radarX, radarY;
    double radarR, radarPitR, radarLappedR;    
};

}
#endif // DRIVERRADARPOSITIONER_H
