/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERTRACKERITEM_H
#define DRIVERTRACKERITEM_H

#include "driverradaritem.h"
#include "drivertrackerpositioner.h"

namespace DriverTrackerTool
{

class DriverTrackerItem : public DriverRadarItem
{
    Q_OBJECT
public:
    explicit DriverTrackerItem(DriverRadarPositioner *positioner, QGraphicsItem *parent = 0);

    virtual void setFlags();

    QRectF boundingRect () const;
    void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);


    virtual void setViewScale(double scaleFactor)
    {
        viewScale = scaleFactor;

        resetMatrix();

        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
        rescaleLabels();
    }

    virtual void setViewRotation(int rotation)
    {
        viewRotation = rotation;

        resetMatrix();

        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
    }

    void updateScaleAndRotation()
    {
        resetMatrix();
        scale(1.0/viewScale, 1.0/viewScale);
        rotate(-viewRotation);
        update();
    }

    void rescaleLabels();
    void setMapCoords(int x, int y, int w, int h)
    {
        if (drPositioner)
        {
            static_cast<DriverTrackerPositioner*>(drPositioner)->setMapCoords(x, y, w, h);
        }
    }
    
signals:
    void driverSelected(DriverTrackerItem *driver);
    
public slots:

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

};

}

#endif // DRIVERTRACKERITEM_H
