/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERRADARSCENE_H
#define DRIVERRADARSCENE_H

#include <QDebug>
#include <QGraphicsScene>
#include <QVector>
#include <QWidget>

#include "driverradararea.h"
#include "driverradaritem.h"
#include "drivertrackerinfo.h"
#include "drivertrackertimer.h"

#include "../../core/eventdata.h"

namespace DriverTrackerTool
{

class DriverRadarScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit DriverRadarScene(DriverTrackerTimer *timer, QObject *parent = 0);
    virtual ~DriverRadarScene();
           
    virtual void loadDriversList();
    virtual void setupDrivers();

    virtual void checkSetupCorrect()
    {
        dtArea->checkSetupCorrect();
        updateSize();
    }

    virtual void rotate(int, int totalAngle)
    {
        dtArea->setViewRotation(totalAngle);
    }

    virtual void scale(double currentScale, double totalScale)
    {
        dtArea->scale(currentScale, currentScale);
        dtArea->setViewScale(totalScale);
    }

    void updatePosition()
    {        
        dtArea->updatePosition();
    }

public slots:
    virtual void selectDriver(int id)
    {
        dtArea->selectDriver(id);
        dtInfo->setDriver(id);
    }

    void excludeDriver(int id, bool exclude)
    {
        dtArea->excludeDriver(id, exclude);
    }

    void updateSize();

    virtual void sessionTimerUpdated()
    {
        dtInfo->update();
    }

signals:

    void driverSelected(int);
    void driverSelected(DriverRadarItem*);


protected:

    bool isExcluded(int id);


    QList<int> excludedDrivers;

    QVector<DriverRadarPositioner*> drp;

    int selectedDriver;

    DriverRadarArea *dtArea;
    DriverTrackerTimer *dtTimer;

private:
    DriverTrackerInfo *dtInfo;
};

}
#endif // DRIVERRADARSCENE_H
