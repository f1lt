/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverradarwidget.h"
#include "ui_driverradarwidget.h"

#include <QKeyEvent>

namespace DriverTrackerTool {


DriverRadarWidget::DriverRadarWidget(DriverTrackerTimer *timer, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DriverRadarWidget)
{
    ui->setupUi(this);

    driverRadarScene = new DriverRadarScene(timer, this);

    connect (driverRadarScene, SIGNAL(driverSelected(int)), this, SIGNAL(driverSelected(int)));

    ui->driverRadarView->setScene(driverRadarScene);
    ui->driverRadarView->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
    ui->driverRadarView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing | QGraphicsView::DontSavePainterState);
    ui->driverRadarView->setCacheMode(QGraphicsView::CacheBackground);

    connect (timer, SIGNAL(positionsUpdated()), this, SLOT(update()));
}

DriverRadarWidget::~DriverRadarWidget()
{
    delete ui;
}

void DriverRadarWidget::loadSettings(QSettings *settings)
{
    restoreGeometry(settings->value("ui/driver_radar_geometry").toByteArray());
}

void DriverRadarWidget::saveSettings(QSettings *settings)
{
    settings->setValue("ui/driver_radar_geometry", saveGeometry());
}

void DriverRadarWidget::setup()
{
    driverRadarScene->loadDriversList();
    driverRadarScene->setupDrivers();

    setWindowTitle("Driver radar: " + EventData::getInstance().getEventInfo().eventName);
}

void DriverRadarWidget::exec()
{
    setWindowTitle("Driver radar: " + EventData::getInstance().getEventInfo().eventName);

    show();

    driverRadarScene->checkSetupCorrect();
}

void DriverRadarWidget::checkSetupCorrect()
{
    driverRadarScene->checkSetupCorrect();
}

void DriverRadarWidget::onDriverSelected(int id)
{
    driverRadarScene->selectDriver(id);
}

void DriverRadarWidget::sessionTimerUpdated()
{
    driverRadarScene->sessionTimerUpdated();
}

void DriverRadarWidget::update()
{
    driverRadarScene->updatePosition();
}

void DriverRadarWidget::excludeDriver(int id, bool exclude)
{
    driverRadarScene->excludeDriver(id, exclude);
}

void DriverRadarWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        close();
}

void DriverRadarWidget::closeEvent(QCloseEvent *event)
{
    emit closed();
    QWidget::closeEvent(event);
}

void DriverRadarWidget::resizeEvent(QResizeEvent *event)
{
    driverRadarScene->updateSize();
    QWidget::resizeEvent(event);
}

}

void DriverTrackerTool::DriverRadarWidget::on_closeButton_clicked()
{
    close();
}
