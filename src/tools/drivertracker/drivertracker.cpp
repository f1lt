/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertracker.h"


namespace DriverTrackerTool
{

DriverTracker::DriverTracker(QObject *parent) :
    QObject(parent)
{
    driverTrackerTimer = new DriverTrackerTimer(this);
    dtWidget = new DriverTrackerWidget(driverTrackerTimer);
    drWidget = new DriverRadarWidget(driverTrackerTimer);

    connect(drWidget, SIGNAL(closed()), this, SLOT(onWidgetClose()));
    connect(dtWidget, SIGNAL(closed()), this, SLOT(onWidgetClose()));
    connect(dtWidget, SIGNAL(driverSelected(int)), drWidget, SLOT(onDriverSelected(int)));
    connect(drWidget, SIGNAL(driverSelected(int)), dtWidget, SLOT(onRadarDriverSelected(int)));
    connect(dtWidget, SIGNAL(driverExcluded(int,bool)), drWidget, SLOT(excludeDriver(int,bool)));
    connect(dtWidget, SIGNAL(FPSChanged(int)), driverTrackerTimer, SLOT(setFPS(int)));
}

DriverTracker::~DriverTracker()
{
    delete dtWidget;
    delete drWidget;
}

void DriverTracker::loadSettings(QSettings *settings)
{
    dtWidget->loadSettings(settings);
    drWidget->loadSettings(settings);
}

void DriverTracker::saveSettings(QSettings *settings)
{
    dtWidget->saveSettings(settings);
    drWidget->saveSettings(settings);
}

bool DriverTracker::isVisible()
{
    return dtWidget->isVisible() || drWidget->isVisible();
}

void DriverTracker::close()
{
    drWidget->close();
    dtWidget->close();
}

void DriverTracker::sessionTimerUpdated()
{
    dtWidget->sessionTimerUpdated();
    drWidget->sessionTimerUpdated();
}

void DriverTracker::startTimer(int s)
{
    bool isVisible = dtWidget->isVisible() || drWidget->isVisible();

    if (driverTrackerTimer->isTimerActive())
    {
        driverTrackerTimer->setTimerInterval(s, !isVisible);
    }

    else
    {
        dtWidget->checkSetupCorrect();
        drWidget->checkSetupCorrect();

        driverTrackerTimer->startTimer(s, !isVisible);
    }
}

void DriverTracker::openDriverTracker()
{
    if (!dtWidget->isVisible())
    {
        int fps = dtWidget->getFPS();
        driverTrackerTimer->setFPS(fps);

        dtWidget->exec();
    }
    else
        dtWidget->raise();
}

void DriverTracker::openDriverRadar()
{
    if (!drWidget->isVisible())
    {
        int fps = dtWidget->getFPS();
        driverTrackerTimer->setFPS(fps);

        drWidget->exec();
    }
    else
        drWidget->raise();
}

void DriverTracker::setup()
{
    driverTrackerTimer->loadDriversList();
    dtWidget->setup();
    drWidget->setup();
}

void DriverTracker::onWidgetClose()
{
    bool isVisible = dtWidget->isVisible() || drWidget->isVisible();

    if (!isVisible)
        driverTrackerTimer->setFPS(1);
}

}
