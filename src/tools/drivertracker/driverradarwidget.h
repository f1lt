/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERRADARWIDGET_H
#define DRIVERRADARWIDGET_H

#include <QSettings>
#include <QWidget>

#include "driverradarscene.h"
#include "drivertrackertimer.h"


namespace Ui {
class DriverRadarWidget;
}

namespace DriverTrackerTool {

class DriverRadarWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit DriverRadarWidget(DriverTrackerTimer *timer, QWidget *parent = 0);
    ~DriverRadarWidget();

    void loadSettings(QSettings *settings);
    void saveSettings(QSettings *settings);

    void setup();

    void exec();
    void checkSetupCorrect();

signals:
    void driverSelected(int);
    void closed();

public slots:
    void onDriverSelected(int);
    void sessionTimerUpdated();
    void update();
    void excludeDriver(int, bool);

protected:
    void keyPressEvent(QKeyEvent *);
    void closeEvent(QCloseEvent *event);
    void resizeEvent(QResizeEvent *event);
    
private slots:
    void on_closeButton_clicked();

private:
    Ui::DriverRadarWidget *ui;

    DriverRadarScene *driverRadarScene;
};

}

#endif // DRIVERRADARWIDGET_H
