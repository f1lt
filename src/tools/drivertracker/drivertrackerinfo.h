/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERTRACKERINFO_H
#define DRIVERTRACKERINFO_H

#include <QGraphicsObject>
#include "../../core/driverdata.h"

namespace DriverTrackerTool
{

class DriverTrackerInfo : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DriverTrackerInfo(QGraphicsItem *parent = 0);

    QRectF boundingRect () const;
    void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    void setDriverData(DriverData *dd)
    {
        driverData = dd;        

        update();
    }

    void paintDriverInfo(QPainter *p);
    void paintLapsInfo(QPainter *p);
    void paintGapsInfo(QPainter *p);
    
signals:
    
public slots:
    void setDriver(int id);

private:
    QPixmap labelBig;
    QPixmap labelInfoLong;

    DriverData *driverData;
    
};

}
#endif // DRIVERTRACKERINFO_H
