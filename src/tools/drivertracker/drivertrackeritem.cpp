/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "../../core/eventdata.h"
#include "drivertrackeritem.h"

#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "../../core/colorsmanager.h"

namespace DriverTrackerTool
{

DriverTrackerItem::DriverTrackerItem(DriverRadarPositioner *positioner, QGraphicsItem *parent) :
    DriverRadarItem(positioner, parent)
{
}

void DriverTrackerItem::setFlags()
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}


QRectF DriverTrackerItem::boundingRect() const
{
    if (drPositioner && drPositioner->getDriverData() && drPositioner->getDriverData()->getCarID() > 0)
    {
        int size = HELMET_SIZE;

        if (viewScale >= 2.0)
        {
            size = HELMET_SIZE * 2;
        }
        else if (viewScale > 1.0)
        {
            size = HELMET_SIZE * viewScale;
        }
        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);

        int w = helmet.width() > label[0].width() ? helmet.width() : label[0].width();

        return QRectF(-w/2 - 9, -helmet.height()/2, w  + 18, helmet.height() + OFFSET * 2 + label[0].height());
    }
    return QRectF();
}

void DriverTrackerItem::rescaleLabels()
{
    int size = LABEL_SIZE;

    if (viewScale >= 2.0)
    {
        size = LABEL_SIZE * 2;
    }
    else if (viewScale > 1.0)
    {
        size = LABEL_SIZE * viewScale;
    }

    label[NORMAL] = QPixmap(":/ui_icons/label-driver.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[SELECTED] = QPixmap(":/ui_icons/label-driver-sel.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[LEADER] = QPixmap(":/ui_icons/label-driver-leader.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[RETIRED] = QPixmap(":/ui_icons/label-driver-retired.png").scaledToHeight(size, Qt::SmoothTransformation);
    label[LAPPED] = QPixmap(":/ui_icons/label-driver-lapped.png").scaledToHeight(size, Qt::SmoothTransformation);
}

void DriverTrackerItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
//    sprawdzic to!!
    if ((event->button() == Qt::LeftButton) && drPositioner && !drPositioner->isInPits())
    {
        if (!selected)
        {
            selected = true;
            emit driverSelected(this);
            setZValue(2);
            update();
        }
        else
        {
            selected = false;
            setZValue(1);
            emit driverSelected(NULL);
            update();
        }
    }
}

void DriverTrackerItem::paint(QPainter *p, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if (drPositioner && drPositioner->getDriverData() && (drPositioner->getDriverData()->getCarID() > 0) &&
         !drPositioner->isInPits() && !drPositioner->getDriverData()->isRetired() && !excluded && !drPositioner->isFinished())
    {
        p->setClipRect( option->exposedRect );
        drPositioner->setTrackPosition();
        setPos(drPositioner->getPos());

        QPen namePen(QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE)));

        QPixmap drvLabel = label[NORMAL];

        if (selected)
        {
            drvLabel = label[SELECTED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        else if (drPositioner->getDriverData()->getPosition() == 1)
        {
            drvLabel = label[LEADER];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }
        else if (drPositioner->getDriverData()->isRetired() || drPositioner->isQualiOut())
        {
            drvLabel = label[RETIRED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::WHITE));
        }
        else if (drPositioner->isLapped())
        {
            drvLabel = label[LAPPED];
            namePen = QPen(ColorsManager::getInstance().getColor(LTPackets::BACKGROUND));
        }


        int size = HELMET_SIZE;
        int fontSize = (int)((double)LABEL_SIZE / 2);

        if (viewScale >= 2.0)
        {
            size = HELMET_SIZE * 2;
            fontSize *= 2;
        }
        else if (viewScale > 1.0)
        {
            size = HELMET_SIZE * viewScale;
            fontSize *= viewScale;
        }

        p->setFont(QFont("Arial", fontSize, 75));

        QPixmap helmet = ImagesFactory::getInstance().getHelmetsFactory().getHelmet(drPositioner->getDriverData()->getNumber(), size);
        p->drawPixmap(-size/2, boundingRect().y()+1, helmet);

        int labelX = -drvLabel.width() / 2;
        int labelY = boundingRect().y() + helmet.height() + OFFSET;

        p->drawPixmap(-drvLabel.width()/2, labelY, drvLabel);

        QString name = QString("%1 %2").arg(drPositioner->getDriverData()->getPosition()).arg(SeasonData::getInstance().getDriverShortName(drPositioner->getDriverData()->getDriverName()));//QString::number(driverData->getNumber());

        p->setPen(namePen);

        int numX = labelX + (drvLabel.width() - p->fontMetrics().width(name))/2;
        int numY = labelY + (drvLabel.height() + p->fontMetrics().height())/2 - 2;
        p->drawText(numX, numY, name);
    }
}

}
