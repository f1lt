/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivertrackerpositioner.h"

#include "../../core/colorsmanager.h"
#include "../../core/eventdata.h"

#include <QStyleOptionGraphicsItem>

namespace DriverTrackerTool
{

DriverTrackerPositioner::DriverTrackerPositioner(QObject *parent, DriverData *dd, int speed) :
    DriverRadarPositioner(parent, dd, speed), coordinatesCount(0), mapX(0), mapY(0), trackCoordinates(NULL)
{

}



void DriverTrackerPositioner::setStartupPosition()
{
    trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();
    int laps = EventData::getInstance().getEventInfo().laps;
    lapped = false;
    prevLapped = false;
    qualiOut = false;
    inPits = false;

    racingState = RACING;

    syncData.reset(1);

    noSmoothSync = true;

    bool sessionFinished = EventData::getInstance().isSessionFinished();

    if (trackCoordinates)
        coordinatesCount = trackCoordinates->coordinates.size();

    //a very rough estimate ;)
    avgTime = 200 - 30 * log10(laps*laps);

    if (EventData::getInstance().getSessionRecords().getFastestLap().getTime().isValid())
        avgTime = EventData::getInstance().getSessionRecords().getFastestLap().getTime().toDouble();

    //if it's raining, we add 10 seconds
    if (EventData::getInstance().getWeather().getWetDry().getValue() == 1)
        avgTime += 10;

    //if SC is on track, we add 1 minute
    if (EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED)
        avgTime += 60;

    if (driverData && (driverData->isInPits() || driverData->isRetired() || sessionFinished))
    {
        if (sessionFinished)
            racingState = FINISHED;

        goToPit(true);
        calculatePitPosition();
    }

    else if (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT &&
        (!EventData::getInstance().isSessionStarted() || !driverData->getLastLap().getSectorTime(1).isValid()))
    {
        goToPit(false);

        if (trackCoordinates)
        {
            currentPositionIndex = trackCoordinates->indexes[TrackCoordinates::START] - (round((double)driverData->getPosition()/3.0))-1;

            if (currentPositionIndex < 0)
                currentPositionIndex = coordinatesCount + currentPositionIndex;

            currentLapTime = -(avgTime-(avgTime * currentPositionIndex) / coordinatesCount);
        }
    }
    else
    {
        currentLapTime = 0;
        for (int i = 0; i < 3; ++i)
            currentLapTime += driverData->getLastLap().getSectorTime(i+1).toDouble();

        calculatePosition();
    }
}


void DriverTrackerPositioner::calculatePosition()
{
    EventData &ev = EventData::getInstance();
    if (!ev.isSessionStarted())
        return;

    if (currentPositionIndex > 0)
    {
        if (driverData->getLastLap().getSectorTime(1).isValid() && ((currentSector == 1) || ((currentSector == 0) && noSmoothSync)) &&
            ((ev.getEventType() == LTPackets::RACE_EVENT && driverData->getColorData().lapTimeColor() == LTPackets::YELLOW) ||
            (!driverData->getLastLap().getSectorTime(2).isValid() || !driverData->getLastLap().getSectorTime(3).isValid())))
        {
            currentSector = 2;
            setSynchronizationPointOnTrackReached();
        }
        else if (driverData->getLastLap().getSectorTime(2).isValid() && ((currentSector == 2) || ((currentSector < 2) && noSmoothSync)) &&
                 !driverData->getLastLap().getSectorTime(3).isValid() &&
                 (driverData->getLastLap().getTime().toString() != "OUT"))
        {
            currentSector = 3;
            setSynchronizationPointOnTrackReached();
        }
        else
        {
            double step = ((double)coordinatesCount / avgTime) / (double)fps;

            if (syncData.syncCounter > 0)
                step /= 2;

            if (syncData.syncCounter < 0)
                step *= 2;

            currentPositionIndex += step; //(360 * currentLapTime) / avgTime;

            if ((int)(round(currentPositionIndex)) >= coordinatesCount)
            {
                currentPositionIndex = 0;
                ++missedFLCounter;

                if ((syncData.countingDirection == SynchronizationData::NONE) && !noSmoothSync)
                {
                    syncData.passedSyncPointOnMap = true;
                    syncData.countingDirection = SynchronizationData::UP;
                    syncData.synchronizing = true;
                }
            }

            if (!noSmoothSync)
                detectSynchronizationPointOnMapReached();
        }
        if (racingState == FINISH_IN_LAP)
            finishInLapPositionIndex += ((double)coordinatesCount / avgTime) / fps;
    }
    else
    {
        currentPositionIndex += ((double)coordinatesCount / avgTime) / fps;
    }
}

void DriverTrackerPositioner::calculatePitOutPosition()
{
    if (trackCoordinates)
        currentPositionIndex = trackCoordinates->indexes[2];
    syncData.reset(currentSector);
}

void DriverTrackerPositioner::setTrackPosition()
{
    if ((coordinatesCount > 0) && trackCoordinates)
    {
        if (isInPits())
        {
            x = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[0].x();
            y = -(mapY + mapH /2) + trackCoordinates->coordinates[0].y();
            return;
        }

        int idx = (int)floor(currentPositionIndex);//(int)round(currentDeg);
        int nextIdx = idx + 1;
        double factor = currentPositionIndex - (double)idx;

        if (idx >= coordinatesCount)
        {
            idx = 0;
            nextIdx = 1;
        }
        else if (nextIdx >= coordinatesCount)
            nextIdx = 0;

        double newX = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[idx].x();
        double newY = -(mapY + mapH /2) + trackCoordinates->coordinates[idx].y();

        double nextX = mapX -(mapX + mapW /2) + trackCoordinates->coordinates[nextIdx].x();
        double nextY = -(mapY + mapH /2) + trackCoordinates->coordinates[nextIdx].y();

        double resX = newX + factor * (nextX - newX);
        double resY = newY + factor * (nextY - newY);

        if ((resX != x) || (resY != y))
        {
            x = resX;
            y = resY;
        }
    }
}



}
