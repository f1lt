/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERTRACKER_H
#define DRIVERTRACKER_H

#include <QObject>
#include <QSettings>

#include "drivertrackertimer.h"
#include "drivertrackerwidget.h"
#include "driverradarwidget.h"

namespace DriverTrackerTool
{

class DriverTracker : public QObject
{
    Q_OBJECT
public:
    explicit DriverTracker(QObject *parent = 0);
    ~DriverTracker();

    void loadSettings(QSettings *settings);
    void saveSettings(QSettings *settings);

    bool isVisible();
    
signals:
    void timerStarted (int s, bool reduced);
    
public slots:

    void close();
    void sessionTimerUpdated();
    void startTimer(int s = 1000);
    void openDriverTracker();
    void openDriverRadar();

    void setup();

    void stopTimer()
    {
        driverTrackerTimer->stopTimer();
    }
    void pauseTimer(bool pause)
    {
        driverTrackerTimer->pauseTimer(pause);
    }

    void playingSeeked()
    {
        driverTrackerTimer->playingSeeked();
    }

    void onWidgetClose();

private:

    DriverTrackerTimer *driverTrackerTimer;
    DriverTrackerWidget *dtWidget;
    DriverRadarWidget *drWidget;
};

}
#endif // DRIVERTRACKER_H
