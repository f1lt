/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERTRACKERWIDGET_H
#define DRIVERTRACKERWIDGET_H

#include <QGraphicsView>
#include <QSettings>
#include <QTimer>
#include <QWheelEvent>
#include <QWidget>

#include "driverradarscene.h"
#include "drivertrackerscene.h"
#include "drivertrackertimer.h"

namespace Ui {
class DriverTrackerWidget;
}

namespace DriverTrackerTool
{

class DTGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    DTGraphicsView(QWidget *parent = 0) : QGraphicsView(parent){ }

protected:
    void wheelEvent(QWheelEvent *event)
    {
        if (event->modifiers() != Qt::ControlModifier)
        {
            QGraphicsView::wheelEvent(event);
        }
        else
        {
            emit viewScaled(event->delta()/60);
        }

    }

signals:
    void viewScaled(int);

};

class DriverTrackerWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit DriverTrackerWidget(DriverTrackerTimer *timer, QWidget *parent = 0);
    ~DriverTrackerWidget();

    void loadSettings(QSettings *);
    void saveSettings(QSettings *);


    void setup();

    void exec();
    void checkSetupCorrect();
    int getFPS() const;

public slots:

    void sessionTimerUpdated();

    void onRadarDriverSelected(int id);
    void onTrackerDriverSelected(int id);

    void resetView();


protected:
    void keyPressEvent(QKeyEvent *);
    void closeEvent(QCloseEvent *event);

signals:
    void timerStarted (int s, bool reduced);
    void closed();
    void FPSChanged(int);
    void driverSelected(int);
    void driverExcluded(int, bool);
    
private slots:
    void on_pushButton_clicked();
    void update();
    void viewScaled(int delta);

    void on_zoomSlider_valueChanged(int value);

    void on_rotationSlider_valueChanged(int value);

    void on_fpsSlider_valueChanged(int value);

    void on_resetButton_clicked();

    void on_followDriverBox_toggled(bool checked);

private:
    Ui::DriverTrackerWidget *ui;    

    DriverTrackerScene *driverTrackerScene;
    DriverTrackerItem *selectedDriver;
};

}
#endif // DRIVERTRACKERWIDGET_H
