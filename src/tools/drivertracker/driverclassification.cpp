/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverclassification.h"
#include "../../core/eventdata.h"

#include "../../core/colorsmanager.h"
#include "../../main_gui/models/qualimodel.h"

#include <QDebug>
#include <QStyleOptionGraphicsItem>
#include <QPainter>

namespace DriverTrackerTool
{

DriverClassification::DriverClassification(QGraphicsItem *parent) :
    QGraphicsObject(parent), selectedDriver(-1)
{
    labels[NORMAL] = QPixmap(":/ui_icons/label.png").scaledToHeight(20, Qt::SmoothTransformation);
    labels[SELECTED] = QPixmap(":/ui_icons/label-sel.png").scaledToHeight(20, Qt::SmoothTransformation);

    statusIcons[UP] = QPixmap(":/ui_icons/label-up.png").scaledToHeight(20, Qt::SmoothTransformation);
    statusIcons[DOWN] = QPixmap(":/ui_icons/label-down.png").scaledToHeight(20, Qt::SmoothTransformation);
    statusIcons[PIT] = QPixmap(":/ui_icons/label-pit.png").scaledToHeight(20, Qt::SmoothTransformation);
    statusIcons[FINISH] = QPixmap(":/ui_icons/label-finish.png").scaledToHeight(20, Qt::SmoothTransformation);
    statusIcons[NONE] = QPixmap();

//    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
//    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
}

QRectF DriverClassification::boundingRect() const
{
    int w = labels[0].width();
    int h = labels[0].height() * EventData::getInstance().getDriversData().size();

    return QRectF(-qreal(w/2), -qreal(h/2), qreal(w), qreal(h));
}

void DriverClassification::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    drivers.clear();
    for (int i = 0; i < EventData::getInstance().getDriversData().size(); ++i)
    {
        drivers.append(&EventData::getInstance().getDriversData()[i]);
    }
    qSort(drivers.begin(), drivers.end(), QualiLessThan(0, EventData::getInstance().getSessionRecords().getQualiBestTime(1).calc107p()));

    painter->setClipRect( option->exposedRect );
    QRectF bRect = boundingRect();

    QColor bgColor = ColorsManager::getInstance().getColor(LTPackets::BACKGROUND);
    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();
    if (trackCoordinates != 0)
        bgColor = trackCoordinates->bgColor;

    painter->setFont(QFont("Arial", 10, 100));
    for (int i = 0; i < drivers.size(); ++i)
    {
        DriverData *dd = drivers[i];//EventData::getInstance().getDriverDataByPosPtr(i+1);
        if (dd != 0)
        {
            QString number = QString::number(dd->getNumber());

            if (dd->getNumber() < 10)
                number = "  " + number;

            QString txt = SeasonData::getInstance().getDriverShortName(dd->getDriverName());

            QColor drvColor = ColorsManager::getInstance().getCarColor(dd->getNumber());
            painter->setBrush(drvColor);

            if (excludedDrivers.contains(dd->getCarID()))
                painter->setBrush(QColor(80, 80, 80));

            //draw label
            int labelX = bRect.x();
            int labelY = bRect.y() + i * 20;

            int drvX = labelX + 35;
            int drvY = labelY + painter->fontMetrics().height()/2 + 8;

            if (dd->getCarID() == selectedDriver)
            {
                painter->drawPixmap(labelX, labelY, labels[SELECTED]);
                painter->setPen(QColor(0,0,0));
            }
            else
            {
                painter->drawPixmap(labelX, labelY, labels[NORMAL]);
                painter->setPen(ColorsManager::getInstance().getDefaultColor(LTPackets::WHITE));
            }


            //draw driver name
            if (excludedDrivers.contains(dd->getCarID()))
                painter->setPen(QColor(80, 80, 80));

            painter->drawText(drvX, drvY, txt);

            //draw driver pos
            painter->setPen(ColorsManager::getInstance().getDefaultColor(LTPackets::WHITE));

            if (dd->isInPits())
                painter->setPen(ColorsManager::getInstance().getDefaultColor(LTPackets::PIT));
            QString pos = QString::number(i+1);

            EventData &ed = EventData::getInstance();
            bool qualiOut = (ed.getEventType() == LTPackets::QUALI_EVENT) && (ed.getQualiPeriod() > 1) && (dd->getPosition() > (10 + (3 - ed.getQualiPeriod()) * 6));

            if (!dd->isRetired() && !qualiOut)
            {
                if (i+1 < 10)
                    pos = "  " + pos;
                painter->drawText(labelX+10, drvY, pos);
            }

            //draw gap
            QString gap = "";
            if (i > 0)
            {
                if (ed.getEventType() == LTPackets::QUALI_EVENT)
                {
                    int quali = ed.getQualiPeriod();

                    LapTime bestTime = ed.getSessionRecords().getQualiBestTime(quali);

                    gap = DriverData::calculateGap(dd->getQualiTime(quali), bestTime);

                    if (gap != "")
                        gap = "+" + gap;
                }
                else
                {
                    gap = dd->getLastLap().getGap();
                    if (gap != "")
                        gap = "+" + gap;

                    if (!gap.contains("L") && gap.size() < 5)
                        gap = "  " + gap;
                }
            }
            else
            {
                if (ed.getEventType() == LTPackets::RACE_EVENT)
                {
                    int laps = ed.getCompletedLaps() + 1;
                    if (laps > ed.getEventInfo().laps)
                        laps = ed.getEventInfo().laps;
                    gap = QString("LAP %1").arg(laps);
                }

                else if (ed.getEventType() == LTPackets::PRACTICE_EVENT)
                    gap = dd->getLastLap().getTime().toString();

                else
                    gap = dd->getQualiTime(ed.getQualiPeriod()).toString();
            }

            gap = QString("%1").arg(gap, 10, ' ');
            painter->setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW)));

            if (excludedDrivers.contains(dd->getCarID()))
                painter->setPen(QColor(80, 80, 80));

            painter->drawText(labelX+70, drvY, gap);

            //draw status
            QPixmap &statusPixmap = getStatusPixmap(*dd);

            if (!statusPixmap.isNull())
            {
                painter->drawPixmap(labelX, labelY, statusPixmap);
            }
        }
    }
}

QPixmap &DriverClassification::getStatusPixmap(const DriverData &dd)
{
    if (dd.getNumber() > 0)
    {
        if (dd.isInPits())
            return statusIcons[2];

        EventData &eventData = EventData::getInstance();

        if (eventData.getEventType() != LTPackets::RACE_EVENT)
        {
            if ((eventData.isSessionFinished() || eventData.isQualiBreak()) && !dd.getLapData().isEmpty() &&
                dd.getLapData().last().getPracticeLapExtraData().getSessionTime().toString("h:mm:ss") == "0:00:00")
                return statusIcons[3];
        }
        else if (!dd.isRetired())
        {
            if (eventData.getCompletedLaps() == eventData.getEventInfo().laps)
            {
                QString gap = dd.getLastLap().getGap();
                int lapsBehindLeader = 0;
                if (gap.contains("L"))
                {
                    lapsBehindLeader = gap.left(gap.size()-1).toInt();
                }
                if ((dd.getLastLap().getLapNumber() + lapsBehindLeader) >= eventData.getCompletedLaps())
                    return statusIcons[3];
            }

            int lastPos = 0, prevPos = 0;
            if (dd.getLapData().size() > 1)
            {
                lastPos = dd.getLastLap().getPosition();
                prevPos = dd.getLapData()[dd.getLapData().size()-2].getPosition();
            }
            else if (!dd.getPositionHistory().isEmpty())
            {
                lastPos = dd.getLastLap().getPosition();
                prevPos = dd.getPositionHistory()[dd.getLastLap().getLapNumber() == 1 ? 0 : dd.getPositionHistory().size()-1];
            }
            if (lastPos != prevPos)
                return (lastPos < prevPos ? statusIcons[0] : statusIcons[1]);
        }
    }

    return statusIcons[4];
}

void DriverClassification::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        int y = event->pos().y();

        int position = (y - boundingRect().y()) / labels[0].height();

        if (position >= 0 && position < drivers.size())
        {
            if (event->modifiers() != Qt::ControlModifier)
            {
                if (selectedDriver == drivers[position]->getCarID())
                    selectedDriver = -1;

                else
                    selectedDriver = drivers[position]->getCarID();

                emit driverSelected(selectedDriver);
            }
            else
            {
                int idx = excludedDrivers.indexOf(drivers[position]->getCarID());

                bool exclude;
                if (idx != -1)
                {
                    excludedDrivers.takeAt(idx);
                    exclude = false;
                }

                else
                {
                    excludedDrivers.append(drivers[position]->getCarID());
                    exclude = true;
                }
                emit driverExcluded(drivers[position]->getCarID(), exclude);
            }

            update();
        }
    }
}

}
