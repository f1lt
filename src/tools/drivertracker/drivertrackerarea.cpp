/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "drivertrackerarea.h"
#include <QPainter>
#include <QDebug>

#include "../../core/eventdata.h"

#include <QGraphicsPixmapItem>

namespace DriverTrackerTool
{

DriverTrackerArea::DriverTrackerArea(DriverTrackerTimer *timer, QGraphicsItem *parent) :
    DriverRadarArea(timer, parent)
{
    scItem = new DriverTrackerSCItem(timer, this);
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    viewRotation = 0;
    viewScale = 1.0;

    trackLabels[S1] = QPixmap(":/ui_icons/track-label-s1.png").scaledToHeight(14, Qt::SmoothTransformation);
    trackLabels[S2] = QPixmap(":/ui_icons/track-label-s2.png").scaledToHeight(14, Qt::SmoothTransformation);
    trackLabels[DRS] = QPixmap(":/ui_icons/track-label-drs.png").scaledToHeight(22, Qt::SmoothTransformation);
}

QRectF DriverTrackerArea::boundingRect() const
{
    int w = trackMap.width();
    int h = trackMap.height();

    return QRectF(-w/2.0, -h/2.0, w, h);
}

void DriverTrackerArea::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!trackMap.isNull())
    {
        int w = trackMap.width();
        int h = trackMap.height();

        painter->setRenderHint(QPainter::SmoothPixmapTransform);
        painter->drawPixmap(- w/2, - h/2, trackMap);

        paintDRSZones(painter);
        paintTrackLabels(painter);
    }
}

void DriverTrackerArea::paintDRSZones(QPainter *painter)
{
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::SmoothPixmapTransform, true);

    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

    if (trackCoordinates != 0)
    {
        for (int i = 0; i < trackCoordinates->drsZones.size(); ++i)
        {
            int startIdx = trackCoordinates->drsZones[i].startIdx;
            int endIdx = trackCoordinates->drsZones[i].endIdx;

            QPen pen(QColor(255, 0, 255, 150));

            int mapX = scenePos().x();
            int mapY = scenePos().y();
            int mapW = trackMap.width();
            int mapH = trackMap.height();

            int penWidth = 0;

            if (startIdx < endIdx)
            {
                QVector<QPoint> points;
                for (int j = startIdx; j < endIdx; j++)
                {
                    if (j == startIdx)
                    {
                        int x1 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j].x();
                        int y1 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j].y();

                        points.append(QPoint(x1, y1));
                    }

                    int x2 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j+1].x();
                    int y2 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j+1].y();
                    points.append(QPoint(x2, y2));

                    QPoint p1 = points[j - startIdx];
                    QPoint p2 = points[j+1 - startIdx];

                    penWidth += sqrt(pow(p2.x() - p1.x(), 2) + pow(p2.y() - p1.y(), 2));
                }
                pen.setWidth(penWidth / (endIdx - startIdx));
                painter->setPen(pen);
                painter->drawPolyline(points.data(), points.size());
            }
            else
            {
                QVector<QPoint> points;
                int n = 0;
                for (int j = startIdx; j < trackCoordinates->coordinates.size()-1; j++)
                {
                    if (j == startIdx)
                    {
                        int x1 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j].x();
                        int y1 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j].y();

                        points.append(QPoint(x1, y1));
                    }

                    int x2 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j+1].x();
                    int y2 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j+1].y();
                    points.append(QPoint(x2, y2));

                    QPoint p1 = points[j - startIdx];
                    QPoint p2 = points[j+1 - startIdx];

                    ++n;
                    penWidth += sqrt(pow(p2.x() - p1.x(), 2) + pow(p2.y() - p1.y(), 2));
                }
                for (int j = 0; j < endIdx; j++)
                {
                    if (j == 0)
                    {
                        int x1 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j].x();
                        int y1 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j].y();

                        points.append(QPoint(x1, y1));
                    }

                    int x2 = mapX - (mapX + mapW / 2) + trackCoordinates->coordinates[j+1].x();
                    int y2 = -(mapY + mapH / 2) + trackCoordinates->coordinates[j+1].y();
                    points.append(QPoint(x2, y2));

                    QPoint p1 = points[j];
                    QPoint p2 = points[j+1];

                    ++n;
                    penWidth += sqrt(pow(p2.x() - p1.x(), 2) + pow(p2.y() - p1.y(), 2));
                }
                pen.setWidth(penWidth / n);
                painter->setPen(pen);
                painter->drawPolyline(points.data(), points.size());
            }
        }
    }
}

void DriverTrackerArea::paintTrackLabels(QPainter *painter)
{
    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

    if (trackCoordinates != 0)
    {
        for (int i = 0; i < trackCoordinates->labels.size(); ++i)
        {
            int mapX = 0;
            int mapY = 0;
            int mapW = trackMap.width();
            int mapH = trackMap.height();

            int x = mapX -(mapX + mapW /2) + trackCoordinates->labels[i].coordinates.x();
            int y = -(mapY + mapH /2) + trackCoordinates->labels[i].coordinates.y();

            QPixmap label;
            switch (trackCoordinates->labels[i].type)
            {
                case TrackCoordinates::Label::S1: label = trackLabels[S1]; break;
                case TrackCoordinates::Label::S2: label = trackLabels[S2]; break;
                case TrackCoordinates::Label::DRS: label = trackLabels[DRS]; break;
                default: break;
            }

            painter->save();
            painter->translate(x, y);
            painter->rotate(-viewRotation);

            if (viewScale > 0.0 && viewScale < 1.0)
                painter->scale(1.0/viewScale, 1.0/viewScale);

            painter->drawPixmap(-label.width()/2, -label.height()/2, label);
            painter->restore();
        }
    }
}

void DriverTrackerArea::setup()
{
    trackMap = SeasonData::getInstance().getTrackDataManager().getCurrentTrackMap();//EventData::getInstance().getEventInfo().trackImg;    

    if (selectedDriver != -2)
        selectedDriver = -1;

    for (int i = 0; i < drp.size(); ++i)
    {
        static_cast<DriverTrackerItem*>(drp[i])->setMapCoords(scenePos().x(), scenePos().y(), trackMap.width(), trackMap.height());        
        drp[i]->setStartupPosition();        
        drp[i]->setExcluded(false);
        scItem->setVisible(false);
    }

    update(boundingRect());
}

void DriverTrackerArea::checkSetupCorrect()
{
    trackMap = SeasonData::getInstance().getTrackDataManager().getCurrentTrackMap();//EventData::getInstance().getEventInfo().trackImg;

    if (drp.isEmpty() || (selectedDriver == -2))
    {
        setup();
    }
    else
    {
        for (int i = 0; i < drp.size(); ++i)
        {
            drp[i]->updateScaleAndRotation();
        }

        if (scItem->isVisible())
            scItem->updateScaleAndRotation();
    }
}

void DriverTrackerArea::loadDriversList()
{
    selectedDriver = -1;
    for (int i = 0; i < drp.size(); ++i)
        delete drp[i];

    drp.resize(EventData::getInstance().getDriversData().size());

    //drivers are added in reverse order, so that the leaders will be always on top
    for (int i = drp.size()-1; i >= 0; --i)
    {
        drp[i] = new DriverTrackerItem(dtTimer->getDriverTrackerPositioner(i), this);//, &EventData::getInstance().getDriversData()[i]);
        drp[i]->setFlags();
        connect (drp[i], SIGNAL(driverSelected(DriverTrackerItem*)), this, SLOT(onDTPDriverSelected(DriverTrackerItem*)), Qt::UniqueConnection);
    }
}

void DriverTrackerArea::updatePosition()
{
    DriverRadarArea::updatePosition();

    if (scItem->isVisible())
        scItem->updatePosition();

    if (!scItem->isVisible() && (EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED))
    {
        scItem->setVisible(true);
        scItem->update();
    }
    else if (scItem->isVisible() && (EventData::getInstance().getFlagStatus() != LTPackets::SAFETY_CAR_DEPLOYED))
    {
        scItem->setVisible(false);
        scItem->update();
    }
}


DriverTrackerItem *DriverTrackerArea::getSelectedDriver()
{
    if (selectedDriver == -1)
        return NULL;

    for (int i = 0; i < drp.size(); ++i)
    {
        if (drp[i]->getDriverId() == selectedDriver)
            return static_cast<DriverTrackerItem*>(drp[i]);
    }

    return NULL;
}

void DriverTrackerArea::onDTPDriverSelected(DriverTrackerItem *driver)
{
    if (driver)
    {
        /* A single press can be detected by several drivers (if they are close to each other), but only one can be selected
         * at a time. So all others have to be deselected then
         */
        for (int i = 0; i < drp.size(); ++i)
        {
            if (drp[i] != driver)
            {
                drp[i]->setSelected(false);
            }
        }


        selectedDriver = driver->getDriverId();
    }
    else
    {
        selectedDriver = -1;
    }
    emit driverSelected(selectedDriver);
}

void DriverTrackerArea::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    /* This function checks whether a mouse button has been pressed somewhere on the track, but not on any driver.
     * If yes - all drivers have to be deselected and appropriate signal has to be emitted to inform driverTrackerWidget that
     * there is no driver to follow
     */
    if (event->button() == Qt::LeftButton)
    {
        bool driverFound = false;
        for (int i = 0; i < drp.size(); ++i)
        {
            if (!drp[i]->contains(event->pos()))
                drp[i]->setSelected(false);
            else
                driverFound = true;
        }

        if (!driverFound)
            emit driverSelected(-1);
    }
}




}
