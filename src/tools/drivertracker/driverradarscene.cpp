/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include <QMouseEvent>
#include <QGraphicsView>

#include "driverradarscene.h"
#include <cmath>

#include "../../core/colorsmanager.h"

namespace DriverTrackerTool
{

DriverRadarScene::DriverRadarScene(DriverTrackerTimer *timer, QObject *parent) :
    QGraphicsScene(parent), selectedDriver(-2), dtTimer(timer)
{        
    dtArea = NULL;
    dtInfo = NULL;

    loadDriversList();

    setItemIndexMethod(NoIndex);
}

DriverRadarScene::~DriverRadarScene()
{
    for (int i = 0; i < drp.size(); ++i)
        delete drp[i];

    if (dtArea != NULL)
        delete dtArea;

    if (dtInfo != NULL)
        delete dtInfo;
}

void DriverRadarScene::setupDrivers()
{        
    if (!items().contains(dtArea))
        addItem(dtArea);

    if (!items().contains(dtInfo))
        addItem(dtInfo);

    updateSize();

    excludedDrivers.clear();

    dtArea->setPos(0, 0);
    dtInfo->setPos(0, (dtArea->boundingRect().height() + dtInfo->boundingRect().height())/2 + 20);
    dtArea->setup();
}

void DriverRadarScene::updateSize()
{
    if (!views().isEmpty())
    {
        QGraphicsView* view = views().first();

        //minimum size should be 50
        int size = qMax(50, qMin(view->size().width(), view->size().height()));
        dtArea->updateSize(size);
        dtInfo->setPos(0, (dtArea->boundingRect().height() + dtInfo->boundingRect().height())/2 + 20);

        QRectF rect = dtArea->boundingRect();

        rect.setHeight(rect.height() + dtInfo->boundingRect().height() + 20);

        rect.setWidth(rect.width() + 20);
        if (dtInfo->boundingRect().width() > rect.width())
            rect.setWidth(dtInfo->boundingRect().width() + 20);

        setSceneRect(rect);
    }
}

void DriverRadarScene::loadDriversList()
{
    if (dtArea == NULL)    
        dtArea = new DriverRadarArea(dtTimer, 0);


    if (dtInfo == NULL)
        dtInfo = new DriverTrackerInfo(0);

    connect (dtArea, SIGNAL(driverSelected(int)), this, SIGNAL(driverSelected(int)), Qt::UniqueConnection);
    connect (dtArea, SIGNAL(driverSelected(int)), dtInfo, SLOT(setDriver(int)));

    dtArea->loadDriversList();
    dtInfo->setDriverData(NULL);
}



bool DriverRadarScene::isExcluded(int id)
{
    for (int i = 0; i < excludedDrivers.size(); ++i)
    {
        if (excludedDrivers[i] == id)
            return true;
    }

    return false;
}

}
