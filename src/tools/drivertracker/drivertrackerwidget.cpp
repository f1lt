/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivertrackerwidget.h"
#include "ui_drivertrackerwidget.h"

#include <QDebug>
#include <QKeyEvent>
//#include <QGLWidget>

namespace DriverTrackerTool
{

DriverTrackerWidget::DriverTrackerWidget(DriverTrackerTimer *timer, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DriverTrackerWidget)
{
    ui->setupUi(this);

//    QGLWidget *glWidget = new QGLWidget(this);

    selectedDriver = 0;


    driverTrackerScene = new DriverTrackerScene(timer, this);

    ui->driverTrackerView->setScene(driverTrackerScene);

    ui->driverTrackerView->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);

    ui->driverTrackerView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing | QGraphicsView::DontSavePainterState);

    ui->driverTrackerView->setCacheMode(QGraphicsView::CacheBackground);

    connect (timer, SIGNAL(positionsUpdated()), this, SLOT(update()));
    connect (driverTrackerScene, SIGNAL(driverSelected(int)), this, SLOT(onTrackerDriverSelected(int)));

    connect (driverTrackerScene, SIGNAL(driverExcluded(int,bool)), this, SIGNAL(driverExcluded(int,bool)));
    connect (ui->driverTrackerView, SIGNAL(viewScaled(int)), this, SLOT(viewScaled(int)));
}

DriverTrackerWidget::~DriverTrackerWidget()
{
    delete ui;
}

void DriverTrackerWidget::update()
{
    driverTrackerScene->updatePosition();

    if (selectedDriver && ui->followDriverBox->isChecked())
        ui->driverTrackerView->centerOn(selectedDriver);
}

void DriverTrackerWidget::viewScaled(int delta)
{
    int max = ui->zoomSlider->maximum();
    int min = ui->zoomSlider->minimum();

    double zoom = ui->zoomValue->text().toDouble() * 10.0 + delta;
    if (zoom > max)
        zoom = max;

    if (zoom < min)
        zoom = min;

    ui->zoomSlider->setValue(zoom);
}

void DriverTrackerWidget::setup()
{    
    selectedDriver = 0;
    driverTrackerScene->loadDriversList();

    driverTrackerScene->setupDrivers();

    resetView();
    setWindowTitle("Driver tracker: " + EventData::getInstance().getEventInfo().eventName);    
}

void DriverTrackerWidget::loadSettings(QSettings *settings)
{
    restoreGeometry(settings->value("ui/driver_tracker_geometry").toByteArray());

    ui->fpsSlider->setValue(settings->value("ui/tracker_fps", 25).toInt());
    ui->followDriverBox->setChecked(settings->value("ui/follow_selected_driver", true).toBool());
}

void DriverTrackerWidget::saveSettings(QSettings *settings)
{
    settings->setValue("ui/driver_tracker_geometry", saveGeometry());
    settings->setValue("ui/tracker_fps", ui->fpsSlider->value());
    settings->setValue("ui/follow_selected_driver", ui->followDriverBox->isChecked());
}



void DriverTrackerWidget::on_pushButton_clicked()
{   
    close();
}

void DriverTrackerWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        close();
}

void DriverTrackerWidget::closeEvent(QCloseEvent *event)
{
    emit closed();
    QWidget::closeEvent(event);
}

void DriverTrackerWidget::exec()
{
    setWindowTitle("Driver tracker: " + EventData::getInstance().getEventInfo().eventName);

    //set speed back to default
//    int fps = ui->fpsValue->text().toInt();

    driverTrackerScene->setTrackPosition();

    show();

    driverTrackerScene->checkSetupCorrect();
}

void DriverTrackerWidget::checkSetupCorrect()
{
    driverTrackerScene->checkSetupCorrect();
}

int DriverTrackerWidget::getFPS() const
{
    return ui->fpsValue->text().toInt();
}

void DriverTrackerWidget::sessionTimerUpdated()
{
    //this slot will be called by session timer to update for example classification
    driverTrackerScene->sessionTimerUpdated();
}

void DriverTrackerWidget::onRadarDriverSelected(int id)
{
    driverTrackerScene->selectDriver(id);

    if (id != -1)
    {
        selectedDriver = driverTrackerScene->getSelectedDriver();

        if (selectedDriver && selectedDriver->isVisible() && ui->followDriverBox->isChecked())
            ui->driverTrackerView->centerOn(selectedDriver);
    }
    else
        selectedDriver = 0;
}

void DriverTrackerWidget::onTrackerDriverSelected(int id)
{
    if (id != -1)
    {
        selectedDriver = driverTrackerScene->getSelectedDriver();

        if (selectedDriver && ui->followDriverBox->isChecked())
            ui->driverTrackerView->centerOn(selectedDriver);
    }
    else
        selectedDriver = 0;

    emit driverSelected(id);
}

void DriverTrackerWidget::resetView()
{
    driverTrackerScene->resetView();
    ui->zoomValue->setText("1.0");
    ui->zoomSlider->setValue(10);

    ui->rotationValue->setText("0");
    ui->rotationSlider->setValue(0);

    const TrackCoordinates *trackCoordinates = SeasonData::getInstance().getTrackMapsCoordinates().getCurrentTrackCoordinates();

    if (trackCoordinates != 0)
    {
        QPalette bg = ui->driverTrackerView->palette();
        bg.setColor(QPalette::Base, trackCoordinates->bgColor);
        ui->driverTrackerView->setPalette(bg);
    }
}

}

void DriverTrackerTool::DriverTrackerWidget::on_zoomSlider_valueChanged(int value)
{
    double currentScale = ui->zoomValue->text().toDouble();
    double newScale = double(value / 10.0);

    driverTrackerScene->scale(newScale/currentScale, newScale);

    ui->zoomValue->setText(QString::number(newScale, 'f', 1));

    //reset timer speed
//    driverTrackerTimer->setFPS(ui->fpsSlider->value());
}

void DriverTrackerTool::DriverTrackerWidget::on_rotationSlider_valueChanged(int value)
{
    int currentRotation = ui->rotationValue->text().toInt();
    int newRotation = value;

    qreal angle = newRotation - currentRotation;

    driverTrackerScene->rotate(angle, value);

    ui->rotationValue->setText(QString::number(newRotation));

    //reset timer speed
//    driverTrackerTimer->setFPS(ui->fpsSlider->value());
}

void DriverTrackerTool::DriverTrackerWidget::on_fpsSlider_valueChanged(int value)
{
    ui->fpsValue->setText(QString::number(value));
    int fps = value;

    //reset timer speed
    emit FPSChanged(fps);
}

void DriverTrackerTool::DriverTrackerWidget::on_resetButton_clicked()
{
    resetView();
}

void DriverTrackerTool::DriverTrackerWidget::on_followDriverBox_toggled(bool checked)
{
    if (checked && selectedDriver)
        ui->driverTrackerView->centerOn(selectedDriver);
}
