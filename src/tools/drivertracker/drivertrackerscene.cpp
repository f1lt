/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "drivertrackerscene.h"
#include "drivertrackerpositioner.h"

#include <QMouseEvent>

#include "../../core/colorsmanager.h"
#include "../../main_gui/models/qualimodel.h"

namespace DriverTrackerTool
{

DriverTrackerScene::DriverTrackerScene(DriverTrackerTimer *timer, QObject *parent) :
    DriverRadarScene(timer, parent)
{
    dtArea = NULL;
    driverClassificaction = NULL;

    setItemIndexMethod(QGraphicsScene::NoIndex);
}

DriverTrackerScene::~DriverTrackerScene()
{
//    if (dtArea == NULL)
//        delete dtArea;

    if (driverClassificaction == NULL)
        delete driverClassificaction;
}


void DriverTrackerScene::setupDrivers()
{
    dtArea->setup();    

    if (!items().contains(dtArea))
        addItem(dtArea);


    if (!items().contains(driverClassificaction))
        addItem(driverClassificaction);

    excludedDrivers.clear();

    driverClassificaction->setPos(sceneRect().x()+20, 0);

    setTrackPosition();
}



void DriverTrackerScene::loadDriversList()
{
    if (dtArea == NULL)
        dtArea = new DriverTrackerArea(dtTimer, 0);

    if (driverClassificaction == NULL)
        driverClassificaction = new DriverClassification(0);

    connect (dtArea, SIGNAL(driverSelected(int)), this, SIGNAL(driverSelected(int)), Qt::UniqueConnection);
    connect (dtArea, SIGNAL(driverSelected(int)), driverClassificaction, SLOT(selectDriver(int)), Qt::UniqueConnection);

    connect (driverClassificaction, SIGNAL(driverSelected(int)), dtArea, SLOT(selectDriver(int)));
    connect (driverClassificaction, SIGNAL(driverSelected(int)), this, SIGNAL(driverSelected(int)));

    connect (driverClassificaction, SIGNAL(driverExcluded(int,bool)), this, SIGNAL(driverExcluded(int,bool)));
    connect (driverClassificaction, SIGNAL(driverExcluded(int,bool)), dtArea, SLOT(excludeDriver(int,bool)));

    dtArea->loadDriversList();
    driverClassificaction->selectDriver(-1);
}


void DriverTrackerScene::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        emit viewScaled(event->delta()/60);
    }
}

}
