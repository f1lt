/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERCLASSIFICATION_H
#define DRIVERCLASSIFICATION_H

#include <QGraphicsObject>
#include "../../core/eventdata.h"
#include <QGraphicsSceneMouseEvent>


namespace DriverTrackerTool
{

class DriverClassification : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DriverClassification(QGraphicsItem *parent = 0);

    QRectF boundingRect () const;
    void paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    QPixmap &getStatusPixmap(const DriverData &dd);

    enum LabelType
    {
        NORMAL = 0, SELECTED
    };
    enum StatusLabelType
    {
        UP, DOWN, PIT, FINISH, NONE
    };
    
signals:
    void driverSelected(int id);
    void driverExcluded(int id, bool excl);
    
public slots:
    void selectDriver(int id)
    {
        selectedDriver = id;
        update();
    }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    QPixmap statusIcons[5];
    QPixmap labels[2];
    
    int selectedDriver;
    QList<DriverData *> drivers;
    QList<int> excludedDrivers;
};

}

#endif // DRIVERCLASSIFICATION_H
