/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#ifndef DRIVERTRACKERSCENE_H
#define DRIVERTRACKERSCENE_H

#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsSceneWheelEvent>
#include <QPixmap>

#include "driverradarscene.h"
#include "drivertrackerarea.h"
#include "driverclassification.h"

namespace DriverTrackerTool
{

class DriverTrackerScene : public DriverRadarScene
{
    Q_OBJECT

public:
    DriverTrackerScene(DriverTrackerTimer *timer, QObject *parent = 0);
    virtual ~DriverTrackerScene();

    virtual void loadDriversList();
    virtual void setupDrivers();

    virtual void checkSetupCorrect()
    {
        dtArea->checkSetupCorrect();
        static_cast<DriverTrackerArea*>(dtArea)->updateAll();
        setTrackPosition();
        setSceneRect(sceneBoundingRect());
    }

    void rotate(int currentAngle, int totalAngle)
    {
        DriverRadarScene::rotate(currentAngle, totalAngle);

        dtArea->rotate(currentAngle);
        setTrackPosition();
        setSceneRect(sceneBoundingRect());
    }

    void scale(double currentScale, double totalScale)
    {
        DriverRadarScene::scale(currentScale, totalScale);

        setTrackPosition();
        setSceneRect(sceneBoundingRect());
    }

    void setTrackPosition()
    {
        dtArea->setPos(driverClassificaction->x() + (driverClassificaction->boundingRect().width() + dtArea->boundingRect().width())/2 + 50, 0);
        int x = dtArea->boundingRect().x();

        if (x <= (driverClassificaction->boundingRect().x() + driverClassificaction->boundingRect().width()/2))
            dtArea->setPos(driverClassificaction->x() + (driverClassificaction->boundingRect().width() + dtArea->sceneBoundingRect().width())/2 + 50, 0);
    }

    //returns bounding rect of all scene items
    QRectF sceneBoundingRect()
    {
        QRectF rect = driverClassificaction->sceneBoundingRect();

        rect.setWidth(rect.width() + 50 + dtArea->sceneBoundingRect().width());

        qreal h = driverClassificaction->sceneBoundingRect().height();
        qreal y = driverClassificaction->sceneBoundingRect().y();

        if (h < dtArea->sceneBoundingRect().height())
        {
            h = dtArea->sceneBoundingRect().height();
            y = dtArea->sceneBoundingRect().y();
        }
        rect.setY(y);
        rect.setHeight(h);

        return rect;
    }

    DriverTrackerItem *getSelectedDriver()
    {
        return static_cast<DriverTrackerArea*>(dtArea)->getSelectedDriver();
    }

    void resetView()
    {
        dtArea->resetMatrix();
        driverClassificaction->resetMatrix();

        setSceneRect(sceneBoundingRect());
    }

public slots:
    virtual void selectDriver(int id)
    {
        dtArea->selectDriver(id);
        driverClassificaction->selectDriver(id);
    }

    virtual void sessionTimerUpdated()
    {
        driverClassificaction->update();
    }

signals:

    void driverSelected(DriverTrackerItem*);
    void driverExcluded(int, bool);
    void viewScaled(int);

protected:

    void wheelEvent(QGraphicsSceneWheelEvent *event);

    DriverClassification *driverClassificaction;
};

}
#endif // DRIVERTRACKERSCENE_H
