/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include <QPainter>
#include "driverradararea.h"
#include "../../core/eventdata.h"
#include "../../core/colorsmanager.h"

namespace DriverTrackerTool
{

DriverRadarArea::DriverRadarArea(DriverTrackerTimer *timer, QGraphicsItem *parent) :
    QGraphicsObject(parent), dtTimer(timer), selectedDriver(-2)
{
    radarX = 0;
    radarY = 0;
    radarR = 50;
    radarLappedR = radarR * 0.75;
    radarPitR = radarR * 0.5;
    scOnTrack = false;

    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}

QRectF DriverRadarArea::boundingRect() const
{
    qreal w = (radarR + 40) * 2;
    qreal h = w;

    return QRectF(-w/2.0, -h/2.0, w, h);
}

void DriverRadarArea::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    qDebug() << "REPAINT";
    painter->setRenderHint(QPainter::Antialiasing);

    QPen pen(QColor(255, 255, 255), 2);

    DriverRadarPositioner *drPositioner = dtTimer->getDriverRadarPositioner(0);
    if (drPositioner)
    {
        double s1Pos = drPositioner->sectorIndex(0);
        double s2Pos = drPositioner->sectorIndex(1);
        double alpha, x1, x2, y1, y2;

        QColor color = ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW);
        color.setAlpha(30);
        painter->setPen(color);
        painter->setBrush(color);

        int startAngle = 90 * 16;
        int spanAngle = (-s1Pos + 1) * 16;

        painter->drawPie(radarX - radarR, radarY  - radarR, radarR*2, radarR*2, startAngle, spanAngle);


        painter->setFont(QFont("Arial", 16, 75, true));

        alpha = s1Pos / 360.0 * M_PI;
        QString text = "S1";
        x1 = radarX + sin(alpha) * (radarR - (radarR - radarLappedR + painter->fontMetrics().width(text)) / 2);
        y1 = radarY - cos(alpha) * (radarR - (radarR - radarLappedR + painter->fontMetrics().height()) / 2);

        painter->setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW)));
        painter->drawText(x1, y1, text);


        alpha = (s1Pos + (s2Pos - s1Pos) / 2.0) / 180.0 * M_PI;
        text = "S2";
        x1 = radarX + sin(alpha) * (radarR - (radarR - radarLappedR) / 2);
        y1 = radarY - cos(alpha) * (radarR - (radarR - radarLappedR - painter->fontMetrics().height()) / 2);

        color = ColorsManager::getInstance().getDefaultColor(LTPackets::GREEN);
        color.setAlpha(30);
        painter->setPen(color);
        painter->setBrush(color);

        startAngle = (90-s1Pos) * 16;
        spanAngle = (-s2Pos + s1Pos + 1) * 16;

        painter->drawPie(radarX - radarR, radarY  - radarR, radarR*2, radarR*2, startAngle, spanAngle);

        painter->setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::GREEN)));
        painter->drawText(x1, y1, text);

        alpha = (s2Pos + (360.0 - s2Pos) / 2.0) / 180.0 * M_PI;
        text = "S3";
        x1 = radarX + sin(alpha) * (radarR - (radarR - radarLappedR - painter->fontMetrics().width(text)) / 2);
        y1 = radarY - cos(alpha) * (radarR - (radarR - radarLappedR + painter->fontMetrics().height()) / 2);

        color = ColorsManager::getInstance().getDefaultColor(LTPackets::VIOLET);
        color.setAlpha(30);
        painter->setPen(color);
        painter->setBrush(color);

        startAngle = (90-s2Pos) * 16;
        spanAngle = (-360 + s2Pos + 1) * 16;

        painter->drawPie(radarX - radarR, radarY  - radarR, radarR*2, radarR*2, startAngle, spanAngle);

        painter->setPen(QColor(ColorsManager::getInstance().getDefaultColor(LTPackets::VIOLET)));
        painter->drawText(x1, y1, text);

        //s1 line

        if ((EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED) &&
            (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT))
            pen.setColor(ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW));

        painter->setPen(pen);


        alpha = s1Pos / 180.0 * M_PI;
        x1 = radarX + sin(alpha) * (radarR);
        x2 = radarX + sin(alpha) * (radarLappedR);
        y1 = radarY - cos(alpha) * (radarR);
        y2 = radarY - cos(alpha) * (radarLappedR);

        painter->drawLine(x1, y1, x2, y2);

        //s2 line

        alpha = s2Pos / 360.0 * 2.0 * M_PI;
        x1 = radarX + sin(alpha) * (radarR);
        x2 = radarX + sin(alpha) * (radarLappedR);
        y1 = radarY - cos(alpha) * (radarR);
        y2 = radarY - cos(alpha) * (radarLappedR);
        painter->drawLine(x1, y1, x2, y2);
    }

    pen = QPen(QColor(255, 255, 255), 5);

    if ((EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED) &&
        (EventData::getInstance().getEventType() == LTPackets::RACE_EVENT))
        pen.setColor(ColorsManager::getInstance().getDefaultColor(LTPackets::YELLOW));

    painter->setBrush(QBrush());
    painter->setPen(pen);
    painter->drawEllipse(QPoint(radarX, radarY), (int)radarR, (int)radarR);

    pen.setWidth(3);
    painter->setPen(pen);
    painter->setBrush(QBrush(ColorsManager::getInstance().getDefaultColor(LTPackets::BACKGROUND)));
    painter->drawEllipse(QPoint(radarX, radarY), (int)radarLappedR, (int)radarLappedR);

    pen.setWidth(5);
    painter->setPen(pen);
    painter->setBrush(QBrush());

    int x = radarX - radarPitR;
    int y = radarY - radarPitR;
    int w = radarX + radarPitR - x;
    int h = radarY + radarPitR - y;
    painter->drawArc(x, y, w, h, 270*16, 180*16);

    //paint finish line
    pen.setWidth(3);
    painter->setPen(pen);
    painter->drawLine(radarX, radarY - radarR - 10, radarX, radarY - radarLappedR + 10);
}

void DriverRadarArea::checkSetupCorrect()
{
    if (drp.isEmpty() || (selectedDriver == -2))
    {
        setup();
    }
}

void DriverRadarArea::loadDriversList()
{
    selectedDriver = -1;
    scOnTrack = false;
    for (int i = 0; i < drp.size(); ++i)
        delete drp[i];

    drp.resize(EventData::getInstance().getDriversData().size());

    for (int i = drp.size()-1; i >= 0; --i)
    {
        drp[i] = new DriverRadarItem(dtTimer->getDriverRadarPositioner(i), this);//, &EventData::getInstance().getDriversData()[i]);
        drp[i]->setFlags();
        connect (drp[i], SIGNAL(driverSelected(DriverRadarItem*)), this, SLOT(onDRDriverSelected(DriverRadarItem*)), Qt::UniqueConnection);
    }
}

void DriverRadarArea::updateSize(int width)
{
    radarR = width / 2 - 40;
    radarLappedR = radarR * 0.75;
    radarPitR = radarR * 0.5;

    update();

    for (int i = 0; i < drp.size(); ++i)
        drp[i]->setRadarCoords(radarX, radarY, radarR, radarPitR, radarLappedR);
}


void DriverRadarArea::setup()
{
    if (selectedDriver != -2)
        selectedDriver = -1;

    radarX = x();
    radarY = y();

    for (int i = 0; i < drp.size(); ++i)
    {
        drp[i]->setRadarCoords(radarX, radarY, radarR, radarPitR, radarLappedR);
        drp[i]->setStartupPosition();
        drp[i]->setExcluded(false);        
    }    
}

void DriverRadarArea::updatePosition()
{
    if (selectedDriver == -2)
        selectedDriver = -1;

    for (int i = 0; i < drp.size(); ++i)
    {
        drp[i]->updatePosition();
    }

    bool currentSCStatus = (EventData::getInstance().getFlagStatus() == LTPackets::SAFETY_CAR_DEPLOYED);
    if (scOnTrack != currentSCStatus)    
        update();

    scOnTrack = currentSCStatus;
}

void DriverRadarArea::selectDriver(int id)
{
    selectedDriver = id;
    for (int i = 0; i < drp.size(); ++i)
    {
        drp[i]->setSelected(false);

        if (id == drp[i]->getDriverId())
        {
            selectedDriver = id;
            drp[i]->setSelected(true);
        }
    }
}

void DriverRadarArea::excludeDriver(int id, bool exclude)
{
    for (int i = 0; i < drp.size(); ++i)
    {
        if (drp[i]->getDriverId() == id)
        {
            drp[i]->setExcluded(exclude);
        }
    }
}

void DriverRadarArea::onDRDriverSelected(DriverRadarItem *driver)
{
    if (driver)
    {
        /* A single press can be detected by several drivers (if they are close to each other), but only one can be selected
         * at a time. So all others have to be deselected then
         */
        for (int i = 0; i < drp.size(); ++i)
        {
            if (drp[i] != driver)
            {
                drp[i]->setSelected(false);
            }
        }
        selectedDriver = driver->getDriverId();
        emit driverSelected(driver->getDriverId());
    }
    else
    {
        selectedDriver = -1;
        emit driverSelected(-1);
    }
}

}
