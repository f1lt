/*******************************************************************************
 *                                                                             *
 *   F1LT - unofficial Formula 1 live timing application                       *
 *   Copyright (C) 2012-2013  MP                                               *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERTRACKERTIMER_H
#define DRIVERTRACKERTIMER_H

#include <QObject>
#include <QThread>
#include "core/timer.h"

#include "drivertrackerpositioner.h"

namespace DriverTrackerTool
{



class DriverTrackerTimer : public QObject
{
    Q_OBJECT
public:
    explicit DriverTrackerTimer(QObject *parent = 0);
//    ~DriverTrackerTimer();
    
signals:
    void positionsUpdated();
    
public slots:
    void timeout();

    void startTimer(int s=1000, bool reducedSpeed = false);
    void loadDriversList();

    void pauseTimer(bool pause)
    {
        if (pause)
            timer->stop();

        else
        {
            double value = double(sessionInterval/fps);

            if (visible)
                timer->start(round(value));
            else
                timer->start(sessionInterval);

            intervalData.resetData();
        }
    }

    void stopTimer()
    {
        timer->stop();
    }

    void setFPS(int newFPS, bool updatePositionersOnly = false)
    {
        if (!updatePositionersOnly)
        {
            fps = newFPS;
            setTimerInterval(sessionInterval);
            intervalData.resetData();
        }

        int dFPS = newFPS;// > 2 ? fps - 2 : fps;
        for (int i = 0; i < drp.size(); ++i)
        {
            drp[i]->setFPS(dFPS);
            dtp[i]->setFPS(dFPS);
        }        
    }

    void setTimerInterval(int s=1000, bool reducedSpeed = false)
    {
        sessionInterval = s;
        double value = double(sessionInterval/fps);

        if (!reducedSpeed)
            timer->setInterval(round(value));
        else
            timer->setInterval(sessionInterval);

        setFPS(fps, true);
    }

    int positionersCount() const
    {
        return drp.size();
    }

    DriverRadarPositioner* getDriverRadarPositioner(int i)
    {
        if (i >= 0 && i < drp.size())
            return drp[i];

        return NULL;
    }

    DriverTrackerPositioner* getDriverTrackerPositioner(int i)
    {
        if (i >= 0 && i < dtp.size())
            return dtp[i];

        return NULL;
    }

    bool isTimerActive() const
    {
        return timer->isActive();
    }

    void playingSeeked()
    {
        for (int i = 0; i < dtp.size(); ++i)
        {
            drp[i]->setNoSmoothSync(true);
            dtp[i]->setNoSmoothSync(true);
        }
    }

    DriverTrackerPositioner *getLeaderDriver();


private:
    QObject *parent;
    Timer *timer;
    int fps;
    int sessionInterval;

    bool visible;       //if driver tracker is not opened reduce fps to 1

    QVector<DriverRadarPositioner*> drp;
    QVector<DriverTrackerPositioner*> dtp;

    struct IntervalData
    {
        qint64 previousTick;
        qint64 avgInterval;
        qint64 prevAvgInterval;
        int timeoutCounter;

        void resetData()
        {
            previousTick = 0;
            avgInterval = 0;
            prevAvgInterval = 0;
            timeoutCounter = 0;
        }

    };

    IntervalData intervalData;
    
};

}
#endif // DRIVERTRACKERTIMER_H
